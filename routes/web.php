<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\Auth\LoginController;
use App\Http\Controllers\Dashboard\AdminController;
use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\GovernorateController;
use App\Http\Controllers\Dashboard\CountryController;
use App\Http\Controllers\Dashboard\CouponController;
use App\Http\Controllers\Dashboard\PageController;
use App\Http\Controllers\Dashboard\SlideController;
use App\Http\Controllers\Dashboard\SiteInfoController;
use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Controllers\Dashboard\ShippingCompanyController;
use App\Http\Controllers\Dashboard\BrandController;
use App\Http\Controllers\Dashboard\ProductController;
use App\Http\Controllers\Dashboard\ProductImageController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\MessageController;

use App\Http\Controllers\Site\Auth\GoogleAuthController;
use App\Http\Controllers\Site\Auth\FacebookAuthController;
use App\Http\Controllers\Site\User\AccountController;
use App\Http\Controllers\Site\HomeController As SiteHomeController;
use App\Http\Controllers\Site\ContactUsController;
use App\Http\Controllers\Site\CartController;
use App\Http\Controllers\Site\CheckoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale() , 
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'
    ] ], function()
    {

        Route::get('/dashboard', function () {
            return view('dashboard');
        })->middleware(['auth'])->name('dashboard');
        require __DIR__.'/auth.php';

        Route::get('/Dashboard/login', [LoginController::class , 'form'])->name('dashboard.login.form');
        Route::post('/Dashboard/login', [LoginController::class , 'login'])->name('dashboard.login');
        Route::group(['prefix' => 'Dashboard' , 'as' => 'dashboard.' , 'middleware' => ['admin'] ], function() {
            Route::get('/' , [HomeController::class , 'index'])->name('home');
            Route::resource('admins', AdminController::class);
            Route::resource('categories', CategoryController::class);
            Route::resource('countries', CountryController::class);
            Route::resource('governorates', GovernorateController::class);
            Route::resource('coupons', CouponController::class);
            Route::resource('pages', PageController::class);
            Route::resource('brands', BrandController::class);
            Route::resource('orders', OrderController::class);
            Route::resource('products', ProductController::class);
            Route::resource('messages', MessageController::class);
            Route::get('product_images.destroy', [ProductImageController::class , 'destroy' ])->name('product_images.destroy');
            Route::resource('shipping_companies', ShippingCompanyController::class);
            Route::resource('slides', SlideController::class);
            Route::resource('slides', SlideController::class);
            Route::get('/site_info' , [SiteInfoController::class , 'edit'])->name('site_info.edit');
            Route::patch('/site_info' , [SiteInfoController::class , 'update'])->name('site_info.update');
            Route::get('/logout' ,  [LoginController::class , 'logout'])->name('logout');
            Route::get('/profile' ,  [ProfileController::class , 'profile'])->name('profile');
            Route::patch('/profile' ,  [ProfileController::class , 'update_profile'])->name('profile.update');

            Route::get('/password' ,  [ProfileController::class , 'password'])->name('password');
            Route::patch('/password' ,  [ProfileController::class , 'update_password'])->name('password.update');
        });


        Route::get('/auth/google' , [GoogleAuthController::class , 'index'] );
        Route::get('/auth/google/callback', [GoogleAuthController::class , 'handleGoogleCallback']);
        Route::get('/auth/facebook' , [FacebookAuthController::class , 'index'] );
        Route::get('/auth/facebook/callback', [FacebookAuthController::class , 'handleFacebookCallback']);


        Route::group(['middleware' => 'auth'], function() {
            Route::get('/account' , [AccountController::class , 'index'])->name('account');
            Route::patch('/account' , [AccountController::class , 'update'])->name('account.update');
            Route::get('/logout' , [AccountController::class , 'logout'])->name('user.logout');
            Route::get('/orders' , [AccountController::class , 'logout'])->name('user.orders');
        });



        Route::group(['middleware' => ['cart' , 'country'] ], function() {
            Route::get('/' , [SiteHomeController::class , 'index'] );
            Route::get('/p/{product}' , [SiteHomeController::class , 'product'] )->name('products.show');
            Route::get('/contact' , [ContactUsController::class , 'form'] )->name('contact.form');
            Route::post('/send' , [ContactUsController::class , 'send'] )->name('contact.send');
            Route::post('/cart'  , [CartController::class , 'store'] )->name('cart.store');
            Route::get('/cart'  , [CartController::class , 'index'] )->name('cart.index');
            Route::get('/shop/{category}' , [SiteHomeController::class , 'category_products'] )->name('category.products');
            Route::get('/shop' ,  [SiteHomeController::class , 'shop']  )->name('shop');
            Route::get('/page/{page}' ,  [SiteHomeController::class , 'page']  )->name('pages.show');
            Route::get('/checkout' , [CheckoutController::class , 'show'] )->name('checkout.show');
            Route::post('/checkout' , [CheckoutController::class , 'store'] )->name('checkout.store');
            Route::get('/checkout/success' , [CheckoutController::class , 'index'] )->name('checkout.index');
            Route::get('/coupon' , [CheckoutController::class , 'validateDiscountCoupon'] )->name('coupons.validate');
            Route::get('/get_product_details' , [SiteHomeController::class  , 'get_product_details'] )->name('get_product_details');
        });


        
    });


