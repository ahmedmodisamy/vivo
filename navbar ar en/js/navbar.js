$(document).ready(function() {
  $('#mainNavHeader-open-btn').click(() => {
    $('#mainheader-mainNav')
      .addClass('mainheader-mainNav-active');
  });
  
  $('#mainNavHeader-close-btn').click(() => {
    $('#mainheader-mainNav')
      .removeClass('mainheader-mainNav-active');
  });
})