<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
|
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],
    'google' => [    
        'client_id' => env('GOOGLE_CLIENT_ID'),  
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),  
        'redirect' => env('GOOGLE_REDIRECT_URI') 
    ],
    'facebook' => [
        'client_id' => '420056543129533',
        'client_secret' => '76a5b46127bdef0d8ee22d0963ab548b',
        'redirect' => 'http://localhost:8000/en/auth/facebook/callback',
    ],
     'facebook' => [
        'client_id' => env('Facebook_CLIENT_ID'),
        'client_secret' => env('Facebook_CLIENT_SECRET'),
        'redirect' => env('Facebook_REDIRECT_URI'),
    ],


];
