@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
<header class="navbar navbar-expand-md navbar-light d-print-none">
  <div class="container-xl">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu">
      <span class="navbar-toggler-icon"></span>
    </button>
    <h1 class="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
      <a href="{{ url('Dashboard/') }}">
        <img src="{{ Storage::url('dashboard_assets/static/logo.svg') }}" width="110" height="32" alt="Tabler" class="navbar-brand-image">
      </a>
    </h1>
    <div class="navbar-nav flex-row order-md-last">
     <div class="nav-item dropdown d-none d-md-flex me-3">
      <a href="{{ $lang == 'ar' ? LaravelLocalization::getLocalizedURL('en') :  LaravelLocalization::getLocalizedURL('ar') }}" class="nav-link px-0" >
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-language" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <path d="M5 7h7m-2 -2v2a5 8 0 0 1 -5 8m1 -4a7 4 0 0 0 6.7 4"></path>
          <path d="M11 19l4 -9l4 9m-.9 -2h-6.2"></path>
        </svg>
        @lang('dashboard.change_language')
      </a>

    </div>

    <div class="nav-item dropdown d-none d-md-flex me-3">
      <a href="{{ url('/') }}" class="nav-link px-0" >
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-link" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <path d="M10 14a3.5 3.5 0 0 0 5 0l4 -4a3.5 3.5 0 0 0 -5 -5l-.5 .5"></path>
          <path d="M14 10a3.5 3.5 0 0 0 -5 0l-4 4a3.5 3.5 0 0 0 5 5l.5 -.5"></path>
        </svg>
        @lang('dashboard.visit_website')
      </a>
    </div>


    <div class="nav-item dropdown d-none d-md-flex me-3">
      <a href="#" class="nav-link px-0" data-bs-toggle="dropdown" tabindex="-1" aria-label="Show notifications">
        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M10 5a2 2 0 0 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6" /><path d="M9 17v1a3 3 0 0 0 6 0v-1" /></svg>
        <span class="badge bg-red"></span>
      </a>
      <div class="dropdown-menu dropdown-menu-end dropdown-menu-card">
       @include('dashboard.layouts.notifications')
     </div>
   </div>
   <div class="nav-item dropdown">
    <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
      <span class="avatar avatar-sm" style="background-image: url({{ Storage::url('admins/'.Auth::guard('admin')->user()->image) }})"></span>
      <div class="d-none d-xl-block ps-2">
        <div> {{ Auth::guard('admin')->user()->username }} </div>
        <div class="mt-1 small text-muted"> @lang('dashboard.admin') </div>
      </div>
    </a>
    <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
      <a href="{{ route('dashboard.profile') }}" class="dropdown-item"> 
        <span class='nav-link-icon' >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-id" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <rect x="3" y="4" width="18" height="16" rx="3"></rect>
            <circle cx="9" cy="10" r="2"></circle>
            <line x1="15" y1="8" x2="17" y2="8"></line>
            <line x1="15" y1="12" x2="17" y2="12"></line>
            <line x1="7" y1="16" x2="17" y2="16"></line>
          </svg>
        </span>
        @lang('dashboard.profile') 
      </a>
      <a href="{{ route('dashboard.password') }}" class="dropdown-item">
        <span  class='nav-link-icon' >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-lock-open" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <rect x="5" y="11" width="14" height="10" rx="2"></rect>
            <circle cx="12" cy="16" r="1"></circle>
            <path d="M8 11v-5a4 4 0 0 1 8 0"></path>
          </svg>
        </span>
        @lang('dashboard.change_password')
      </a>
      <div class="dropdown-divider"></div>
      <a href="{{ route('dashboard.site_info.edit') }}" class="dropdown-item">
        <span class='nav-link-icon' >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M10.325 4.317c.426 -1.756 2.924 -1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543 -.94 3.31 .826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756 .426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543 -.826 3.31 -2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756 -2.924 1.756 -3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543 .94 -3.31 -.826 -2.37 -2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756 -.426 -1.756 -2.924 0 -3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94 -1.543 .826 -3.31 2.37 -2.37c1 .608 2.296 .07 2.572 -1.065z"></path>
            <circle cx="12" cy="12" r="3"></circle>
          </svg>
        </span>
        @lang('dashboard.settings') 
      </a>
      <a href="{{ route('dashboard.logout') }}" class="dropdown-item"> 
        <span class='nav-link-icon' >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-logout" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2"></path>
            <path d="M7 12h14l-3 -3m0 6l3 -3"></path>
          </svg>
        </span>
        @lang('dashboard.logout') 
      </a>
    </div>
  </div>
</div>
</div>
</header>
<div class="navbar-expand-md">
  <div class="collapse navbar-collapse" id="navbar-menu">
    <div class="navbar navbar-light">
      <div class="container-xl">
        <ul class="navbar-nav">
          @php
          $slides = $home = $pages = $categories =  $orders = $countries  = $brands = $products = $shipping_companies = $governorates = $admins = $site_info = $coupons = '';

          // dd(Request::segment(3));
          switch (Request::segment(3)) {
            case null:
            $home = 'active';
            break;
            case 'slides':
            $slides = 'active';
            break;
            case 'orders':
            $orders = 'active';
            break;
            case 'pages':
            $pages = 'active';
            break;
            case 'countries':
            $countries = 'active';
            break;
            case 'governorates':
            $countries = 'active';
            break;
            case 'admins':
            $admins = 'active';
            break;
            case 'coupons':
            $products = 'active';
            break;
            case 'categories':
            $categories = 'active';
            break;
            case 'products':
            $products = 'active';
            break;
            case 'shipping_companies':
            $shipping_companies = 'active';
            break;
            case 'site_info':
            $site_info = 'active';
            break;


            default:
                // code...
            break;
          }
          @endphp
          <li class="nav-item {{ $home }}">
            <a class="nav-link" href="{{ route('dashboard.home') }}" >
              <span class="nav-link-icon d-md-none d-lg-inline-block"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><polyline points="5 12 3 12 12 3 21 12 19 12" /><path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7" /><path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6" /></svg>
              </span>
              <span class="nav-link-title">
                @lang('dashboard.home')
              </span>
            </a>
          </li>


          <li class="nav-item dropdown {{ $admins }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <circle cx="9" cy="7" r="4"></circle>
                  <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                  <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                  <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('admins.admins')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.admins.create') }}" >
                @lang('admins.add_new_admin')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.admins.index') }}" >
                @lang('admins.show_all_admins')
              </a>

            </div>
          </li>

          <li class="nav-item dropdown {{ $slides }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-photo" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <line x1="15" y1="8" x2="15.01" y2="8"></line>
                  <rect x="4" y="4" width="16" height="16" rx="3"></rect>
                  <path d="M4 15l4 -4a3 5 0 0 1 3 0l5 5"></path>
                  <path d="M14 14l1 -1a3 5 0 0 1 3 0l2 2"></path>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('slides.slides')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.slides.create') }}" >
                @lang('slides.add_new_slide')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.slides.index') }}" >
                @lang('slides.show_all_slides')
              </a>
            </div>
          </li>

          <li class="nav-item dropdown {{ $pages }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-notebook" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <path d="M6 4h11a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-11a1 1 0 0 1 -1 -1v-14a1 1 0 0 1 1 -1m3 0v18"></path>
                  <line x1="13" y1="8" x2="15" y2="8"></line>
                  <line x1="13" y1="12" x2="15" y2="12"></line>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('pages.pages')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.pages.create') }}" >
                @lang('pages.add_new_page')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.pages.index') }}" >
                @lang('pages.show_all_pages')
              </a>

            </div>
          </li>
          <li class="nav-item dropdown {{ $categories }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-certificate" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <circle cx="15" cy="15" r="3"></circle>
                  <path d="M13 17.5v4.5l2 -1.5l2 1.5v-4.5"></path>
                  <path d="M10 19h-5a2 2 0 0 1 -2 -2v-10c0 -1.1 .9 -2 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -1 1.73"></path>
                  <line x1="6" y1="9" x2="18" y2="9"></line>
                  <line x1="6" y1="12" x2="9" y2="12"></line>
                  <line x1="6" y1="15" x2="8" y2="15"></line>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('categories.categories')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.categories.create') }}" >
                @lang('categories.add_new_category')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.categories.index') }}" >
                @lang('categories.show_all_categories')
              </a>

            </div>
          </li>
          <li class="nav-item dropdown {{ $coupons }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-discount" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <line x1="9" y1="15" x2="15" y2="9"></line>
                  <circle cx="9.5" cy="9.5" r=".5" fill="currentColor"></circle>
                  <circle cx="14.5" cy="14.5" r=".5" fill="currentColor"></circle>
                  <circle cx="12" cy="12" r="9"></circle>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('coupons.coupons')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.coupons.create') }}" >
                @lang('coupons.add_new_coupon')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.coupons.index') }}" >
                @lang('coupons.show_all_coupons')
              </a>

            </div>
          </li>



          <li class="nav-item dropdown {{ $products }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-building-store" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <line x1="3" y1="21" x2="21" y2="21"></line>
                  <path d="M3 7v1a3 3 0 0 0 6 0v-1m0 1a3 3 0 0 0 6 0v-1m0 1a3 3 0 0 0 6 0v-1h-18l2 -4h14l2 4"></path>
                  <line x1="5" y1="21" x2="5" y2="10.85"></line>
                  <line x1="19" y1="21" x2="19" y2="10.85"></line>
                  <path d="M9 21v-4a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v4"></path>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('products.products')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.products.create') }}" >
                @lang('products.add_new_product')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.products.index') }}" >
                @lang('products.show_all_products')
              </a>            
            </div>
          </li>


          <li class="nav-item dropdown {{ $shipping_companies }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-truck" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <circle cx="7" cy="17" r="2"></circle>
                  <circle cx="17" cy="17" r="2"></circle>
                  <path d="M5 17h-2v-11a1 1 0 0 1 1 -1h9v12m-4 0h6m4 0h2v-6h-8m0 -5h5l3 5"></path>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('shipping_companies.shipping_companies')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.shipping_companies.create') }}" >
                @lang('shipping_companies.add_new_shipping_company')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.shipping_companies.index') }}" >
                @lang('shipping_companies.show_all_shipping_companies')
              </a>
            </div>
          </li>



          <li class="nav-item dropdown {{ $countries }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-current-location" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <circle cx="12" cy="12" r="3"></circle>
                  <circle cx="12" cy="12" r="8"></circle>
                  <line x1="12" y1="2" x2="12" y2="4"></line>
                  <line x1="12" y1="20" x2="12" y2="22"></line>
                  <line x1="20" y1="12" x2="22" y2="12"></line>
                  <line x1="2" y1="12" x2="4" y2="12"></line>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('countries.countries')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.countries.create') }}" >
                @lang('countries.add_new_country')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.countries.index') }}" >
                @lang('countries.show_all_countries')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.governorates.create') }}" >
                @lang('governorates.add_new_governorate')
              </a>
              <a class="dropdown-item" href="{{ route('dashboard.governorates.index') }}" >
                @lang('governorates.show_all_governorates')
              </a>
            </div>
          </li>

          <li class="nav-item dropdown {{ $orders }}">
            <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false" >
              <span class="nav-link-icon d-md-none d-lg-inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-current-location" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <circle cx="12" cy="12" r="3"></circle>
                  <circle cx="12" cy="12" r="8"></circle>
                  <line x1="12" y1="2" x2="12" y2="4"></line>
                  <line x1="12" y1="20" x2="12" y2="22"></line>
                  <line x1="20" y1="12" x2="22" y2="12"></line>
                  <line x1="2" y1="12" x2="4" y2="12"></line>
                </svg>
              </span>
              <span class="nav-link-title">
                @lang('orders.orders')
              </span>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('dashboard.orders.index') }}" >
                @lang('orders.show_all_orders')
              </a>
            </div>
          </li>


        </ul>
      </div>
    </div>
  </div>
</div>