<div class="list-group list-group-flush list-group-hoverable">

 @foreach (Auth::guard('admin')->user()->notifications as $one_notification)
 <div class="list-group-item">
  <div class="row align-items-center">
   @if ($one_notification->read_at == null)
   <div class="col-auto"><span class="badge bg-blue"></span></div>
   @endif

   <div class="col text-truncate">
    <small class="d-block text-muted text-truncate mt-n1"> {{ $one_notification->data['content'] }} </small>



    @php
      switch ($one_notification->type) {
        case 'App\Notifications\NewMessageFromContactUsNotification':
          $url = route('dashboard.messages.show' , ['message' => $one_notification->data['id']] );
          break;
        
        default:
          $url = '#' ;
          break;
      }
    @endphp

   




    <a href="{{ $url }}" class="text-body d-block"> {{ $one_notification->created_at->diffForHumans() }} </a>
  </div>

</div>
</div>
@endforeach
</div>