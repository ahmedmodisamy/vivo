{{-- <div class="alert alert-icon alert-primary" role="alert">
	<i class="fe fe-bell mr-2" aria-hidden="true"></i> You have done 5 actions. 
</div>
 --}}
@if (session('success'))

<div class="alert alert-important alert-success alert-dismissible" role="alert">
  <div class="d-flex">
    <div>
      <!-- Download SVG icon from http://tabler-icons.io/i/check -->
      <!-- SVG icon code with class="alert-icon" -->
    </div>
    <div>
      {{ session('success') }} 
    </div>
  </div>
  <a class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="close"></a>
</div>

@endif



@if (session('error'))

<div class="alert alert-important alert-danger alert-dismissible" role="alert">
  <div class="d-flex">
    <div>
      <!-- Download SVG icon from http://tabler-icons.io/i/alert-circle -->
      <!-- SVG icon code with class="alert-icon" -->
    </div>
    <div>
      {{ session('error') }}
    </div>
  </div>
  <a class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="close"></a>
</div>
@endif
