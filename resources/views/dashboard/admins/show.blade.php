@extends('dashboard.layouts.master')


@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        Dashboard
      </div>
      <h2 class="page-title">
        Admins
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.admins.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('admins.show_all_admins')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('admins.show_admin_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('admins.created_at') </th>
              <td>{{ $admin->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $admin->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('admins.status') </th>
              <td>
                @switch($admin->active)
                @case(1)
                <span class="badge bg-green">@lang('admins.active')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('admins.inactive')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('admins.username') </th>
              <td> {{ $admin->username }} </td>
            </tr>
            <tr>
              <th> @lang('admins.email') </th>
              <td> {{ $admin->email }} </td>
            </tr>
            <tr>
              <th> @lang('admins.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$admin->admin_id]) }}">  {{ optional($admin->admin)->username }}  </a> </td>
            </tr>
            <tr>
              <th> @lang('admins.image') </th>
              <td> <img  class="avatar" src="{{ Storage::url('admins/'.$admin->image) }}" alt="">  </td>
            </tr>
            <tr>
              <th> @lang('admins.username') </th>
              <td> {{ $admin->username }} </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection