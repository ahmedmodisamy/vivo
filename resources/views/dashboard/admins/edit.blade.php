@extends('dashboard.layouts.master')


@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('admins.admins')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.admins.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('admins.show_all_admins')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">

  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('admins.edit_admin_details') </h3>
      </div>
      <form action="{{ route('dashboard.admins.update' , ['admin' => $admin->id ] ) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.username') </label>
                <div >
                  <input type="text" name='username' value="{{ $admin->username }}"  class="form-control @error('username') is-invalid @enderror" >
                  @error('username')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.email') </label>
                <div >
                  <input type="email" name='email'  value="{{ $admin->email }}" class="form-control @error('email') is-invalid @enderror" >
                  @error('email')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.image') </label>
                <div >
                  <input type="file" name='image' class="form-control @error('image') is-invalid @enderror" >
                  @error('image')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $admin->active == 1 ? 'checked="checked"' : '' }} >
                    <span class="form-check-label"> @lang('admins.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.curent_profile_image') </label>
                <div >
                  <img src="{{ Storage::url('admins/'.$admin->image) }}" alt="">
                </div>
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.admins.index') }}" class="btn btn-link"> @lang('admins.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('admins.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection