@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_title')
{{ trans('slides.edit_slide_details') }}
@endsection

@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('slides.slides')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.slides.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('slides.show_all_slides')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('slides.edit_slide_details') </h3>
      </div>
      <form action="{{ route('dashboard.slides.update' , ['slide' => $slide->id]) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.image') * (2376 * 807) px  </label>
                <div >
                  <input type="file" name='image'  class="form-control @error('image') is-invalid @enderror" >
                  @error('image')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.title_ar') </label>
                <div >
                  <input type="text" name='title[ar]' value="{{ $slide->getTranslation('title' , 'ar') }}" class="form-control @error('title.ar') is-invalid @enderror" >
                  @error('title.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.subtitle_ar') </label>
                <div >
                  <input type="text" name='subtitle[ar]' value="{{ $slide->getTranslation('subtitle' , 'ar') }}" class="form-control @error('subtitle.ar') is-invalid @enderror" >
                  @error('subtitle.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>




            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.title_en') </label>
                <div >
                  <input type="text" name='title[en]'  value="{{ $slide->getTranslation('title' , 'en') }}" class="form-control @error('title.en') is-invalid @enderror" >
                  @error('titl.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.subtitle_en') </label>
                <div >
                  <input type="text" name='subtitle[en]'  value="{{ $slide->getTranslation('subtitle' , 'en') }}" class="form-control @error('subtitle.en') is-invalid @enderror" >
                  @error('subtitle.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.link') </label>
                <div >
                  <input type="text" name='link'  value="{{ $slide->link }}" class="form-control @error('link') is-invalid @enderror" >
                  @error('link')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $slide->active == 1 ? 'checked=""' : 'checked' }} >
                    <span class="form-check-label"> @lang('slides.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('slides.current_image') </label>
                <img class="img-thumbnail" src="{{ Storage::url('slides/'.$slide->image) }}" alt="">
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.slides.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-warning ms-auto" style='float: right' > @lang('dashboard.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

