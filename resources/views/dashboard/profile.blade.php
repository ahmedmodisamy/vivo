@extends('dashboard.layouts.master')

@section('page_title')
@lang('dashboard.edit_profile_details')
@endsection

@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('dashboard.profile')
      </h2>
    </div>

  </div>
</div>
<div class="row row-deck row-cards">
    @include('dashboard.layouts.messages')

  <div class="col-12">
    
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('dashboard.edit_profile_details') </h3>
      </div>
      <form action="{{ route('dashboard.profile.update') }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.username') </label>
                <div >
                  <input type="text" name='username' value="{{ $admin->username }}"  class="form-control @error('username') is-invalid @enderror" >
                  @error('username')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.email') </label>
                <div >
                  <input type="email" name='email'  value="{{ $admin->email }}" class="form-control @error('email') is-invalid @enderror" >
                  @error('email')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.image') </label>
                <div >
                  <input type="file" name='image' class="form-control @error('image') is-invalid @enderror" >
                  @error('image')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('admins.curent_profile_image') </label>
                <div >
                  <img class='img-thumbnail ' src="{{ Storage::url('admins/'.$admin->image) }}" alt="">
                </div>
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.home') }}" class="btn btn-link"> @lang('admins.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('admins.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection