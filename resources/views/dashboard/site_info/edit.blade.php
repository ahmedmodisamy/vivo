@extends('dashboard.layouts.master')
@php

$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('site_info.site_info')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">

    </div>
  </div>
</div>
<div class="row row-deck row-cards">
    @include('dashboard.layouts.messages')
  
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('site_info.site_info') </h3>
      </div>
      <form action="{{ route('dashboard.site_info.update') }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.facebook') </label>
                <div >
                  <input type="text" name='facebook' value="{{ $site_info->facebook }}" class="form-control @error('facebook') is-invalid @enderror" >
                  @error('facebook')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.twitter') </label>
                <div >
                  <input type="text" name='twitter' value="{{ $site_info->twitter }}" class="form-control @error('twitter') is-invalid @enderror" >
                  @error('twitter')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.instagram') </label>
                <div >
                  <input type="text" name='instagram' value="{{ $site_info->instagram }}" class="form-control @error('instagram') is-invalid @enderror" >
                  @error('instagram')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.phone') </label>
                <div >
                  <input type="text" name='phone' value="{{ $site_info->phone }}" class="form-control @error('phone') is-invalid @enderror" >
                  @error('phone')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.email') </label>
                <div >
                  <input type="text" name='email' value="{{ $site_info->email }}" class="form-control @error('email') is-invalid @enderror" >
                  @error('email')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.working_hourse') </label>
                <div >
                  <input type="text" name='working_hourse' value="{{ $site_info->working_hourse }}" class="form-control @error('working_hourse') is-invalid @enderror" >
                  @error('working_hourse')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.address_ar') </label>
                <div >
                  <input type="text" name='address[ar]' value="{{ $site_info->getTranslation('address' , 'ar') }}" class="form-control @error('address.ar') is-invalid @enderror" >
                  @error('address.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.address_en') </label>
                <div >
                  <input type="text" name='address[en]' value="{{ $site_info->getTranslation('address' , 'en') }}" class="form-control @error('address.en') is-invalid @enderror" >
                  @error('address.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('site_info.location_on_map') </label>
                <div >
                 <div id="map" style="width: 100%; height: 400px;" ></div>
                  <input type="hidden" name="latitude" value="{{ $site_info->lat }}"  id="latitude" >
                  <input type="hidden" name="longitude" value="{{ $site_info->long }}" id="longitude" >
                </div>
              </div>
            </div>






          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.site_info.edit') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection




@section('scripts')
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBuQymvDTcNgdRWQN0RhT2YxsJeyh8Bys4&amp;libraries=places">
</script>
<script>
  $(document).ready(function() {
   

    var latlng = new google.maps.LatLng({{ $site_info->lat }}, {{ $site_info->lng }} );
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      title: 'Set lat/lon values for this property',
      draggable: true
    });

    google.maps.event.addListener(marker, 'dragend', function (event) {
      document.getElementById("latitude").value = this.getPosition().lat();
      document.getElementById("longitude").value = this.getPosition().lng();
    });


  });

  
</script>
@endsection