@extends('dashboard.layouts.master')
@php

$lang = LaravelLocalization::getCurrentLocale();
@endphp


@section('page_content')
<!-- shipping_company title -->
<div class="shipping_company-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- shipping_company pre-title -->
      <div class="shipping_company-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="shipping_company-title">
        @lang('shipping_companies.shipping_companies')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.shipping_companies.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('shipping_companies.show_all_shipping_companies')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('shipping_companies.show_shipping_company_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('shipping_companies.created_at') </th>
              <td>{{ $shipping_company->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $shipping_company->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('shipping_companies.status') </th>
              <td>
                @switch($shipping_company->main)
                @case('yes')
                <span class="badge bg-green">@lang('shipping_companies.main')</span>
                @break
                @case('no')
                <span class="badge bg-green">@lang('shipping_companies.notmain')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('shipping_companies.company_name') </th>
              <td> {{ $shipping_company->name }} </td>
            </tr>

            <tr>
              <th> @lang('shipping_companies.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$shipping_company->admin_id]) }}">  {{ optional($shipping_company->admin)->username }}  </a> </td>
            </tr>

            <tr>
              <th> @lang('shipping_companies.governorates') </th>
              <td> 
               <ul>
                 @foreach ($shipping_company->prices as $price)
                  <li> {{ optional($price->governorate)['name_'.$lang] }} => {{ $price->price }} LE </li>  
                 @endforeach
               </ul>
              </td>
            </tr>



          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection