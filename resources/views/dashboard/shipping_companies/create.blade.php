@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_title')
{{ trans('shipping_companies.add_new_shipping_company') }}
@endsection

@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('shipping_companies.shipping_companies')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.shipping_companies.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('shipping_companies.show_all_shipping_companies')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('shipping_companies.add_new_shipping_company') </h3>
      </div>
      <form action="{{ route('dashboard.shipping_companies.store') }}" method='POST' enctype="multipart/form-data">
        @csrf
        <div class="card-body border-bottom py-3">
          <div class="row">

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('shipping_companies.company_name') </label>
                <div >
                  <input type="text" name='name' value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" >
                  @error('name')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th> @lang('shipping_companies.governorate') </th>
                    <th> @lang('shipping_companies.price') </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($governorates as $governorate)
                  <tr>
                    <td>
                      <label class="form-check form-switch">
                        <input class="form-check-input" name='governorate[]' value="{{ $governorate->id }}" type="checkbox">
                        <span class="form-check-label">{{ $governorate->name }}</span>
                      </label>
                    </td>
                    <td>
                      <input type="number" name='price[{{ $governorate->id }}]' class="form-control @error('price.*') is-invalid @enderror" >
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>




          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.shipping_companies.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.add') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
