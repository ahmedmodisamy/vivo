@extends('dashboard.layouts.master')


@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('coupons.coupons')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.coupons.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-discount" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="15" x2="15" y2="9"></line>
            <circle cx="9.5" cy="9.5" r=".5" fill="currentColor"></circle>
            <circle cx="14.5" cy="14.5" r=".5" fill="currentColor"></circle>
            <circle cx="12" cy="12" r="9"></circle>
          </svg>
          @lang('coupons.show_all_coupons')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('coupons.show_coupon_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('coupons.created_at') </th>
              <td>{{ $coupon->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $coupon->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('coupons.status') </th>
              <td>
                @switch($coupon->active)
                @case(1)
                <span class="badge bg-green">@lang('coupons.active')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('coupons.inactive')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('coupons.code') </th>
              <td> {{ $coupon->code }} </td>
            </tr>
            <tr>
              <th> @lang('coupons.discount_type') </th>
              <td> 
                @switch($coupon->discount_type)
                @case(1)
                <span class="badge bg-green">@lang('coupons.percentage')</span>
                @break
                @case(2)
                <span class="badge bg-blue">@lang('coupons.fixed_price')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('coupons.discount') </th>
              <td> {{ $coupon->discount }} </td>
            </tr>
            <tr>
              <th> @lang('coupons.allowed_used_times_per_user') </th>
              <td> {{ $coupon->allowed_used_times_per_user }} </td>
            </tr>
            <tr>
              <th> @lang('coupons.allowed_used_times') </th>
              <td> {{ $coupon->allowed_used_times }} </td>
            </tr>
            <tr>
              <th> @lang('coupons.max_discount_amount') </th>
              <td> {{ $coupon->max_discount_amount }} </td>
            </tr>
            <tr>
              <th> @lang('coupons.start_date') </th>
              <td> {{ $coupon->start_date->toFormattedDateString() }} <span class="text-muted"> {{ $coupon->start_date->diffForHumans() }}  </span> </td>
            </tr>
            <tr>
              <th> @lang('coupons.end_date') </th>
              <td> {{ $coupon->end_date->toFormattedDateString() }} <span class="text-muted"> {{  $coupon->end_date->diffForHumans() }}  </span> </td>
            </tr>
            <tr>
              <th> @lang('coupons.start_time') </th>
              <td> {{ $coupon->start_time }} </td>
            </tr>
            <tr>
              <th> @lang('coupons.end_time') </th>
              <td> {{ $coupon->end_time }} </td>
            </tr>

            <tr>
              <th> @lang('coupons.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$coupon->admin_id]) }}">  {{ optional($coupon->admin)->username }}  </a> </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection