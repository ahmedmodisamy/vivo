@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('coupons.coupons')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.coupons.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('coupons.show_all_coupons')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">

  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"> @lang('coupons.add_new_coupon') </h3>
      </div>
      <form action="{{ route('dashboard.coupons.store') }}" method='POST' enctype="multipart/form-data">
        @csrf
        <div class="card-body border-bottom py-3">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.code') </label>
                <div >
                  <input type="text" name='code' value="{{ old('code') }}" class="form-control @error('code') is-invalid @enderror" >
                  @error('code')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.discount_type') </label>
                <div >
                  <select name="discount_type"class="form-control" required="required">
                    <option value="1"> @lang('coupons.percentage') % </option>
                    <option value="2"> @lang('coupons.fixed_price') LE </option>
                  </select>
                  @error('discount_type')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.discount') </label>
                <div >
                  <input type="number" value="{{ old('discount') }}" name='discount' class="form-control @error('discount') is-invalid @enderror" >
                  @error('discount')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.allowed_used_times_per_user') </label>
                <div >
                  <input type="number" value="{{ old('allowed_used_times_per_user') }}" name='allowed_used_times_per_user' class="form-control @error('allowed_used_times_per_user') is-invalid @enderror" >
                  @error('allowed_used_times_per_user')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.allowed_used_times') </label>
                <div >
                  <input type="number" value="{{ old('allowed_used_times') }}" name='allowed_used_times' class="form-control @error('allowed_used_times') is-invalid @enderror" >
                  @error('allowed_used_times')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.max_discount_amount') </label>
                <div >
                  <input type="number" value="{{ old('max_discount_amount') }}" name='max_discount_amount' class="form-control @error('max_discount_amount') is-invalid @enderror" >
                  @error('max_discount_amount')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.start_date') </label>
                <div >
                  <input id="calendar-simple" type="date"  value="{{ old('start_date') }}" name='start_date' class="form-control @error('start_date') is-invalid @enderror" >
                  @error('start_date')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.start_time') </label>
                <div >
                  <input type="time" name='start_time' value="{{ old('start_time') }}" class="form-control @error('start_time') is-invalid @enderror" >
                  @error('start_time')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.end_date') </label>
                <div >
                  <input id="calendar-simple1" type="date" name='end_date'  value="{{ old('end_date') }}" class="form-control @error('end_date') is-invalid @enderror" >
                  @error('end_date')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.end_time') </label>
                <div >
                  <input type="time" name='end_time' value="{{ old('end_time') }}"  class="form-control @error('end_time') is-invalid @enderror" >
                  @error('end_time')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" checked="">
                    <span class="form-check-label"> @lang('coupons.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
             <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.categories') </label>
                <div >
                  <select name="category_id[]" multiple="multiple" class="form-control" >
                    @foreach ($categories as $category)
                      <option value="{{ $category->id }}"> {{ $category->name }}</option>
                    @endforeach
                  </select>
                  @error('categories')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.coupons.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.add') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('styles')


    <link href="{{ Storage::url('dashboard_assets/dist/libs/flatpickr/dist/flatpickr.min.css') }}" rel="stylesheet"/>

@endsection

@section('scripts')


<script src="{{ Storage::url('dashboard_assets/dist/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
<script src="{{ Storage::url('dashboard_assets/dist/libs/flatpickr/dist/plugins/rangePlugin.js') }}"></script>

<script>
  document.addEventListener("DOMContentLoaded", function () {
    flatpickr(document.getElementById('calendar-simple'), {
    });
    flatpickr(document.getElementById('calendar-simple1'), {
    });
  });
</script>
@endsection