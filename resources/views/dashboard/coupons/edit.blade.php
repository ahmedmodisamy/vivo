@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('coupons.coupons')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.coupons.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-discount" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="15" x2="15" y2="9"></line>
            <circle cx="9.5" cy="9.5" r=".5" fill="currentColor"></circle>
            <circle cx="14.5" cy="14.5" r=".5" fill="currentColor"></circle>
            <circle cx="12" cy="12" r="9"></circle>
          </svg>
          @lang('coupons.show_all_coupons')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">

  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"> @lang('coupons.edit_coupon_details') </h3>
      </div>
      <form action="{{ route('dashboard.coupons.update' , ['coupon' => $coupon->id]) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.code') </label>
                <div >
                  <input type="text" name='code' value="{{ $coupon->code }}" class="form-control @error('code') is-invalid @enderror" >
                  @error('code')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.discount_type') </label>
                <div >
                  <select name="discount_type"class="form-control" required="required">
                    <option value="1" {{ $coupon->discount_type == 1 ? 'selected="selected"' : '' }}> @lang('coupons.percentage') % </option>
                    <option value="2" {{ $coupon->discount_type == 2 ? 'selected="selected"' : '' }}> @lang('coupons.fixed_price') LE </option>
                  </select>
                  @error('discount_type')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.discount') </label>
                <div >
                  <input type="number" value="{{ $coupon->discount }}" name='discount' class="form-control @error('discount') is-invalid @enderror" >
                  @error('discount')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.allowed_used_times_per_user') </label>
                <div >
                  <input type="number" value="{{ $coupon->allowed_used_times_per_user }}" name='allowed_used_times_per_user' class="form-control @error('allowed_used_times_per_user') is-invalid @enderror" >
                  @error('allowed_used_times_per_user')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.allowed_used_times') </label>
                <div >
                  <input type="number" value="{{ $coupon->allowed_used_times }}" name='allowed_used_times' class="form-control @error('allowed_used_times') is-invalid @enderror" >
                  @error('allowed_used_times')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.max_discount_amount') </label>
                <div >
                  <input type="number" value="{{ $coupon->max_discount_amount }}" name='max_discount_amount' class="form-control @error('max_discount_amount') is-invalid @enderror" >
                  @error('max_discount_amount')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.start_date') </label>
                <div >
                  <input id="calendar-simple" type="date"  value="{{ $coupon->start_date }}" name='start_date' class="form-control @error('start_date') is-invalid @enderror" >
                  @error('start_date')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.start_time') </label>
                <div >
                  <input type="time" name='start_time' value="{{ $coupon->start_time }}" class="form-control @error('start_time') is-invalid @enderror" >
                  @error('start_time')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.end_date') </label>
                <div >
                  <input id="calendar-simple1" type="date" name='end_date'  value="{{ $coupon->end_date }}" class="form-control @error('end_date') is-invalid @enderror" >
                  @error('end_date')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.end_time') </label>
                <div >
                  <input type="time" name='end_time' value="{{ $coupon->end_time }}"  class="form-control @error('end_time') is-invalid @enderror" >
                  @error('end_time')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $coupon->active == 1 ? 'checked="checked"' : '' }} >
                    <span class="form-check-label"> @lang('coupons.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('coupons.categories') </label>
                <div >
                  <select name="category_id[]" multiple="multiple" class="form-control" >
                    @if ($coupon->categories != null)
                    @foreach ($categories as $category)
                    <option value="{{ $category->id }}" {{ in_array($category->id , json_decode($coupon->categories) ) ? 'selected="selected"' : '' }}> {{ $category['name_'.$lang] }}</option>
                    @endforeach
                    @else
                    @foreach ($categories as $category)
                    <option value="{{ $category->id }}" > {{ $category['name_'.$lang] }}</option>
                    @endforeach
                    @endif
                  </select>
                  @error('categories')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.coupons.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-warning ms-auto" style='float: right' > @lang('dashboard.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('styles')


<link href="{{ Storage::url('dashboard_assets/dist/libs/flatpickr/dist/flatpickr.min.css') }}" rel="stylesheet"/>

@endsection

@section('scripts')


<script src="{{ Storage::url('dashboard_assets/dist/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
<script src="{{ Storage::url('dashboard_assets/dist/libs/flatpickr/dist/plugins/rangePlugin.js') }}"></script>

<script>
  document.addEventListener("DOMContentLoaded", function () {
    flatpickr(document.getElementById('calendar-simple'), {
    });
    flatpickr(document.getElementById('calendar-simple1'), {
    });
  });
</script>
@endsection