@extends('dashboard.layouts.master')


@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('topics.topics')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.topics.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('topics.show_all_topics')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('topics.show_topic_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('topics.created_at') </th>
              <td>{{ $topic->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $topic->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('topics.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$topic->admin_id]) }}">  {{ optional($topic->admin)->username }}  </a> </td>
            </tr>

            <tr>
              <th> @lang('topics.status') </th>
              <td>
                @switch($topic->active)
                @case(1)
                <span class="badge bg-green">@lang('topics.active')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('topics.inactive')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('topics.status') </th>
              <td>
                @switch($topic->commentable)
                @case(1)
                <span class="badge bg-blue">@lang('topics.yes')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('topics.no')</span>
                @break
                @endswitch
              </td>
            </tr>

             <tr>
              <th> @lang('topics.views_count') </th>
              <td> {{ $topic->views_count }} </td>
            </tr>


            <tr>
              <th> @lang('topics.title') </th>
              <td> {{ $topic->title }} </td>
            </tr>
     
            
            <tr>
              <th> @lang('topics.content') </th>
              <td> 
                {!! $topic->content !!}
              </td>
            </tr>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection