@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_title')
@lang('topics.edit_topic_details')
@endsection
@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('topics.topics')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.topics.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('topics.show_all_topics')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  @include('dashboard.layouts.messages')
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('topics.edit_topic_details') </h3>
      </div>
      <form action="{{ route('dashboard.topics.update' , ['topic' => $topic->id]) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">


            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.image') </label>
                <div >
                  <input type="file" name='image'  class="form-control @error('image') is-invalid @enderror" >
                  @error('image')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.title') </label>
                <div >
                  <input type="text" name='title' value="{{ $topic->title }}" class="form-control @error('title') is-invalid @enderror" >
                  @error('title')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.slug') </label>
                <div >
                  <input type="text" name='slug' value="{{ $topic->slug }}" class="form-control @error('slug') is-invalid @enderror" >
                  @error('slug')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.content') </label>
                <div >
                  <textarea type="text" name='content' class="form-control @error('content') is-invalid @enderror"> 
                    {{ $topic->content }}
                  </textarea>
                  @error('content')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $topic->active == 1 ? 'checked' : '' }} >
                    <span class="form-check-label"> @lang('topics.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.activaion') </label>
                <div >
                  <label class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" value='1' name="commentable" {{ $topic->commentable == 1 ? 'checked' : '' }} >
                    <span class="form-check-label"> @lang('topics.commentable') ? </span>
                  </label>
                  @error('commentable')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('topics.current_image') </label>
                <img class='img-thumbnail' src="{{ Storage::url('topics/'.$topic->image) }}" alt="">
              </div>
            </div>




          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.topics.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')

<script src="https://cdn.tiny.cloud/1/ic4s7prz04qh4jzykmzgizzo1lize2ckglkcjr9ci9sgkbuc/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    toolbar_mode: 'floating',
  });
</script>
@endsection