@extends('dashboard.layouts.master')
@php

$lang = LaravelLocalization::getCurrentLocale();
@endphp
@section('page_title')
{{ trans('pages.edit_page_details') }}
@endsection
@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('pages.pages')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.pages.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('pages.show_all_pages')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('pages.edit_page_details') </h3>
      </div>
      <form action="{{ route('dashboard.pages.update' , ['page' => $page->id ]) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.title_ar') </label>
                <div >
                  <input type="text" name='title[ar]' value="{{ $page->getTranslation('title' , 'ar') }}" class="form-control @error('title.ar') is-invalid @enderror" >
                  @error('title.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.content_ar') </label>
                <div >
                  <textarea type="text" name='content[ar]' class="form-control @error('content.ar') is-invalid @enderror"> 
                    {{ $page->getTranslation('content' , 'ar') }}
                  </textarea>
                  @error('content.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.title_en') </label>
                <div >
                  <input type="text" name='title[en]' value="{{ $page->getTranslation('title' , 'en') }}" class="form-control @error('title.en') is-invalid @enderror" >
                  @error('title.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.content_en') </label>
                <div >
                  <textarea type="text" name='content[en]' class="form-control @error('content.en') is-invalid @enderror"> 
                    {{ $page->getTranslation('content' , 'en') }}
                  </textarea>
                  @error('content.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $page->active == 1 ? 'checked="checked"' : '' }} >
                    <span class="form-check-label"> @lang('pages.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.pages.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')

<script src="https://cdn.tiny.cloud/1/ic4s7prz04qh4jzykmzgizzo1lize2ckglkcjr9ci9sgkbuc/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    toolbar_mode: 'floating',
  });
</script>
@endsection