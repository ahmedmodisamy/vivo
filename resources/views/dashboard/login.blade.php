<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-alpha.22
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
  <title>Dashboar | Login</title>
  <!-- CSS files -->
  <link href="{{ Storage::url('dashboard_assets/dist/css/tabler.min.css') }}" rel="stylesheet"/>
  <link href="{{ Storage::url('dashboard_assets/dist/css/tabler-flags.min.css') }}" rel="stylesheet"/>
  <link href="{{ Storage::url('dashboard_assets/dist/css/tabler-payments.min.css') }}" rel="stylesheet"/>
  <link href="{{ Storage::url('dashboard_assets/dist/css/tabler-vendors.min.css') }}" rel="stylesheet"/>
  <link href="{{ Storage::url('dashboard_assets/dist/css/demo.min.css') }}" rel="stylesheet"/>
</head>
<body class="antialiased border-top-wide border-primary d-flex flex-column">
  <div class="flex-fill d-flex flex-column justify-content-center py-4">
    <div class="container-tight py-6">
      <div class="text-center mb-4">
        <a href="."><img src="{{ Storage::url('dashboard_assets/static/logo.svg') }}" height="36" alt=""></a>
      </div>
      <form class="card card-md" action="{{ route('dashboard.login') }}" method="post" autocomplete="off">
        @csrf
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Login to your account</h2>
          <div class="mb-3">
            <label class="form-label">Username</label>
            <input type="text" name="username" class="form-control @error('uername') is-invalid @enderror" placeholder="Username">
            @error('username')
            <p class="text-danger"> {{ $message }} </p>
            @enderror
          </div>
          <div class="mb-2">
            <label class="form-label">
              Password
            </label>
            <div class="input-group input-group-flat">
              <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password"  autocomplete="off">
            </div>
             @error('password')
              <p class="text-danger"> {{ $message }} </p>
              @enderror

          </div>
          <div class="mb-2">
            <label class="form-check">
              <input type="checkbox" name="remember" class="form-check-input"/>
              <span class="form-check-label">Remember me on this device</span>
            </label>
          </div>
          <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100">Sign in</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- Libs JS -->
  <script src="{{ Storage::url('dashboard_assets/dist/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Tabler Core -->
  <script src="{{ Storage::url('dashboard_assets/dist/js/tabler.min.js') }}"></script>
</body>
</html>