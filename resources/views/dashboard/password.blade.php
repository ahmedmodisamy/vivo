@extends('dashboard.layouts.master')

@section('page_title')
@lang('dashboard.edit_password')
@endsection

@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('dashboard.profile')
      </h2>
    </div>

  </div>
</div>
<div class="row row-deck row-cards">
    @include('dashboard.layouts.messages')

  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('dashboard.edit_password') </h3>
      </div>
      <form action="{{ route('dashboard.password.update') }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('dashboard.current_password') </label>
                <div >
                  <input type="password" name='current_password'  class="form-control @error('current_password') is-invalid @enderror" >
                  @error('current_password')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('dashboard.new_password') </label>
                <div >
                  <input type="password" name='password' class="form-control @error('password') is-invalid @enderror" >
                  @error('password')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('dashboard.password_confirmation') </label>
                <div >
                  <input type="password" name='password_confirmation' class="form-control @error('password_confirmation') is-invalid @enderror" >
                  @error('password_confirmation')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.home') }}" class="btn btn-link"> @lang('admins.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('admins.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection