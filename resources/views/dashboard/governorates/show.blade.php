@extends('dashboard.layouts.master')

@php

$lang = LaravelLocalization::getCurrentLocale();
@endphp
@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        Dashboard
      </div>
      <h2 class="page-title">
        @lang('governorates.governorates')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.governorates.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('governorates.show_all_governorates')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('governorates.show_governorate_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('governorates.created_at') </th>
              <td>{{ $governorate->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $governorate->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('governorates.status') </th>
              <td>
                @switch($governorate->active)
                @case(1)
                <span class="badge bg-green">@lang('governorates.active')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('governorates.inactive')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('governorates.name_ar') </th>
              <td> {{ $governorate->name_ar }} </td>
            </tr>
            <tr>
              <th> @lang('governorates.name_en') </th>
              <td> {{ $governorate->name_en }} </td>
            </tr>
              <tr>
              <th> @lang('governorates.country') </th>
              <td> <a href="{{ route('dashboard.countries.show' , ['country' => $governorate->country_id]) }}">  {{ optional($governorate->country)['name_'.$lang] }}  </a> </td>
            </tr>
            <tr>
              <th> @lang('governorates.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$governorate->admin_id]) }}">  {{ optional($governorate->admin)->username }}  </a> </td>
            </tr>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection