@extends('dashboard.layouts.master')
@php

$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_title')
{{ trans('governorates.show_all_governorates') }}
@endsection

@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
          @lang('governorates.governorates')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.governorates.create') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
          @lang('governorates.add_new_governorate')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">

 @livewire('dashboard.governorates.list-all-governorates')
</div>
@endsection