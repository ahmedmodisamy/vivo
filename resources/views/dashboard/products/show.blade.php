@extends('dashboard.layouts.master')


@section('page_content')
<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('pages.pages')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.pages.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('pages.show_all_pages')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('pages.show_page_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('pages.created_at') </th>
              <td>{{ $page->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $page->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('pages.status') </th>
              <td>
                @switch($page->active)
                @case(1)
                <span class="badge bg-green">@lang('pages.active')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('pages.inactive')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('pages.title_ar') </th>
              <td> {{ $page->title_ar }} </td>
            </tr>
            <tr>
              <th> @lang('pages.title_en') </th>
              <td> {{ $page->title_en }} </td>
            </tr>

            <tr>
              <th> @lang('pages.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$page->admin_id]) }}">  {{ optional($page->admin)->username }}  </a> </td>
            </tr>
            <tr>
              <th> @lang('pages.content_ar') </th>
              <td> 
                {!! $page->content_ar !!}
              </td>
            </tr>
            <tr>
              <th> @lang('pages.content_en') </th>
              <td> 
                {!! $page->content_en !!}
              </td>
            </tr>



          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection