@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_title')
@lang('products.edit_product_details')
@endsection

@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('products.products')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.products.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-new-section" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <line x1="9" y1="12" x2="15" y2="12"></line>
            <line x1="12" y1="9" x2="12" y2="15"></line>
            <path d="M4 6v-1a1 1 0 0 1 1 -1h1m5 0h2m5 0h1a1 1 0 0 1 1 1v1m0 5v2m0 5v1a1 1 0 0 1 -1 1h-1m-5 0h-2m-5 0h-1a1 1 0 0 1 -1 -1v-1m0 -5v-2m0 -5"></path>
          </svg>
          @lang('products.show_all_products')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  @include('dashboard.layouts.messages')
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('products.edit_product_details') </h3>
      </div>
      <form action="{{ route('dashboard.products.update' , ['product' => $product->id ]) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.image')  </label>
                <div >
                  <input type="file" name='image'  class="form-control @error('image') is-invalid @enderror" >
                  @error('image')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.category') </label>
                <div >
                  <select name="category_id"  class="form-control" >
                    <option value=""></option>
                    @foreach ($categories as $category)
                    <option value="{{ $category->id }}" {{ $product->category_id == $category->id ? 'selected="selected"' : '' }}> {{ $category['name_'.$lang] }} </option>
                    @endforeach
                  </select>
                  @error('category_id')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.brand') </label>
                <div >
                  <select name="brand_id"  class="form-control" >
                    <option value=""></option>
                    @foreach ($brands as $brand)
                    <option value="{{ $brand->id }}" {{ $product->brand_id == $brand->id ? 'selected="selected"' : '' }} > {{ $brand->getTranslation('name' , $lang) }} </option>
                    @endforeach
                  </select>
                  @error('brand_id')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.name_ar') </label>
                <div >
                  <input type="text" name='name[ar]'  value="{{ $product->getTranslation('name' , 'ar') }}" class="form-control @error('name.ar') is-invalid @enderror" >
                  @error('name.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.name_en') </label>
                <div >
                  <input type="text" name='name[en]'  value="{{ $product->getTranslation('name' , 'en') }}" class="form-control @error('name.en') is-invalid @enderror" >
                  @error('name.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.price') </label>
                <div >
                  <input type="number" name='price'  value="{{ $product->price }}" class="form-control @error('price') is-invalid @enderror" >
                  @error('price')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.price_after_sale') </label>
                <div >
                  <input type="number" name='price_after_sale'  value="{{ $product->price_after_sale }}" class="form-control @error('price_after_sale') is-invalid @enderror" >
                  @error('price_after_sale')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.slug') </label>
                <div >
                  <input type="text" name='slug'  value="{{ $product->slug }}" class="form-control @error('slug') is-invalid @enderror" >
                  @error('slug')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.stock') </label>
                <div >
                  <input type="number" name='stock'  value="{{ $product->stock }}" class="form-control @error('stock') is-invalid @enderror" >
                  @error('stock')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.returnable') </label>
                <div >
                  <select name="returnable" id="input" class="form-control" >
                    <option value="1" {{ $product->returnable == 1 ? 'selected="selected"' : '' }} >@lang('products.yes')</option>
                    <option value="0" {{ $product->returnable == 0 ? 'selected="selected"' : '' }} >@lang('products.no')</option>
                  </select>
                  @error('returnable')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-6">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.mini_description_ar') </label>
                <div >
                  <input type="text" name='mini_description[ar]' value="{{ $product->getTranslation('mini_description' , 'ar') }}" class="form-control @error('mini_description.ar') is-invalid @enderror" >
                  @error('mini_description.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-6">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.mini_description_en') </label>
                <div >
                  <input type="text" name='mini_description[en]' value="{{ $product->getTranslation('mini_description' , 'en') }}" class="form-control @error('mini_description.en') is-invalid @enderror" >
                  @error('mini_description.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.other_images')  </label>
                <div >
                  <input type="file" name='images[]' multiple="multiple" class="form-control @error('images') is-invalid @enderror" >
                  @error('images')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $product->active == 1 ? 'checked' : '' }} >
                    <span class="form-check-label"> @lang('products.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('products.featured') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="featured" type="checkbox" {{ $product->featured == 1 ? 'checked' : '' }} >
                    <span class="form-check-label"> @lang('products.featured') </span>
                  </label>
                  @error('featured')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.description_ar') </label>
                <div >
                  <textarea type="text" name='description[ar]' class="form-control @error('description.ar') is-invalid @enderror"> 
                    {!! $product->getTranslation('description' , 'ar') !!}
                  </textarea>
                  @error('description.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


            <div class="col-md-12">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.description_en') </label>
                <div >
                  <textarea type="text" name='description[en]' class="form-control @error('description.en') is-invalid @enderror"> 
                    {!! $product->getTranslation('description' , 'en') !!}
                  </textarea>
                  @error('description.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>   

            <div class="col-md-6">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('pages.current_main_image') </label>
                <div >
                 <img src="{{ Storage::url('products/'.$product->image) }}" alt="">
               </div>
             </div>
           </div>  

           <div class="col-md-6">
            <div class="form-group mb-3">
              <label class="form-label"> @lang('pages.other_images') </label>
              <div >
               <div id="carousel-captions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                  @php
                  $i = 1;
                  @endphp

                  @foreach ($product->images as $image)
                  <div class="carousel-item {{ $i == 1 ? 'active' : '' }} ">
                    <img class="d-block w-100 img-thumbnail" alt="" src="{{ Storage::url('products/'.$image->image) }}">
                    <div class="carousel-item-background d-none d-md-block"></div>
                    <div class="carousel-caption d-none d-md-block">
                      <p>

                        <a href="{{ route('dashboard.product_images.destroy' , ['product_image' => $image->id ] ) }}" class='btn btn-danger' > @lang('products.delete') </a>
                        
                      </p>
                    </div>
                  </div>
                  @php
                  $i++;
                  @endphp
                  @endforeach

                </div>
                <a class="carousel-control-prev" href="#carousel-captions" role="button" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-captions" role="button" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>   


      </div>
    </div>

    <div class="card-footer d-flex align-items-center">
      <a href="{{ route('dashboard.products.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
      <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.edit') </button>
    </div>
  </form>
</div>
</div>
</div>
@endsection

@section('scripts')

<script src="https://cdn.tiny.cloud/1/ic4s7prz04qh4jzykmzgizzo1lize2ckglkcjr9ci9sgkbuc/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    toolbar_mode: 'floating',
  });
</script>
@endsection