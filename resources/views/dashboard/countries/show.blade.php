@extends('dashboard.layouts.master')


@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        Dashboard
      </div>
      <h2 class="page-title">
        @lang('categories.categories')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.categories.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('categories.show_all_categories')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> @lang('categories.show_category_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('categories.created_at') </th>
              <td>{{ $category->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $category->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('categories.status') </th>
              <td>
                @switch($category->active)
                @case(1)
                <span class="badge bg-green">@lang('categories.active')</span>
                @break
                @case(0)
                <span class="badge bg-green">@lang('categories.inactive')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('categories.name_ar') </th>
              <td> {{ $category->name_ar }} </td>
            </tr>
            <tr>
              <th> @lang('categories.name_en') </th>
              <td> {{ $category->name_en }} </td>
            </tr>
            <tr>
              <th> @lang('categories.order') </th>
              <td> {{ $category->order }} </td>
            </tr>
            <tr>
              <th> @lang('categories.added_by') </th>
              <td> <a href="{{ route('dashboard.admins.show' , ['admin' =>$category->admin_id]) }}">  {{ optional($category->admin)->username }}  </a> </td>
            </tr>
            <tr>
              <th> @lang('categories.current_category_image') </th>
              <td> <img  class="img-thumbnail" src="{{ Storage::url('categories/'.$category->image) }}" alt="">  </td>
            </tr>
            <tr>
              <th> @lang('categories.current_icon') </th>
              <td> <img  class="img-thumbnail" src="{{ Storage::url('categories/'.$category->icon) }}" alt="">  </td>
            </tr>

            <tr>
              <th> @lang('categories.current_cover') </th>
              <td> <img  class="img-thumbnail" src="{{ Storage::url('categories/'.$category->cover) }}" alt="">  </td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection