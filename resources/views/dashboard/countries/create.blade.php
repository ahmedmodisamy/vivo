@extends('dashboard.layouts.master')
@php

$lang = LaravelLocalization::getCurrentLocale();
@endphp

@section('page_title')
{{ trans('countries.add_new_country') }}
@endsection
@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('countries.countries')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.countries.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('countries.show_all_countries')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('countries.add_new_country') </h3>
      </div>
      <form action="{{ route('dashboard.countries.store') }}" method='POST' enctype="multipart/form-data">
        @csrf
        <div class="card-body border-bottom py-3">
          <div class="row">
           
            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('countries.name_ar') </label>
                <div >
                  <input type="text" name='name[ar]' value="{{ old('name.ar') }}" class="form-control @error('name.ar') is-invalid @enderror" >
                  @error('name.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('countries.name_en') </label>
                <div >
                  <input type="text" name='name[en]' value="{{ old('name.en') }}" class="form-control @error('name.en') is-invalid @enderror" >
                  @error('name.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('countries.image') </label>
                <div >
                  <input type="file" name='image'  class="form-control @error('image') is-invalid @enderror" >
                  @error('image')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>



            <div class="col-md-3">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('countries.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" checked="">
                    <span class="form-check-label"> @lang('countries.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>


          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.countries.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.add') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection