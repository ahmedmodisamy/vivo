@extends('dashboard.layouts.master')
@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp


@section('page_title')

{{ trans('categories.edit_category_details') }}
@endsection

@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('categories.categories')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{ route('dashboard.categories.index') }}" class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-view-360" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="12" cy="12" r="9"></circle>
            <ellipse cx="12" cy="12" rx="4" ry="9"></ellipse>
            <ellipse cx="12" cy="12" rx="4" ry="9" transform="rotate(90 12 12)"></ellipse>
          </svg>
          @lang('categories.show_all_categories')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-primary text-white">
        <h3 class="card-title"> @lang('categories.edit_category_details') </h3>
      </div>
      <form action="{{ route('dashboard.categories.update' , ['category' => $category->id ] ) }}" method='POST' enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="card-body border-bottom py-3">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('categories.category') </label>
                <div >
                  <select name="category_id" class="form-control">
                    <option value=""></option>
                    @foreach ($categories as $one_category)
                    <option value="{{ $one_category->id }}">{{ $one_category->getTranslation('name' , $lang) }}</option>
                    @endforeach
                  </select>
                  @error('category')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('categories.name_ar') </label>
                <div >
                  <input type="text" name='name[ar]' value="{{ $category->getTranslation('name' , 'ar') }}" class="form-control @error('name.ar') is-invalid @enderror" >
                  @error('name.ar')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('categories.name_en') </label>
                <div >
                  <input type="text" name='name[en]' value="{{ $category->getTranslation('name' , 'en') }}" class="form-control @error('name.en') is-invalid @enderror" >
                  @error('name.en')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            
           
         

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('categories.order') </label>
                <div >
                  <input type="text" name='order' value="{{ $category->order }}" class="form-control @error('order') is-invalid @enderror" >
                  @error('order')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group mb-3">
                <label class="form-label"> @lang('categories.activaion') </label>
                <div >
                  <label class="form-check">
                    <input class="form-check-input" value='1' name="active" type="checkbox" {{ $category->active == 1 ? 'checked="checked"' : '' }} >
                    <span class="form-check-label"> @lang('categories.active') </span>
                  </label>
                  @error('active')
                  <small class="text-danger"> {{ $message }} </small>
                  @enderror
                </div>
              </div>
            </div>
       
      

          </div>
        </div>

        <div class="card-footer d-flex align-items-center">
          <a href="{{ route('dashboard.categories.index') }}" class="btn btn-link"> @lang('dashboard.cancel') </a>
          <button type="submit" class="btn btn-primary ms-auto" style='float: right' > @lang('dashboard.edit') </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection