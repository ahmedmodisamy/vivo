@extends('dashboard.layouts.master')

@section('page_title')

@lang('orders.show_order_details')
@endsection

@section('page_content')

<!-- Page title -->
<div class="page-header d-print-none">
  <div class="row align-items-center">
    <div class="col">
      <!-- Page pre-title -->
      <div class="page-pretitle">
        @lang('dashboard.dashboard')
      </div>
      <h2 class="page-title">
        @lang('dashboard.orders')
      </h2>
    </div>
    <div class="col-auto ms-auto d-print-none">
      <div class="btn-list">
        <a href="{{route('dashboard.orders.index')}}"class="btn btn-primary d-none d-sm-inline-block" >
          <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-users" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
          </svg>
          @lang('orders.show_all_orders')
        </a>
      </div>
    </div>
  </div>
</div>
<div class="row row-deck row-cards">
  <div>
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-primary text-white">
          <h3 class="card-title"> @lang('orders.show_order_details') </h3>
        </div>
        <div class="card-body border-bottom py-3">
          <table class="table table-bordered table-hover">
            <tbody>
             <tr>
              <th> @lang('orders.created_at') </th>
              <td>{{ $order->created_at->toDateTimeString()  }} - <span class="text-muted"> {{ $order->created_at->diffForHumans()  }} </span>  </td>
            </tr>
            <tr>
              <th> @lang('orders.status') </th>
              <td>
                @switch($order->seen)
                @case(1)
                <span class="badge bg-green">@lang('orders.yes')</span>
                @break
                @case(0)
                <span class="badge bg-primary">@lang('orders.no')</span>
                @break
                @endswitch
              </td>
            </tr>
            <tr>
              <th> @lang('orders.code') </th>
              <td> {{ $order->code }} </td>
            </tr>
            <tr>
              <th> @lang('site.first_name') </th>
              <td> {{ $order->first_name }} </td>
            </tr>
            <tr>
              <th> @lang('site.last_name') </th>
              <td> {{ $order->last_name }} </td>
            </tr>
            <tr>
              <th> @lang('site.phone') </th>
              <td> {{ $order->mobile }} </td>
            </tr>
            <tr>
              <th> @lang('site.country') </th>
              <td> {{ optional($order->country)->name }} </td>
            </tr>
            <tr>
              <th> @lang('site.governorate') </th>
              <td> {{ optional($order->governorate)->name }} </td>
            </tr>
            <tr>
              <th> @lang('orders.address') </th>
              <td> {{ $order->address }} </td>
            </tr>
            <tr>
              <th> @lang('site.city') </th>
              <td> {{ $order->city }} </td>
            </tr>
            <tr>
              <th> @lang('orders.subtotal') </th>
              <td> {{ $order->subtotal }} </td>
            </tr>
            <tr>
              <th> @lang('orders.shipping_cost') </th>
              <td> {{ $order->shipping_cost }} </td>
            </tr>
            <tr>
              <th> @lang('orders.total') </th>
              <td> {{ $order->total }} </td>
            </tr>
            <tr>
              <th> @lang('site.comment') </th>
              <td> {{ $order->comment }} </td>
            </tr>
             <tr>
              <th> @lang('site.paymenth_status') </th>
              <td> 
               لم تم الدفع  بعد 
              </td>
            </tr>
            <tr>
              <th> @lang('site.paymenth_mthod') </th>
              <td> 
                @switch($order->payment_method)
                @case(1)
                <span  class='badge bg-primary' > @lang('site.cache_on_delivery')  </span>
                @break
                @default
                لم يتم تحديد طريقه الدفع بعد
                @endswitch
                
              </td>
            </tr>
            <tr>
              <th> @lang('orders.products') </th>
              <td> 
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th> @lang('orders.product') </th>
                      <th> @lang('orders.price') </th>
                      <th> @lang('orders.quantity') </th>
                    </tr>
                  </thead>
                  <tbody>

                   @foreach ($order->items as $item)
                   <tr>
                    <td> <a href="{{ route('dashboard.products.show' , ['product' => $item->product_id ]) }}"> {{ optional($item->product)->name }} </a> </td>
                    <td> {{ $item->price }} </td>
                    <td> {{ $item->quantity }} </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
@endsection