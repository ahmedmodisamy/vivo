@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

@extends('site.layouts.master')
@section('page_title')
@lang('site.my_account')
@endsection
@section('page_content')

  <div class="section">
    <div class="container">
        @include('dashboard.layouts.messages')

      <div class="row">

        <div class="col-lg-3">
          <div class="sidebar sticky-sidebar user-nav sidebar-left">
            <ul>
              <li> <a class="active" href="{{ route('account') }}"> @lang('site.edit_profile') </a> </li>
              <li> <a href="{{ route('user.orders') }}"> @lang('site.my_orders') </a> </li>
              <li> <a href="profile-products.html">Saved Products</a> </li>
              <li> <a href="profile-saved-products.html">My Downloads</a> </li>
              <li> <a class="logout" href="{{ route('user.logout') }}"><i class="flaticon-shut-down-button"></i> @lang('site.logout') </a> </li>
            </ul>
          </div>
        </div>

        <div class="col-lg-9">

          <div class="acr-welcome-message">
            <h3>Welcome Back, Randy Blue</h3>
          </div>

          <form method="POST" action="{{ route('account.update') }}" >
            <div class="row">
              @csrf
              @method('PATCH')
              <div class="col-lg-6 form-group">
                <label> @lang('site.first_name') </label>
                <input type="text" name='first_name' class="form-control" value="{{ $user->first_name }}" placeholder="@lang('site.first_name')" >
                @error('first_name')
                <p class='text-danger' > {{ $message }} </p>
                @enderror
              </div>
              <div class="col-lg-6 form-group">
                <label> @lang('site.last_name') </label>
                <input type="text" name='last_name' class="form-control" placeholder="@lang('site.last_name')" value="{{ $user->last_name }}">
                @error('last_name')
                <p class='text-danger' > {{ $message }} </p>
                @enderror
              </div>
              <div class="col-lg-6 form-group">
                <label> @lang('site.email') </label>
                <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                @error('email')
                <p class='text-danger' > {{ $message }} </p>
                @enderror
              </div>
              <div class="col-lg-6 form-group">
                <label> @lang('site.phone') </label>
                <input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
                @error('phone')
                <p class='text-danger' > {{ $message }} </p>
                @enderror
              </div>
              <div class="col-lg-6 form-group">
                <label> @lang('site.country') </label>
                <select name="country_id" id="input" class="form-control" >
                 @foreach ($countries as $country)
                  <option value="{{ $country->id }}"> {{ $country->getTranslation('name' , $lang) }} </option>
                @endforeach
                </select>
                @error('country_id')
                <p class='text-danger' > {{ $message }} </p>
                @enderror
              </div>
            </div>
            <button type="submit" name="submit" class="btn-custom"> @lang('site.save_changes') </button>
          </form>
          <hr>

         
         

        </div>

      </div>
    </div>
  </div>
@endsection