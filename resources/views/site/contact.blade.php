@extends('site.layouts.master')

@section('page_title' , 'Shop' )
@section('page_content')
<!-- Subheader Start -->
<div class="subheader bg-cover bg-center dark-overlay" style="background-image: url('{{ asset('assets/img/subheader.jpg') }}')">
  <div class="container">
    <div class="subheader-inner">
      <h1 class="text-white"> @lang('site.contact_us') </h1>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ url('/') }}"> <i class="fas fa-home"></i> </a></li>
          <li class="breadcrumb-item active" aria-current="page"> @lang('site.contact_us') </li>
        </ol>
      </nav>
    </div>
  </div>
</div>
<!-- Subheader End -->



<!-- Contact Start -->
<div class="section pt-0 pb-0">
  <div class="container">
    <div class="row">
      <div class="col-12 contact-group">
        @include('dashboard.layouts.messages')
        <div class="row">

          <div class="col-lg-6 mb-lg-30">
            <div class="acr-locations bg-bottom bg-norepeat" style="background-image: url('{{ asset('assets/img/misc/bldg.png') }}')">
              <img src="{{ asset('assets/img/contact.jpg') }}" alt="">
              <div class="row">             
                <div class="col-sm-12">
                  <div class="acr-location">
                    <h5> {{ $data['settings']->address }} </h5>
                    <div class="acr-location-address">
                      <p> {{ $data['settings']->email }} </p>
                      <p> {{ $data['settings']->working_hours }} </p>
                      <a href="tel:+123456789"> {{ $data['settings']->phone }} </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="section-title-wrap section-header">
              <h5 class="custom-primary"> @lang('site.contact_us') </h5>
              <h2 class="title"> @lang('site.Got_Any_Questions') </h2>
            </div>

            <div class="comment-form">
              <form method="POST" action="{{ route('contact.send') }}" >
                @csrf
                <div class="row">
                  <div class="col-md-6 form-group">
                    <label> @lang('site.name') </label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" value="{{ old('name') }}">
                    @error('name')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="col-md-6 form-group">
                    <label>@lang('site.subject')</label>
                    <input type="text" class="form-control"  name="subject" value="{{ old('subject') }}">
                  </div>
                  <div class="col-md-6 form-group">
                    <label> @lang('site.phone') </label>
                    <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}">
                    @error('phone')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="col-md-6 form-group">
                    <label> @lang('site.email') </label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror"  name="email" value="{{ old('email') }}">
                    @error('email')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="col-md-12 form-group">
                    <label> @lang('site.your_message') </label>
                    <textarea class="form-control @error('message') is-invalid @enderror" name="message" rows="7">
                      {{ old('message') }}
                    </textarea>
                    @error('message')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <button type="submit" class="btn-custom primary" name="button"> @lang('site.send_message') </button>
              </form>
            </div>
          </div>

        </div>

      </div>

    </div>
  </div>
  <!-- Start Map -->
  <div class="contact-map">
    
    <div id='map' style="width:100%;height: 400px;" ></div>
  </div>
  <!-- Start Map -->
</div>
<!-- Contact End -->

@endsection




@section('scripts')
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBuQymvDTcNgdRWQN0RhT2YxsJeyh8Bys4&amp;libraries=places">
</script>
<script>
  $(document).ready(function() {

    var latlng = new google.maps.LatLng({{ $data['settings']->lat }}, {{ $data['settings']->lng }} );
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      title: 'Set lat/lon values for this property',
      draggable: true
    });

    google.maps.event.addListener(marker, 'dragend', function (event) {
      document.getElementById("latitude").value = this.getPosition().lat();
      document.getElementById("longitude").value = this.getPosition().lng();
    });


  });

  
</script>
@endsection