@extends('site.layouts.master')

@section('page_title' , 'Shop' )
@section('page_content')


  <!-- About Section Start -->
  <div class="section">
    <div class="container">
      <div class="row align-items-center">


        <div class="col-lg-12">
          <div class="section-title-wrap mr-lg-30">
            <h2 class="title"> @lang('site.your_order_is_success') </h2>
            <p class="subtitle">
              
            </p>
    
            {{-- <a href="shop-map.html" class="btn-custom">Browse Jewelry   </a> --}}
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- About Section End -->

@endsection