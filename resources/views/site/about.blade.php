@extends('site.layouts.master')

@section('page_title' , 'Shop' )
@section('page_content')
      <!-- Subheader Start -->
  <div class="subheader bg-cover bg-center dark-overlay" style="background-image: url('{{ asset('assets/img/subheader.jpg') }}')">
    <div class="container">
      <div class="subheader-inner">
        <h1 class="text-white">About Us</h1>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"> <i class="fas fa-home"></i> </a></li>
            <li class="breadcrumb-item"><a href="#">Pages</a></li>
            <li class="breadcrumb-item active" aria-current="page">About Us</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
  <!-- Subheader End -->

  <!-- About Section Start -->
  <div class="section">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-lg-6 mb-lg-30 acr-dots-wrapper acr-single-img-wrapper">
          <img src="{{ asset('assets/img/products-list/3.jpg') }}" alt="img">
          <div class="acr-dots"></div>
        </div>
        <div class="col-lg-6">
          <div class="section-title-wrap mr-lg-30">
            <h5 class="custom-primary">About Us</h5>
            <h2 class="title">We have more than 100 years of experince in Jewelry.  </h2>
            <p class="subtitle">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </p>
            <p class="subtitle">
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
            </p>
            <a href="shop-map.html" class="btn-custom">Browse Jewelry   </a>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- About Section End -->

  <!-- Infographics Start -->
  <div class="section section-padding bg-cover bg-center bg-parallax dark-overlay dark-overlay-2" style="background-image: url('{{ asset('assets/img/banner/3.jpg') }}')">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="acr-infographic-item">
            <i class="flaticon-necklace"></i>
            <h4>24,934</h4>
            <p>Jewelry</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="acr-infographic-item">
            <i class="flaticon-sales-agent"></i>
            <h4>65,317</h4>
            <p>Agents</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="acr-infographic-item">
            <i class="flaticon-business-center"></i>
            <h4>4,658</h4>
            <p>Agencies</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="acr-infographic-item">
            <i class="flaticon-happy"></i>
            <h4>67,335</h4>
            <p>Happy Customers</p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- Infographics End -->

  <!-- Agents Start -->
  <div class="section section-padding agents">
    <div class="container">

      <div class="section-title-wrap section-header flex-header">
        <div class="section-title-text">
          <h5 class="custom-primary">Our Back bone</h5>
          <h2 class="title">Meet Our Team</h2>
        </div>
        <div class="acr-arrows primary-arrows">
          <i class="slider-prev fas fa-arrow-left slick-arrow"></i>
          <i class="slider-next fas fa-arrow-right slick-arrow"></i>
        </div>
      </div>

      <div class="agents-slider">
        <!-- Agent Start -->
        <div class="acr-agent">
          <div class="acr-dots-wrapper acr-agent-thumb">
            <div class="acr-dots"></div>
            <a href="team-details.html"><img src="{{ asset('assets/img/agents/1.jpg') }}" alt="agent"></a>
          </div>
          <div class="acr-agent-body">
            <h6> <a href="team-details.html">Randy Blue</a> </h6>
            <span>Expert at Company</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
            <a href="team-details.html" class="btn-custom secondary btn-sm">View Profile</a>
          </div>
        </div>
        <!-- Agent End -->

        <!-- Agent Start -->
        <div class="acr-agent">
          <div class="acr-dots-wrapper acr-agent-thumb">
            <div class="acr-dots"></div>
            <a href="team-details.html"><img src="{{ asset('assets/img/agents/2.jpg') }}" alt="agent"></a>
          </div>
          <div class="acr-agent-body">
            <h6> <a href="team-details.html">Rinda Flow</a> </h6>
            <span>Expert at Company</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
            <a href="team-details.html" class="btn-custom secondary btn-sm">View Profile</a>
          </div>
        </div>
        <!-- Agent End -->

        <!-- Agent Start -->
        <div class="acr-agent">

          <div class="acr-dots-wrapper acr-agent-thumb">
            <div class="acr-dots"></div>
            <a href="team-details.html"><img src="{{ asset('assets/img/agents/1.jpg') }}" alt="agent"></a>
          </div>
          <div class="acr-agent-body">
            <h6> <a href="team-details.html">Gina Mconihon</a> </h6>
            <span>Expert at Company</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
            <a href="team-details.html" class="btn-custom secondary btn-sm">View Profile</a>
          </div>
        </div>
        <!-- Agent End -->

        <!-- Agent Start -->
        <div class="acr-agent">
          <div class="acr-dots-wrapper acr-agent-thumb">
            <div class="acr-dots"></div>
            <a href="team-details.html"><img src="{{ asset('assets/img/agents/2.jpg') }}" alt="agent"></a>
          </div>
          <div class="acr-agent-body">
            <h6> <a href="team-details.html">Oliver Rasky</a> </h6>
            <span>Expert at Company</span>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
            <a href="team-details.html" class="btn-custom secondary btn-sm">View Profile</a>
          </div>
        </div>
        <!-- Agent End -->


      </div>

    </div>
  </div>
  <!-- Agents End -->

  <!-- Video Start -->
  <div class="section light-bg section-img-wrapper">
    <div class="section-imgs">
      <img src="{{ asset('assets/img/megamenu.png') }}" alt="img">
      <img src="{{ asset('assets/img/megamenu2.png') }}" alt="img">
    </div>
    <div class="container">
      <div class="section-title-wrap text-center">
        <h5 class="custom-primary">Our Ways</h5>
        <h2 class="title">Visit Our Store , We Are Offline And Online .</h2>
        <p class="subtitle">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        </p>
        <a href="https://www.youtube.com/watch?v=TKnufs85hXk&t=1s" class="btn-custom popup-youtube"> <i class="m-0 fas fa-play"></i> </a>
      </div>
    </div>
  </div>
  <!-- Video End -->

  <!-- Testimonials Start -->
  <div class="section section-padding">
    <div class="container">
      <div class="section-title-wrap section-header">
        <h5 class="custom-primary">Testimonials</h5>
        <h2 class="title">What Are People Saying</h2>
      </div>

      <div class="row">

        <!-- Testimonail item start -->
        <div class="col-lg-4 col-md-6">
          <div class="acr-testimonial">
            <div class="acr-testimonial-body">
              <h5>Perfect service</h5>
              <div class="acr-rating-wrapper">
                <div class="acr-rating">
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                </div>
              </div>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
            </div>
            <div class="acr-testimonial-author">
              <img src="{{ asset('assets/img/people/1.jpg') }}" alt="testimonial">
              <div class="acr-testimonial-author-inner">
                <h6>John Mishlen</h6>
                <span>Executive CEO at company</span>
              </div>
            </div>
          </div>
        </div>
        <!-- Testimonail item end -->

        <!-- Testimonail item start -->
        <div class="col-lg-4 col-md-6">
          <div class="acr-testimonial">
            <div class="acr-testimonial-body">
              <h5>Competitive prices</h5>
              <div class="acr-rating-wrapper">
                <div class="acr-rating">
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                </div>
              </div>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
            </div>
            <div class="acr-testimonial-author">
              <img src="{{ asset('assets/img/people/2.jpg') }}" alt="testimonial">
              <div class="acr-testimonial-author-inner">
                <h6>Moe Sondi</h6>
                <span>Executive CEO at company</span>
              </div>
            </div>
          </div>
        </div>
        <!-- Testimonail item end -->

        <!-- Testimonail item start -->
        <div class="col-lg-4 col-md-12">
          <div class="acr-testimonial">
            <div class="acr-testimonial-body">
              <h5>Great agents</h5>
              <div class="acr-rating-wrapper">
                <div class="acr-rating">
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                  <i class="fas fa-star active"></i>
                </div>
              </div>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
            </div>
            <div class="acr-testimonial-author">
              <img src="{{ asset('assets/img/people/3.jpg') }}" alt="testimonial">
              <div class="acr-testimonial-author-inner">
                <h6>Mandy Floss</h6>
                <span>Executive CEO at company</span>
              </div>
            </div>
          </div>
        </div>
        <!-- Testimonail item end -->

      </div>

    </div>
  </div>
  <!-- Testimonials End -->
@endsection