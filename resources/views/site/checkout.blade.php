@extends('site.layouts.master')

@section('page_title' , 'Shop' )
@section('page_content')
<div class="section checkout-sec">
	<div class="container">
		<form method="post" action="{{ route('checkout.store') }}" > 
			@csrf
			<div class="row">
				<div class="col-xl-7">
					<div class="checkout-form">
						<h4> @lang('site.shipping_details') </h4>
						<div class="row">
							<div class="col-xl-6 form-group">
								<input type="text" class="form-control" placeholder="@lang('site.first_name')" name="first_name" value="{{ optional(Auth::user())->first_name }}">
								@error('first_name')
								<p class='text-danger'> {{ $message }} </p>
								@enderror
							</div>
							
							<div class="col-xl-6 form-group">
								<input type="text" class="form-control" placeholder="@lang('site.last_name')" name="last_name" value="{{ optional(Auth::user())->last_name }}">
								@error('last_name')
								<p class='text-danger'> {{ $message }} </p>
								@enderror
							</div>
							<div class="col-xl-12 form-group">
								<select name='country_id' class="form-control">
									@foreach ($countries as $country)
									<option value="{{ $country->id }}" {{ optional(Auth::user())->country_id == $country->id ? 'selected="selected"' : '' }} > {{ $country->name }} </option>
									@endforeach
								</select>
							</div>
							<div class="col-xl-12 form-group">
								<select name='governorate_id' class="form-control">
									<option value="">  @lang('site.governorate') </option>
									@foreach ($governorates as $governorate)
									<option value="{{ $governorate->id }}"> {{ $governorate->name }} </option>
									@endforeach
								</select>
								@error('governorate_id')
								<p class='text-danger'> {{ $message }} </p>
								@enderror
							</div>
							

							<div class="col-xl-12 form-group">
								<input type="text" class="form-control" value='{{ old('city') }}' placeholder="@lang('site.city') *"  name="city"  >
								@error('city')
								<p class='text-danger'> {{ $message }} </p>
								@enderror
							</div>
							<div class="col-xl-12 form-group">
								<input type="text" class="form-control" name="address" placeholder="@lang('site.Street_Address')*" value="{{ old('address') }}" >
							</div>
							<div class="col-xl-6 form-group">
								<input type="number" class="form-control" name="phone" value="{{ optional(Auth::user())->phone }}" required="" placeholder="@lang('site.phone_number') *" >
								@error('phone')
								<p class='text-danger'> {{ $message }} </p>
								@enderror
							</div>
							<div class="col-xl-6 form-group">
								<input type="email" class="form-control"  placeholder="@lang('site.email')" name="email" value="{{ optional(Auth::user())->email }}" >
								@error('email')
								<p class='text-danger'> {{ $message }} </p>
								@enderror
							</div>
							<div class="col-xl-12 mb-0 form-group">
								<textarea class="form-control" placeholder="@lang('site.commnt')" name="comment" rows="7"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-5 space-top">
					<div class="checkout-table">
						<table>
							<thead>
								<tr>
									<th> @lang('site.product') </th>
									<th> @lang('site.quantity') </th>
									<th> @lang('site.total') </th>
								</tr>
							</thead>
							<tbody>
								
								@foreach (session('cart.items') as $item)
								<tr>
									<td data-title="product">
										<div class="product-name">
											<h6>
												<a href="{{ route('products.show' , ['product' => $item['id'] ] ) }}"> {{ $item['name'] }} </a>
											</h6>
											<p>{{ $item['quantity'] }} Piece</p>
										</div>
									</td>
									<td data-title="Quantity">x {{ $item['quantity'] }} </td>
									<td data-title="Total">
										<strong> {{ ($item['price'] * $item['quantity']) }} @lang('site.da') </strong>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="row">
						{{-- <div class="col-xl-12">
							<div class="coupen-code-box">
								<div class="form-group">
									<p> @lang('site.if_you_have') </p>
									<input type="text" class="form-control" placeholder="Coupen Code" name="coupen">
								</div>
								<button type="button" class="btn-custom primary">Apply Code</button>
							</div>
						</div> --}}

						<div class="col-xl-12 form-group">
							<label> @lang('site.payment_method')
								<span class="text-danger">*</span>
							</label>
							<select name="payment_method" class="form-control">
								<option value="1"> @lang('site.cach_on_delivery') </option>
							</select>
						</div>

						
{{-- 						<div class="col-xl-12 form-group">
							<label>Full Name</label>
							<input type="text" class="form-control" placeholder="Full Name" name="full-name" value="">
						</div>
						<div class="col-xl-6 form-group">
							<label>Expiry Date</label>
							<input type="text" class="form-control" placeholder="Expiry Date (MM/YY)" name="exp-date" value="">
						</div>
						<div class="col-xl-6 form-group">
							<label>CVV*</label>
							<input type="number" class="form-control" placeholder="CVV" name="cvv-no" value="">
						</div>
						<div class="col-xl-12">
							<p class="small">
								Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our
								<a href="legal.html" class="btn-link">Privacy Policy</a>
							</p>
						</div> --}}
						<button type="submit" class="btn-custom primary btn-block"> @lang('site.Proceed_to_Checkout') </button>

					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Checkout End -->
@endsection