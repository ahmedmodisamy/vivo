@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
@extends('site.layouts.master')
@section('page_title' , 'Home' )
@section('page_content')
<!-- Banner Start -->


@foreach ($slides as $slide)
<div class="banner  bg-cover bg-center"  style="background-size:  contain; height: 100%; width: 100%;  background-repeat: no-repeat;;background-image: url('{{ Storage::url('slides/'.$slide->image) }}')">
  <div class="container">

    <div class="banner-item">
      <div class="banner-inner">
        <div class="banner-text">
          <h1 class="title"> {{ $slide->getTranslation('title' , $lang) }} </h1>
          <p class="subtitle"> {{ $slide->getTranslation('subtitle' , $lang) }} </p>
        </div>
      </div>
    </div>

  </div>

  <div class="container d-none d-lg-block">
    <div class="row">
      <div class="col-md-8">
        <div class="section-title-wrap dark-bg" style="visibility:hidden;">
          <h5 class="text-white custom-primary">Jewelry</h5>
          <h2 class="text-white title"> <span class="custom-primary">Free Delivery on orders over $500.</span> </h2>
          <p class="text-white subtitle">Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices</p>
        </div>
      </div>
    </div>
  </div>
 
</div>
@endforeach
<!-- Banner End -->
{{-- 

<div class="section section-padding"  style='background-color:black;' >
  <div class="spacer spacer-lg dark-bg"></div>

  <div class="container">
    <div class="products-slider-center">


      @foreach ($featured_products as $featured_product)
      <div class="product">
        <div class="product-thumbnail">
          <a href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}"><img src="{{ Storage::url('products/'.$featured_product->image) }}" alt="product"></a>
          <div class="product-badges">
            @if ($featured_product->featured)
            <span class="product-badge featured"> <i class="fas fa-star"></i> </span>
            @endif
            @if ($featured_product->stock == 0)
            <span class="product-badge stock">  @lang('site.out_of_stock') </span>
            @endif
          </div>
          <div class="product-controls">
            <a href="#" class="favorite"><i class="far fa-heart"></i></a>
            <a href="#" data-product_id="{{ $featured_product->id }}" class="quick-view"><i class="fas fa-eye"></i></a>
          </div>
        </div>
        <div class="product-body">
          <h5 class="product-title"> <a href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}" title="Blue Blast">{{ $featured_product->getTranslation('name' , $lang) }}</a> </h5>
          <div class="acr-rating">
            <i class="fas fa-star "></i>
            <i class="fas fa-star "></i>
            <i class="fas fa-star "></i>
            <i class="fas fa-star "></i>
            <i class="fas fa-star "></i>
          </div>
          <span class="product-price"> {{ $featured_product->price }} @lang('site.da') </span>
          <p class="product-text">{{ $featured_product->getTranslation('mini_description' , $lang) }}</p>
          <div class="product-gallery-wrapper">
            <a  href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}" class="btn-custom btn-sm secondary add_to_cart" data-product_id="{{ $featured_product->id }}" > @lang('site.add_to_cart') </a>
            <a  href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}" class="btn-custom btn-sm secondary " > @lang('site.view_product') </a>
            
          </div>
        </div>
      </div>

      @endforeach
    </div>
  </div>

</div>

 --}}



<div class="section section-padding" style='background-color:black;'> 
  <div class="container-fluid">

    <div class="section-title-wrap section-header">

      {{-- <h2 class="title custom-primary"> @lang('site.new_arrival') </h2> --}}
    </div>

    <div class="row">



      <div class="col-lg-12 ">

        <div class="row">       


          @foreach ($featured_products as $featured_product)
          <div class=" col-lg-2 col-6 ">
            <div class="product">
              <div class="product-thumbnail">
                <a href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}"><img src="{{ Storage::url('products/'.$featured_product->image) }}" alt="product"></a>
                <div class="product-badges  d-md-block d-sm-block">
                  @if ($featured_product->stock == 0)
                  <span class="product-badge stock"> @lang('site.out_of_stock') </span>
                  @endif
                </div>
                <div class="product-controls d-none d-sm-block">
                  <a href="#" class="favorite  "><i class="far fa-heart"></i></a>

                  <a href="#" data-product_id="{{ $featured_product->id }}" class="quick-view "><i class="fas fa-eye"></i></a>
                </div>
              </div>
              <div class="product-body">
                <h5 class="product-title"> <a href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}" title="{{ $featured_product->name }}">{{ $featured_product->name }}</a> </h5>
                <div class="acr-rating">
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                </div>
                @if ($featured_product->hasSale())
                <span class="product-price text-bold" style='font-weight: bold; font-size: 28px;' > {{ $featured_product->price() }} @lang('site.da') <span style='font-weight: bold; font-size: 28px;' >{{ $featured_product->oldPrice() }}  @lang('site.da') </span> </span>
                @else
                <span class="product-price text-bold" style='font-weight: bold; font-size: 28px !important;' > {{ $featured_product->price }} @lang('site.da') </span>
                @endif
                <p class="product-text"> {{ $featured_product->getTranslation('mini_description' , $lang) }} </p>
                <div class="product-gallery-wrapper">
                  <a href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}" class="btn-custom btn-sm secondary add_to_cart" data-product_id="{{ $featured_product->id }}" >@lang('site.add_to_cart') </a>
                  <a href="{{ route('products.show' , ['product' => $featured_product->id , '-' , $featured_product->slug] ) }}" class="btn-custom btn-sm secondary d-none  d-md-block d-sm-block" >@lang('site.view_product') </a>
                  
                </div>
              </div>
            </div>
          </div>
          @endforeach  

          


        </div>

      </div>
  

    </div>

  </div>
</div>



{{-- <div class="section section-padding" style='background-color:black;'>
  <div class="container-fluid">

    <div class="section-title-wrap section-header">

      <h2 class="title custom-primary"> @lang('site.best_selling') </h2>
    </div>

    <div class="row">


      <div class="col-lg-12">

        <div class="row">

          @foreach ($best_selling as $best_selling_product)

          <div class="col-lg-2">
            <div class="product">
              <div class="product-thumbnail">
                <a href="{{ route('products.show' , ['product' => $best_selling_product->id , '-' , $best_selling_product->slug] ) }}"><img src="{{ Storage::url('products/'.$best_selling_product->image) }}" alt="product"></a>
                <div class="product-badges">
                  @if ($best_selling_product->featured)
                  <span class="product-badge featured"> <i class="fas fa-star"></i> </span>
                  @endif
                  @if ($best_selling_product->hasSale())
                  <span class="product-badge stock"> Out of Stock</span>
                  @endif
                </div>
                <div class="product-controls">
                  <a href="#" class="favorite"><i class="far fa-heart"></i></a>
                  <a href="#" class="compare"><i class="fas fa-sync-alt"></i></a>
                  <a href="#" data-toggle="modal" data-target="#quickViewModal" class="quick-view"><i class="fas fa-eye"></i></a>
                </div>
              </div>
              <div class="product-body">
                <h5 class="product-title"> <a href="{{ route('products.show' , ['product' => $best_selling_product->id , '-' , $best_selling_product->slug] ) }}" title="Blue Blast"> {{ $best_selling_product->getTranslation('name' , $lang) }} </a> </h5>
                <div class="acr-rating">
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                  <i class="fas fa-star "></i>
                </div>
                <span class="product-price"> {{ $best_selling_product->price }} @lang('site.da') </span>
                <p class="product-text"> {{ $best_selling_product->getTranslation('mini_description' , $lang) }} </p>
                <div class="product-gallery-wrapper">
                  <a href="{{ route('products.show' , ['product' => $best_selling_product->id , '-' , $best_selling_product->slug] ) }}" class="btn-custom btn-sm secondary add_to_cart" data-product_id="{{ $best_selling_product->id }}" > @lang('site.add_to_cart') </a>
                  <a href="{{ route('products.show' , ['product' => $best_selling_product->id , '-' , $best_selling_product->slug] ) }}" class="btn-custom btn-sm secondary " > @lang('site.view_product') </a>
                  
                </div>
              </div>
            </div>
          </div>

          @endforeach

        </div>

      </div>
    </div>

  </div>
</div> --}}
@endsection

@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  $(document).ready(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })




    $(document).on('click', 'a.quick-view', function(event) {
      event.preventDefault();
      var modal = $('#quickViewModal');
      var product_id = $(this).attr('data-product_id');
      $.ajax({
        url: '{{ route('get_product_details') }}',
        type: 'GET',
        dataType: 'json',
        data: {product_id:product_id},
      })
      .done(function(data) {
        console.log(data.data.image)
        modal.find('.product-zoom-image img').attr('src' , data.data.image );
        modal.find('.shop-detail-title h3').text(data.data.name );
        modal.find('.product-price-box span.product-price ').text(data.data.price );
        modal.find('.product-descr p.mb-0 ').text(data.data.mini_description );
        modal.find('a.add_to_cart ').attr(  'data-product_id'  ,  data.data.id );    
        modal.modal('show');
      });
    });


    $(document).on('click', 'a.add_to_cart', function(event) {
      event.preventDefault();
      var product_id = $(this).attr('data-product_id');
      // console.log(product_id)
      $.ajax({
        url: '{{ route('cart.store') }}',
        type: 'POST',
        dataType: 'json',
        data: {product_id:product_id , _token:'{{ csrf_token() }}' , quantity:1 },
      })
      .done(function(data) {
        Toast.fire({
          icon: data.status,
          title: data.message
        });
          Livewire.emit('itemAddedToCart');
      });
      
    });
  });
</script>
@endsection