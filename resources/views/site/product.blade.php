@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
@extends('site.layouts.master')
@section('page_title' , 'Shop' )
@section('page_content')


<!-- Shop Detail Start -->

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="detail-slider-wrapper">
          <div class="detail-page-slider-nav-1">
            <div class="slide-item">
              <a href="javascript:void(0)">
                <img src="{{ Storage::url('products/'.$product->image) }}" class="full-width" alt="image">
              </a>
            </div>
            @foreach ($product->images as $image)
            <div class="slide-item">
              <a href="javascript:void(0)">
                <img src="{{ Storage::url('products/'.$image->image) }}" class="full-width" alt="image">
              </a>
            </div>
            @endforeach
          </div>
          <div class="detail-page-slider-1">
            <div class="slide-item">
              <div class="product-zoom-image">
                <img src="{{ Storage::url('products/'.$product->image) }}" alt="image">
              </div>
            </div>      
            @foreach ($product->images as $image)
            <div class="slide-item">
              <div class="product-zoom-image">
                <img src="{{ Storage::url('products/'.$image->image) }}" alt="image">
              </div>
            </div>
            @endforeach

          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="shop-detail-wrapper">
          <div class="shop-social-icons">
            <a href="#" class="add-to-favorite" data-toggle="tooltip" title="Add to Wishlist">
              <i class="far fa-heart"></i>
            </a>
            <ul class="icon-box">
              <li class="mt-0">
                <a href="#" data-toggle="tooltip" title="Share on facebook">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="mt-0">
                <a href="#" data-toggle="tooltip" title="Share on twitter">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="mt-0">
                <a href="#" data-toggle="tooltip" title="Share on Linkedin">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>

          <div class="shop-detail-title">
            <h3 class="mb-0">{{ $product->getTranslation('name' , $lang) }}</h3>
          </div>
          <div class="rating d-flex">
            <div class="acr-rating">
              <i class="fas fa-star "></i>
              <i class="fas fa-star "></i>
              <i class="fas fa-star "></i>
              <i class="fas fa-star "></i>
              <i class="fas fa-star "></i>
            </div>
            <span>({{ $product->reviews()->count() }}) @lang('site.reviews') </span>
          </div>
          <div class="product-price-box">
            <span class="product-price" style="font-weight: bold;font-size: 26px" >{{ $product->price() }} @lang('site.da')
             
             @if ($product->hasSale())
             <span style="font-weight: bold;font-size: 26px" > {{ $product->oldPrice() }} @lang('site.da') </span>
             @endif
           </span>
         </div>
         <div class="product-descr">
          <p class="mb-0"> {{ $product->getTranslation('mini_description' , $lang) }} </p>
        </div>
        <form class="product-variation-form">
          <div class="product-variation-wrapper">

            <div class="variation-item">
            </div>

          </div>
          <div class="shop-button-box">
            <div class="qty-box">
              <span class="qty-subtract">
                <i class="fa fa-minus"></i>
              </span>
              <input type="text" name="qty" value="1">
              <span class="qty-add">
                <i class="fa fa-plus"></i>
              </span>
            </div>
            <a href="#" class="btn-custom primary add_to_cart" data-product_id="{{ $product->id }}" > @lang('site.add_to_cart') </a>
          </div>
        </form>
        <ul class="product-extra-info">
          <li>
            <span> @lang('site.category') : </span>
            <div class="product-info-item">
              <a href="#"> {{ optional($product->category)->name }} </a>
            </div>
          </li>
{{--             <li>
              <span>Tags: </span>
              <div class="product-info-item">
                <a href="#">Scacco Diamond Ring</a>,
                <a href="#">Pearl and Diamond Necklace </a>,
                <a href="#">Regular Men Blue Jewelry</a>
              </div>
            </li>
            --}}
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section pt-0">
  <div class="container">
    <div class="product-additional-info">
      <div class="row">
        <div class="col-lg-4">
          <ul class="nav product-sticky-sec" id="bordered-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="tab-product-desc-tab" data-toggle="pill" href="#tab-product-desc" role="tab" aria-controls="tab-product-desc" aria-selected="true"> @lang('site.description') </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="tab-product-reviews-tab" data-toggle="pill" href="#tab-product-reviews" role="tab" aria-controls="tab-product-reviews" aria-selected="false"> @lang('site.reviews') ({{ $product->reviews()->count() }})</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-8">
          <div class="tab-content" id="bordered-tabContent">
            <div class="tab-pane fade show active" id="tab-product-desc" role="tabpanel" aria-labelledby="tab-product-desc-tab">
              <h4>@lang('site.description')</h4>
              {!! $product->getTranslation('description' , $lang) !!}
            </div>
            <div class="tab-pane fade" id="tab-product-info" role="tabpanel" aria-labelledby="tab-product-info-tab">
              <h4>Additional Information</h4>
              <table>
                <thead>
                  <tr>
                    <th scope="col">Attributes</th>
                    <th scope="col">Values</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <strong>Color</strong>
                    </td>
                    <td>blue, red, yellow, green</td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Refurbished Pave Diamond Star Charm </strong>
                    </td>
                    <td>wood, plastic, stainless steel</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="tab-pane fade" id="tab-product-reviews" role="tabpanel" aria-labelledby="tab-product-reviews-tab">
              <div class="comment-form">
                <h4>Leave a Review</h4>
                <form method="post">
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <label>Full Name</label>
                      <input type="text" class="form-control" placeholder="Full Name" name="fname" value="">
                    </div>
                    <div class="col-md-6 form-group">
                      <label>Email Address</label>
                      <input type="email" class="form-control" placeholder="Email Address" name="email" value="">
                    </div>
                    <div class="col-md-12 form-group">
                      <label>Your Message</label>
                      <textarea class="form-control" placeholder="Type your comment..." name="comment" rows="7"></textarea>
                    </div>
                    <div class="col-md-12 form-group">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="sendNotifications">
                        <label class="custom-control-label fw-400" for="sendNotifications">Notify me when I receive a reply to my comment</label>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn-custom primary" name="button">Post Review</button>
                </form>
              </div>
              <div class="comments-list">
                <ul>
                  <li class="comment-item">
                    <img src="{{ asset('assets/img/people/1.jpg') }}" alt="comment author">
                    <div class="comment-body">
                      <h5>Randy Blue</h5>
                      <span>Posted on: January 13 2020</span>
                      <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</p>
                      <a href="#" class="reply-link"> Reply </a>
                    </div>
                    <ul>
                      <li class="comment-item">
                        <img src="{{ asset('assets/img/people/3.jpg') }}" alt="comment author">
                        <div class="comment-body">
                          <h5>Melany frank</h5>
                          <span>Posted on: January 13 2020</span>
                          <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches</p>
                          <a href="#" class="reply-link"> Reply </a>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li class="comment-item">
                    <img src="{{ asset('assets/img/people/2.jpg') }}" alt="comment author">
                    <div class="comment-body">
                      <h5>Heather Mclayn</h5>
                      <span>Posted on: January 13 2020</span>
                      <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches</p>
                      <a href="#" class="reply-link"> Reply </a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Shop Detail End -->

<!-- Related Products Start -->

<div class="section section-padding pt-0 products">
  <div class="container">

    <div class="section-title-wrap section-header flex-header">
      <div class="section-title-text">

        <h2 class="title custom-primary"> @lang('site.related_products') </h2>
      </div>
      <div class="acr-arrows primary-arrows">
        <i class="slider-prev fas fa-arrow-left slick-arrow"></i>
        <i class="slider-next fas fa-arrow-right slick-arrow"></i>
      </div>
    </div>

    <div class="products-slider">


     @foreach ($related_products as $one_product)
     <div class="product">
      <div class="product-thumbnail">
        <a href="{{ route('products.show' , ['product' => $one_product->id ,  'slug' => $one_product->slug] ) }}"><img src="{{ Storage::url('products/'.$one_product->image) }}" alt="product"></a>
        <div class="product-badges">
         @if ($one_product->hasSale())
         <span class="product-badge sale"> @lang('site.on_sale') </span>
         @endif
       </div>
       <div class="product-controls">
        <a href="#" class="favorite"><i class="far fa-heart"></i></a>
        <a href="#" data-toggle="modal" data-target="#quickViewModal" class="compare"><i class="fas fa-eye"></i></a>
      </div>
    </div>
    <div class="product-body">
      <h5 class="product-title"> <a href="{{ route('products.show' , ['product' => $one_product->id] ) }}" title="Blue Blast"> {{ $one_product->getTranslation('name' , $lang) }} </a> </h5>
      <div class="acr-rating">
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
        <i class="fas fa-star"></i>
      </div>
      @if ($one_product->hasSale())
      <span class="product-price text-bold" style='font-weight: bold; font-size: 28px;' > {{ $one_product->price() }} @lang('site.da') <span style='font-weight: bold; font-size: 28px;' >{{ $one_product->oldPrice() }}  @lang('site.da') </span> </span>
      @else
      <span class="product-price text-bold" style='font-weight: bold; font-size: 28px !important;' > {{ $one_product->price }} @lang('site.da') </span>
      @endif
      <p class="product-text">{{ $one_product->getTranslation('mini_description' , $lang) }}</p>
      <div class="product-gallery-wrapper">
        <a href="{{ route('products.show' , ['product' => $one_product->id , 'slug' => $one_product->slug ] ) }}" class="btn-custom btn-sm secondary add_to_cart" data-product_id="{{ $one_product->id }}" > @lang('site.add_to_cart') </a>
        <a href="{{ route('products.show' , ['product' => $one_product->id , 'slug' => $one_product->slug ] ) }}" class="btn-custom btn-sm secondary"> @lang('site.view_product') </a>
      </div>
    </div>
  </div>
  @endforeach
</div>
</div>
</div>

@endsection

@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  $(document).ready(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })


    $(document).on('click', 'a.add_to_cart', function(event) {
      event.preventDefault();
      var product_id = $(this).attr('data-product_id');
      // console.log(product_id)
      var qty = $('input[name="qty"]').val();
      $.ajax({
        url: '{{ route('cart.store') }}',
        type: 'POST',
        dataType: 'json',
        data: {product_id:product_id , _token:'{{ csrf_token() }}' , quantity:qty },
      })
      .done(function(data) {
        Toast.fire({
          icon: data.status,
          title: data.message
        })
      });
      
    });
  });
</script>
@endsection