@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Vivo Perfumes </title>

  <!-- Vendor Stylesheets -->
  <link rel="stylesheet" href="{{ Storage::url('assets/css/plugins/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ Storage::url('assets/css/all.css') }}" />
  
  <link rel="stylesheet" href="{{ Storage::url('assets/css/plugins/animate.min.css') }}">
  {{-- <link rel="stylesheet" href="{{ Storage::url('assets/css/plugins/magnific-popup.css') }}"> --}}
  <link rel="stylesheet" href="{{ Storage::url('assets/css/plugins/slick.css') }}">
  <link rel="stylesheet" href="{{ Storage::url('assets/css/plugins/slick-theme.css') }}">
  <link rel="stylesheet" href="{{ Storage::url('assets/css/plugins/select2.min.css') }}">

  <!-- Icon Fonts -->
  <link rel="stylesheet" href="{{ Storage::url('assets/fonts/flaticon/flaticon.css') }}">
  <link rel="stylesheet" href="{{ Storage::url('assets/fonts/font-awesome/font-awesome.min.css') }}">

  <!-- Mini Style sheet -->
  <link rel="stylesheet" href="{{ Storage::url('assets/css/style.css') }}">

  @if ($lang == 'en')
    <link rel="stylesheet" href="{{ Storage::url('assets/css/navbar.css') }}" />
  @endif

  @if ($lang == 'ar')
  <link rel="stylesheet" href="{{ Storage::url('assets/css/style-rtl.css') }}">
    <link rel="stylesheet" href="{{ Storage::url('assets/css/navbar-ar.css') }}" />

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@400;600;700&display=swap" rel="stylesheet"> 
  <style>
    a , p , h1 , h2 , h3 , h4 , h5 , h6 , span , div  , table  , th , tr , td {

      font-family: 'Cairo', sans-serif !important;
      font-weight: bold;
    }

    .acr-header.header-style-4 .middle-header {
      padding:  0px !important;
    }


    @media (max-width: 500px) {
     .col-lg-2 ,  .col-6 {
      padding-left: 5px;
    }
    .product {
      padding: 8px;
    }

    .product .product-title a {
      font-weight: bold;
      font-size: 12px !important;
    }
  }

</style>
@endif

<!-- Favicon -->
<link rel="icon" type="{{ Storage::url('image/png') }}" sizes="32x32" href="favicon.ico">
@livewireStyles
@yield('styles')


<style>
  .whatsapp{
    background-color: transparent;
    width: auto;
    height: 50px;
    position: fixed;
    left: 2%;
    bottom: 15px;
    z-index: 9999;
  }
</style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5T97ZG8');</script>
<!-- End Google Tag Manager -->
</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T97ZG8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <!-- Preloader Start -->
  {{-- <div class="acr-preloader">
    <div class="acr-preloader-inner">
      <div class="lds-grid">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div> --}}
  <!-- Preloader End -->
  @include('site.layouts.header')

  @yield('page_content')
  <!-- Clients End -->
  @include('site.layouts.footer')

  <!-- Quick View Modal Start -->
  <div class="modal fade quick-view-modal" id="quickViewModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="close-btn close-dark close" data-dismiss="modal">
            <span></span>
            <span></span>
          </div>
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-5">
                <div class="product-zoom-image">
                  <img src="assets/img/quick-view.png" alt="image">
                </div>
              </div>
              <div class="col-md-7">
                <div class="shop-detail-wrapper">
                  <div class="shop-social-icons">
                    <a href="#" class="add-to-favorite" data-toggle="tooltip" title="Add to Wishlist">
                      <i class="far fa-heart"></i>
                    </a>
                    <ul class="icon-box">
                      <li class="mt-0">
                        <a href="#" data-toggle="tooltip" title="Share on facebook">
                          <i class="fab fa-facebook-f"></i>
                        </a>
                      </li>
                      <li class="mt-0">
                        <a href="#" data-toggle="tooltip" title="Share on twitter">
                          <i class="fab fa-twitter"></i>
                        </a>
                      </li>
                      <li class="mt-0">
                        <a href="#" data-toggle="tooltip" title="Share on Linkedin">
                          <i class="fab fa-linkedin-in"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="rating d-flex">
                    <div class="acr-rating">
                      <i class="fas fa-star active"></i>
                      <i class="fas fa-star active"></i>
                      <i class="fas fa-star active"></i>
                      <i class="fas fa-star active"></i>
                      <i class="fas fa-star active"></i>
                    </div>
                    <span>2 reviews</span>
                  </div>
                  <div class="shop-detail-title">
                    <h3>Square Scacco Diamond Ring</h3>
                  </div>
                  <div class="product-price-box">
                    <span class="product-price">
                      {{-- <span>4,500$</span> --}}
                    </span>
                  </div>
                  <div class="product-descr">
                    <p class="mb-0">Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
                  </div>
                  <form class="product-variation-form">
                    <div class="shop-button-box">
                      <a href="#" data-product_id='' class="btn-custom primary add_to_cart"> @lang('site.add_to_cart') </a>
                      <div class="qty-box">
                        <span class="qty-subtract">
                          <i class="fa fa-minus"></i>
                        </span>
                        <input type="text" name="qty" value="1">
                        <span class="qty-add">
                          <i class="fa fa-plus"></i>
                        </span>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Quick View Modal End -->

  <!-- Vendor Scripts -->
  <script src="{{ Storage::url('assets/js/plugins/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/navbar.js') }}"></script>

  <script src="{{ Storage::url('assets/js/plugins/popper.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/waypoint.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/bootstrap.min.js') }}"></script>
  {{-- <script src="{{ Storage::url('assets/js/plugins/jquery.magnific-popup.min.js') }}"></script> --}}
  <script src="{{ Storage::url('assets/js/plugins/jquery.slimScroll.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/imagesloaded.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/jquery.steps.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/jquery.countdown.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/isotope.pkgd.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/slick.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/select2.min.js') }}"></script>
  <script src="{{ Storage::url('assets/js/plugins/jquery.zoom.min.js') }}"></script>
  @livewireScripts
  <!-- Mini Scripts -->
  @if ($lang == 'ar')
  <script src="{{ Storage::url('assets/js/main-rtl.js') }}"></script>
  @else
  <script src="{{ Storage::url('assets/js/main.js') }}"></script>
  @endif
  @yield('scripts')

</body>

</html>
