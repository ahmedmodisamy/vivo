@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

<div class="whatsapp">
  <a href="https://wa.me/9710545387073" target="-blank">
    <img src="{{ Storage::url('assets/img/whatsapp-icon-square.svg') }}" width="50px" height="50px">
  </a>
</div>  


<!-- Footer Start -->
<footer class="acr-footer footer-2">

  <!-- Footer Middle Start -->
  <div class="footer-middle">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 ">
          <h5 class="widget-title"> @lang('site.information') </h5>
          <ul>
            @foreach ($data['pages'] as $page)
            <li> <a href="{{ route('pages.show' , ['page' => $page->id ]) }}">{{ $page->getTranslation('title' , $lang) }}</a> </li>
            @endforeach
          </ul>
        </div>

        <div class="col-lg-3">

         <h5 class="widget-title"> @lang('site.categories') </h5>
         <ul>
          @foreach ($data['categories'] as $category)
          <li> <a href="{{ route('shop' , ['category' => $category->id  ] ) }}">{{ $category->getTranslation('name' , $lang) }}</a> </li>
          @endforeach
        </ul>

      </div>
      <div class="col-lg-3 ">
        <h5 class="widget-title"> @lang('site.user') </h5>
        <ul>
          <li> <a href="{{ route('login') }}"> @lang('site.login') </a> </li>
          <li> <a href="{{ route('register') }}"> @lang('site.register') </a> </li>
          <li> <a href="{{  route('user.orders')  }}"> @lang('site.my_orders') </a> </li>
          <li> <a href="blog-grid.html"> @lang('site.wishlist') </a> </li>
        </ul>
      </div>
      <div class="col-lg-3 ">
        <h5 class="widget-title"> @lang('site.contact_us') </h5>
        <ul>
          <li> <a href="{{ route('contact.form') }}"> @lang('site.contact_us') </a> </li>
          <li> <a href="faq.html"> @lang('site.faq') </a> </li>
        </ul>
      </div>

    </div>
  </div>
</div>
<!-- Footer Middle End -->

<!-- Footer Bottom Start -->
<div class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <p class="m-0">&copy; Copyright 2020 - <a href="#">YourWebsite</a> All Rights Reserved.</p>
      </div>

    </div>
  </div>
</div>
<!-- Footer Bottom End -->
</footer>
  <!-- Footer End -->