@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp

<header class="mainHeader">
   <div class="container">
     <div class="mainHeader-container">
       <div class="mainHeader-logo">
         <i id="mainNavHeader-open-btn"  class="fa fa-align-left icon mainheader-hideDesktop">
         </i>
         <a href="/"><img width='20' height='20' src="{{ Storage::url('assets/img/Vivo_Logo_Perfumes-03.png') }}" alt="logo icon" /></a>
      </div>

      <div class="mainHeader-info">
         <div class="mainHeader-info-item">
           <div class="mainHeader-info-flag dropdown" >
             <i class="fa fa-chevron-down dropdownicon"></i>

             <span class="mainHeader-info-flag-text mainheader-hideMobileTablet">
               <span> @lang('site.ship_to') </span>

               <span class="mainHeader-info-flag-boldtext">{{ request()->session()->get('country_name') }}</span>
            </span>

            <img src="{{ Storage::url('countries/'.request()->session()->get("country_image")) }}" alt="flag" />

            <ul class="dropdown-menu" >

               @foreach ($data['countries'] as $country)
               <li class="dropdown-item" >
                  <a href="{{ url('/') }}">
                     <i class="fa fa-square-o dropdown-icon"></i>
                     <img src="{{ Storage::url('countries/'.$country->image) }}" />
                     <span class="mainHeader-info-flag-dropdown-item-text" > {{ $country->name }} </span>
                  </a>
               </li>
               @endforeach

            </ul>
         </div>

         <a href="{{ $lang == 'ar' ? LaravelLocalization::getLocalizedURL('en') : LaravelLocalization::getLocalizedURL('ar') }}" class="mainHeader-info-langtext mainheader-hideMobileTablet" > 
            {{ $lang == 'ar' ?  'Egnlish' : 'العربيه' }}
         </a>


         <a href="#" class="mainHeader-info-parentuserIcons mainheader-hideDesktop" >
          <i class="fa fa-search mainHeader-info-userIcons"></i>
       </a>

       @if (!Auth::check())
       <a href="{{ route('login') }}" class="mainHeader-info-parentuserIcons mainheader-hideMobileTablet" >
          <i class="fa fa-user mainHeader-info-userIcons"></i>
       </a>
       @else 
       <a href="{{ route('account') }}" class="mainHeader-info-parentuserIcons mainheader-hideMobileTablet" >
          <i class="fa fa-user mainHeader-info-userIcons"></i>
       </a>

       @endif

       <a href="{{ route('cart.index') }}" class="mainHeader-info-parentuserIcons" >
         @livewire('site.mini-cart')
         <i class="fa fa-shopping-cart mainHeader-info-userIcons"></i>
      </a>
   </div>
</div>

<form class="mainHeader-search" action="{{ route('shop') }}" method="get" >
   <input
   type="search"  id="search__input" name='search' class="mainHeader-search-searchInput form-control"
   placeholder=" @lang('site.Quick_Search') "
   />
   <button type="submit" class="mainHeader-search-buttonIconSubmit">
     <i class="fa fa-search"></i>
  </button>
</form>
</div>
</div>

<nav id="mainheader-mainNav"
class="mainheader-mainNav"
id="navbarSupportedContent" >

<div class="mainheader-mainNav-content">
 <div class="mainheader-mainNav-header">
   <i
   class="navbar-toggler"
   id="mainNavHeader-close-btn"
   data-bs-toggle="collapse"
   data-bs-target="#navbarSupportedContent"
   >
   X
</i>

<h6 class="mainheader-mainNav-header-title">
  @lang('site.wellcome')
  @if (Auth::check())
  {{ Auth::user()->name() }}
  @else 
  <span> @lang('site.guest') </span>
  @endif
</h6>
<a href="{{ route('login') }}" class="mainheader-mainNav-header-title-authButtons" > @lang('site.login') </a>
<a href="{{ route('register') }}" class="mainheader-mainNav-header-title-authButtons" >@lang('site.register') </a>
</div>

<ul class="mainheader-mainNav-list">
   <li class="mainheader-mainNav-item">
      <a href="{{ url('/') }}">
         <i class="fa fa-home"> </i>
         <span class="mainheader-mainNav-item-text" > @lang('site.home') </span>
      </a>
   </li>
   <li class="mainheader-mainNav-item">
     <i class="fa fa-th-large"></i>
     <span class="mainheader-mainNav-item-text" >  @lang('site.categories') </span>

     <div class="mainheader-mainNav-item-categories">

        @foreach ($data['categories'] as $category)
        <a href="{{ route('shop' , ['category' => $category->id  ] ) }}"  class="mainheader-mainNav-item-categories-text" > {{ $category->name }} </a>
        @endforeach
     </div>
  </li>



  <li class="mainheader-mainNav-item">
    <a href="{{ route('contact.form') }}">
     <i class="fa fa-phone"></i>
     <span class="mainheader-mainNav-item-text" >  @lang('site.contact_us') </span>
  </a>
</li>
</ul>

<div class="mainheader-mainNav-shipto dropdown" >
   <i class="fa fa-caret-down"></i>
   <span> @lang('site.ship_to') </span>

   <ul class="dropdown-menu" >

      @foreach ($data['countries'] as $country)
      <li class="dropdown-item" >
         <a href="{{ url('/') }}">
            <i class="fa fa-square-o dropdown-icon"></i>
            <img src="{{ Storage::url('countries/'.$country->image) }}" />
            <span class="mainheader-mainNav-shipto-item-text" >{{ $country->name }}</span>
         </a>
      </li>
      @endforeach
   </ul>

   {{ request()->session()->get('country_name') }} <img src="{{ Storage::url('countries/'.request()->session()->get('country_image')) }}" alt="flag" />
</div>
</div>
<!-- </div> -->
</nav>

<!-- second nav -->
<div class="mainheader-secondNav mainheader-hideMobileTablet" >
   <div class="container">
      <div class="mainheader-secondNav-categories dropdown">
         <i class="fa fa-align-left mainheader-secondNav-categories-alignicon"></i>
         <span class="mainheader-secondNav-titleText" > @lang('site.categories') </span>
         <i class="fa fa-caret-down"></i>
         <ul class="dropdown-menu" >

            @foreach ($data['categories'] as $category)
            <li class="dropdown-item">
               <a href="{{ route('shop' , ['category' => $category->id  ] ) }}"> {{ $category->name }} </a>
            </li>
            @endforeach
         </ul>
      </div>
      <a href="{{ url('/') }}" class="mainheader-secondNav-titleText" > @lang('site.home') </a>
   </div>
</div>
</header>