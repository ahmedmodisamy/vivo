@extends('site.layouts.master')

@section('page_title' , 'Home' )
@section('page_content')
<div class="section login-style-2">
  <div class="container">
   <div class="acr-auth-container">
     <div class="acr-auth-content">

      <form method="POST" action="{{ route('login') }}" >
         @csrf

       <div class="auth-text">
        <h3> @lang('site.login') </h3>
     </div>

     <div class="form-group">
        <input type="text" class="form-control form-control-light" placeholder="@lang('site.email')" name="email" value="{{ old('email') }}">
     </div>
     <div class="form-group">
        <input type="password" class="form-control form-control-light" placeholder="@lang('site.password')" name="password">
     </div>
     <div class="form-group">
        <a href="#" class="forgot-password"> @lang('site.forget_password') </a>
     </div>
     <button type="submit" class="btn-custom secondary btn-block">@lang('site.login')</button>
     <div class="auth-seperator">
        <span>@lang('site.or')</span>
     </div>
     <div class="social-login">
        <a  href='/en/auth/facebook' class="acr-social-login facebook"><i class="fab fa-facebook-f"></i> @lang('site.Continue_with_Facebook') </a>
        <a  href='/en/auth/google' class="acr-social-login google"><i class="fab fa-google"></i> @lang('site.Continue_with_Google') </a>
     </div>
     <p class="text-center mb-0"> @lang('site.dont_have_an_account?') <a href="{{ route('register') }}"> @lang('site.register_now') </a> </p>

  </form>
</div>

</div>
</div>
</div>
@endsection