@extends('site.layouts.master')

@section('page_title' , 'Home' )
@section('page_content')
<div class="section login-style-2">
   <div class="container">
      <div class="acr-auth-container">
         <div class="acr-auth-content">
                 <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
 
            <form method="POST" action="{{ route('password.email') }}" >
               @csrf
               <div class="auth-text">
                  <h3> @lang('site.forget_password') </h3>
               </div>

               <div class="form-group">
                  <input type="text" class="form-control form-control-light" placeholder="@lang('site.email')" name="email" value="{{ old('email') }}">
               </div>
               <button type="submit" class="btn-custom secondary btn-block">@lang('site.Email Password Reset Link')</button>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection