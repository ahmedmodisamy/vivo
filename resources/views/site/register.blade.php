@extends('site.layouts.master')

@php
  $lang = LaravelLocalization::getCurrentLocale();
@endphp
@section('page_title' , 'Home' )
@section('page_content')
  <div class="section login-style-2">
    <div class="container">
      <div class="acr-auth-container">
    <div class="acr-auth-content">

      <form method="POST" action="{{ route('register') }}"  >
        @csrf
        <div class="auth-text">
          <h3> @lang('site.register') </h3>
          <p style="visibility: hidden;" >Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
        </div>

        <div class="form-group">
          <input type="text" class="form-control form-control-light"value='{{ old('first_name') }}'  placeholder="@lang('site.first_name')" name="first_name" >
          @error('first_name')
          <p class='text-danger'> {{ $message }} </p>
          @enderror
        </div>


        <div class="form-group">
          <input type="text" class="form-control form-control-light"value='{{ old('last_name') }}'  placeholder="@lang('site.last_name')" name="last_name" >
          @error('last_name')
          <p class='text-danger'> {{ $message }} </p>
          @enderror
        </div>

        <div class="form-group">
          <input type="text" class="form-control form-control-light" value='{{ old('phone') }}' placeholder="@lang('site.phone')" name="phone" value="">
           @error('phone')
          <p class='text-danger'> {{ $message }} </p>
          @enderror
        </div>
        <div class="form-group">
          {{-- <input type="text" class="form-control form-control-light" placeholder="Mobile" name="mobile" value=""> --}}
          <select name="country_id"  class="form-control form-control-light" required="required">
            <option value=""> @lang('site.country') </option>
            @foreach ($countries as $country)
              <option value="{{ $country->id }}"> {{ $country->getTranslation('name' , $lang) }} </option>
            @endforeach
          </select>

           @error('country_id')
          <p class='text-danger'> {{ $message }} </p>
          @enderror
        </div>


        <div class="form-group">
          <input type="email" class="form-control form-control-light" value='{{ old('email') }}' placeholder="@lang('site.email')" name="email" >
           @error('email')
          <p class='text-danger'> {{ $message }} </p>
          @enderror
        </div>

        <div class="form-group">
          <input type="password" class="form-control form-control-light" placeholder="@lang('site.password')" name="password" value="">
           @error('password')
          <p class='text-danger'> {{ $message }} </p>
          @enderror
        </div>
        <div class="form-group">
          <input type="password" class="form-control form-control-light" placeholder="@lang('site.password_confirmation')" name="password_confirmation" value="">
        </div>
        <button type="submit" class="btn-custom secondary btn-block">@lang('site.register')</button>
        <div class="auth-seperator">
          <span> @lang('site.or') </span>
        </div>
        <div class="social-login">
          <button type="button" class="acr-social-login facebook"><i class="fab fa-facebook-f"></i> @lang('site.Continue with Facebook')  </button>
          <button type="button" class="acr-social-login google"><i class="fab fa-google"></i> @lang('site.Continue with Google') </button>
        </div>
        <p class="text-center mb-0"> @lang('site.Already have an account?') <a href="{{ route('login') }}"> @lang('site.login') </a> </p>

      </form>
    </div>
    {{-- <div class="acr-auth-bg bg-cover bg-center dark-overlay dark-overlay-2" style="background-image: url('{{ Storage::url('assets/img/coming-soon/3.jpg') }}')">
      <div class="acr-auth-inner">
        <h3>Hello World!</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </div> --}}
    </div>
    </div>
  </div>
@endsection