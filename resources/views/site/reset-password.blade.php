@extends('site.layouts.master')

@section('page_title' , 'Home' )
@section('page_content')
<div class="section login-style-2">
   <div class="container">
      <div class="acr-auth-container">
         <div class="acr-auth-content">
             <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <form method="POST" action="{{ route('password.email') }}" >

            <input type="hidden" name="token" value="{{ $request->route('token') }}">
               @csrf
               <div class="auth-text">
                  <h3> @lang('site.reset_password') </h3>
               </div>

               <div class="form-group">
                  <input type="text" class="form-control form-control-light" placeholder="@lang('site.email')" name="email" value="{{ old('email' , $request->email) }}">
               </div>
               <div class="form-group">
                  <input type="password" class="form-control form-control-light" placeholder="@lang('site.password')" name="password">
               </div>
               <div class="form-group">
                  <input type="password" class="form-control form-control-light" placeholder="@lang('site.password_confirmation')" name="password_confirmation">
               </div>
               <button type="submit" class="btn-custom secondary btn-block">@lang('site.reset')</button>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection