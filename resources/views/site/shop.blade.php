@extends('site.layouts.master')

@php
  $lang = LaravelLocalization::getCurrentLocale();
@endphp


@section('page_title' , 'Shop' )
@section('page_content')

  @livewire('site.shop')

@endsection



