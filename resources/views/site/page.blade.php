@extends('site.layouts.master')

@php
    $lang = LaravelLocalization::getCurrentLocale();
@endphp
@section('page_title')
{{ $page->getTranslation('title' , $lang) }}

@endsection
@section('page_content')
  
  <div class="section">
    <div class="container">
      <div class="row">

        <div class="col-lg-12">
          <div class="post-content">

            <h3 class="custom-primary">{{ $page->getTranslation('title' , $lang) }}</h3>
            
           {!! $page->getTranslation('content' , $lang) !!}

          </div>
        </div>


      </div>
    </div>
  </div>

@endsection