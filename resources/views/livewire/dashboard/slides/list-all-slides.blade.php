<div>

    <div class="row row-cards">

          @include('dashboard.layouts.messages')
          


        @foreach ($slides as $slide)
        <div class="col-sm-6 col-lg-4">
            <div class="card card-sm">
                <a href="{{ route('dashboard.slides.show' , ['slide' => $slide->id]) }}" class="d-block"><img src="{{ Storage::url('slides/'.$slide->image) }}" class="card-img-top"></a>
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <span class="avatar me-3 rounded" style="background-image: url({{ Storage::url('admins/'.optional($slide->admin)->image) }})"></span>
                        <div>
                            <div>{{ optional($slide->admin)->username }}</div>
                            <div class="text-muted"> {{ $slide->created_at->diffForHumans() }} </div>
                        </div>
                        <div class="ms-auto">
                            <a href="{{ route('dashboard.slides.edit' , ['slide' => $slide->id ] ) }}" class="btn btn-warning btn-sm">
                                        <svg xmlns="http://www.w3.org/2000/svg" style="margin-right: 0px;" class="icon icon-tabler icon-tabler-pencil" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <path d="M4 20h4l10.5 -10.5a1.5 1.5 0 0 0 -4 -4l-10.5 10.5v4"></path>
                                            <line x1="13.5" y1="6.5" x2="17.5" y2="10.5"></line>
                                        </svg>
                                    </a>

                                    <a data-item_id="{{ $slide->id }}"  class="btn btn-danger btn-sm delete_item">
                                        <svg xmlns="http://www.w3.org/2000/svg" style="margin-right: 0px;" class="icon icon-tabler icon-tabler-trash" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <line x1="4" y1="7" x2="20" y2="7"></line>
                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                        </svg>
                                    </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach



</div>
<div class="d-flex">
    {{ $slides->links() }}       
</div>
</div>


@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(function() {

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
        Livewire.on('itemDeleted', postId => {
            Toast.fire({
              icon: 'success',
              title: "@lang('slides.deleting_success')"
          })
        })
        $(document).on('click', 'a.delete_item', function(event) {
            event.preventDefault();
            var item_id = $(this).attr('data-item_id');
            Swal.fire({
                title: '@lang('slides.confirmation_delete_message')',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '@lang('slides.yes')',
                cancelButtonText: '@lang('slides.back')',
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emit('deleteItem' , item_id);
                }
            })

        });
    });
</script>

@endsection