@php 
$lang = LaravelLocalization::getCurrentLocale();
@endphp
<div>
    <div class="col-12">
          @include('dashboard.layouts.messages')
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h3 class="card-title"> @lang('categories.categories') </h3>
            </div>
            <div class="card-body border-bottom py-3">
                <div class="d-flex">
                    <div class="text-muted">
                        @lang('categories.rows')
                        <div class="mx-2 d-inline-block">
                            <select wire:model="rows" id="inputWire" class="form-control" required="required">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="70">70</option>
                                <option value="100">100</option>
                                <option value="150">150</option>
                            </select>
                        </div>
                    </div>
                    <div class="ms-auto text-muted">
                        @lang('categories.search'):
                        <div class="ms-2 d-inline-block">
                            <input type="text" class="form-control" wire:model="search" placeholder="@lang('dashboard.search')" >
                            {{ $search }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap datatable">
                    <thead>
                        <tr>
                            <th>@lang('categories.name_ar')</th>
                            <th>@lang('categories.name_en')</th>
                            <th>@lang('categories.order')</th>
                            <th>@lang('categories.category')</th>
                            <th>@lang('categories.added_by')</th>
                            <th>@lang('categories.status')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                        <tr>
                            <td>
                                <a href="{{ route('dashboard.categories.show' , ['category' => $category->id ]) }}" > {{ $category->getTranslation('name', 'ar') }} </a>
                            </td>
                            <td> {{ $category->getTranslation('name' , 'en') }} </td>
                            <td> {{ $category->order }} </td>
                            <td>  <a href="{{ route('dashboard.categories.show' , ['category' => $category->id ]) }}" > {{ optional($category->category)['name_'.$lang] }} </a> </td>
                            <td> <a href="{{ route('dashboard.categories.show' , ['category' => $category->admin_id ] ) }}"> {{ optional($category->admin)->username }} </a> </td>
                            <td>
                                @switch($category->active)
                                @case(1)
                                <span class="badge bg-green">@lang('categories.active')</span>
                                @break
                                @case(0)
                                <span class="badge bg-red">@lang('categories.inactive')</span>
                                @break
                                @endswitch
                            </td>

                            <td class="text-end">
                                <div class="btn-list justify-content-center">
                                    <a href="{{ route('dashboard.categories.edit' , ['category' => $category->id ] ) }}" class="btn btn-warning btn-sm">
                                        <svg xmlns="http://www.w3.org/2000/svg" style="margin-right: 0px;" class="icon icon-tabler icon-tabler-pencil" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <path d="M4 20h4l10.5 -10.5a1.5 1.5 0 0 0 -4 -4l-10.5 10.5v4"></path>
                                            <line x1="13.5" y1="6.5" x2="17.5" y2="10.5"></line>
                                        </svg>
                                    </a>

                                    <a data-item_id="{{ $category->id }}"  class="btn btn-danger btn-sm delete_item">
                                        <svg xmlns="http://www.w3.org/2000/svg" style="margin-right: 0px;" class="icon icon-tabler icon-tabler-trash" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <line x1="4" y1="7" x2="20" y2="7"></line>
                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                        </svg>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer d-flex align-items-center">
                <p class="m-0 text-muted"> <span>{{ $categories->perPage() }}</span> of <span>{{ $categories->total() }}</span> entries</p>

                <div class="m-0 ms-auto">
                    {{ $categories->links() }}
                </div>

            </div>
        </div>
    </div>
</div>


@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(function() {

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
        Livewire.on('itemDeleted', postId => {
            Toast.fire({
              icon: 'success',
              title: "@lang('categories.deleting_success')"
          })
        })
        $(document).on('click', 'a.delete_item', function(event) {
            event.preventDefault();
            var item_id = $(this).attr('data-item_id');
            Swal.fire({
                title: '@lang('categories.confirmation_delete_message')',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '@lang('categories.yes')',
                cancelButtonText: '@lang('categories.back')',
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emit('deleteItem' , item_id);
                }
            })

        });
    });
</script>

@endsection