<div>
    <div class="col-12">
          @include('dashboard.layouts.messages')
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h3 class="card-title"> @lang('orders.orders') </h3>
            </div>
            <div class="card-body border-bottom py-3">
                <div class="d-flex">
                    <div class="text-muted">
                        @lang('orders.rows')
                        <div class="mx-2 d-inline-block">
                            <select wire:model="rows" id="inputWire" class="form-control" required="required">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="70">70</option>
                                <option value="100">100</option>
                                <option value="150">150</option>
                            </select>
                        </div>
                    </div>
                    <div class="ms-auto text-muted">
                        @lang('orders.search'):
                        <div class="ms-2 d-inline-block">
                            <input type="text" class="form-control " wire:model="search">
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap datatable">
                    <thead>
                        <tr>
                            <th>@lang('orders.code')</th>
                            <th>@lang('orders.mobile')</th>
                            <th>@lang('orders.address')</th>
                            <th>@lang('orders.governorate')</th>
                            <th>@lang('orders.status')</th>
                            <th> @lang('orders.date') </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td>
                                <a href="{{ route('dashboard.orders.show' , ['order' => $order->id ]) }}" > {{ $order->code }} </a>
                            </td>
                            <td> {{ $order->mobile }} </td>
                            <td> {{ $order->address }} </td>
                            <td> {{ optional($order->governorate)->name }} </td>
                            <td>
                                @switch($order->seen)
                                @case(1)
                                <span class="badge bg-green">@lang('orders.yes')</span>
                                @break
                                @case(0)
                                <span class="badge bg-primary">@lang('orders.no')</span>
                                @break
                                @endswitch
                            </td>
                            <td> {{ $order->created_at->toDateTimeString() }}  -  <span class='text-muted' > {{ $order->created_at->diffForHumans() }} </span> </td>

                            <td class="text-end">
                                <div class="btn-list justify-content-center">
                                    

                                    <a data-item_id="{{ $order->id }}"  class="btn btn-danger btn-sm delete_item">
                                        <svg xmlns="http://www.w3.org/2000/svg" style="margin-right: 0px;" class="icon icon-tabler icon-tabler-trash" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <line x1="4" y1="7" x2="20" y2="7"></line>
                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                        </svg>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer d-flex align-items-center">
                <p class="m-0 text-muted"> <span>{{ $orders->perPage() }}</span> of <span>{{ $orders->total() }}</span> entries</p>

                <div class="m-0 ms-auto">
                    {{ $orders->links() }}
                </div>

            </div>
        </div>
    </div>
</div>


@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(function() {

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
        Livewire.on('itemDeleted', postId => {
            Toast.fire({
              icon: 'success',
              title: "@lang('orders.deleting_success')"
          })
        })
        $(document).on('click', 'a.delete_item', function(event) {
            event.preventDefault();
            var item_id = $(this).attr('data-item_id');
            Swal.fire({
                title: '@lang('orders.confirmation_delete_message')',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '@lang('orders.yes')',
                cancelButtonText: '@lang('orders.back')',
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emit('deleteItem' , item_id);
                }
            })

        });
    });
</script>

@endsection