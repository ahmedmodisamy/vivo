@php
$lang = LaravelLocalization::getCurrentLocale();
@endphp
<div>
  <div class="section">
    <div class="container">

      <div class="row">



        <div class="col-lg-12">



            <div class="row">
                <div class="col-md-4"  wire:ignore>
                    <div class="acr-custom-select form-group">
                        <label>@lang('site.sort_by'): </label>
                        <select class="acr-select2 sort_by" wire:model='sort_by' >
                            <option value=""></option>
                            <option value="sales"> @lang('site.most_sales') </option>
                            <option value="views"> @lang('site.most_viwed') </option>
                            <option value="new_arrivals"> @lang('site.newest_products') </option>
                            <option value="priceLowToHigh"> @lang('site.priceLowToHigh') </option>
                            <option value="priceHighToLow"> @lang('site.priceHighToLow') </option>
                            
                        </select>
                    </div>
                </div>
                <div class="col-md-4" wire:ignore>
                    <div class="acr-custom-select form-group">
                        <label>@lang('site.sort_by_category'): </label>
                        <select class="acr-select2 " wire:model='category' >
                            <option value="all"> @lang('site.all_categories') </option>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}"> {{ $category->getTranslation('name' , $lang) }} </option>
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
            </div>

            <!-- Controls End -->

            <div class="row">


                @foreach ($products as $product)
                <div class="col-md-4">
                    <div class="product">
                        <div class="product-thumbnail">
                          <a href="{{ route('products.show' , ['product' => $product->id , '-', $product->slug ] ) }}"><img src="{{ Storage::url('products/'.$product->image) }}" alt="product"></a>
                          <div class="product-badges d-none d-md-block d-sm-block">
                            @if ($product->featured)
                            <span class="product-badge featured"> <i class="fas fa-star"></i> </span>
                            @endif
                            @if ($product->hasSale())
                            <span class="product-badge sale"> @lang('site.on_sale') </span>
                            @endif
                        </div>
                        <div class="product-controls d-none d-md-block d-sm-block">
                            <a href="#" class="favorite"><i class="far fa-heart"></i></a>
                            <a href="#" data-product_id='{{ $product->id }}'  class="quick-view"><i class="fas fa-eye"></i></a>
                        </div>
                    </div>
                    <div class="product-body">
                      <h5 class="product-title"> <a href="{{ route('products.show' , ['product' => $product->id , '-', $product->slug ] ) }}" title="{{ $product->getTranslation('name', $lang) }}"> {{ $product->getTranslation('name', $lang) }}</a> </h5>
                      <div class="acr-rating">
                        <i class="fas fa-star "></i>
                        <i class="fas fa-star "></i>
                        <i class="fas fa-star "></i>
                        <i class="fas fa-star "></i>
                        <i class="fas fa-star "></i>
                    </div>
                    <span class="product-price" style='font-weight: bold; font-size: 28px;' >{{ $product->price() }} @lang('site.da')
                        @if ($product->hasSale())
                        <span> {{ $product->oldPrice() }}  @lang('site.da')</span>
                        @endif
                    </span>
                    <p class="product-text"> {{ $product->getTranslation('mini_description' , $lang) }} </p>
                    <div class="product-gallery-wrapper">
                        <a href="{{ route('products.show' , ['product' => $product->id , '-', $product->slug ] ) }}" class="btn-custom btn-sm secondary add_to_cart" data-product_id="{{ $product->id }}" > @lang('site.add_to_cart') </a>

                        <a href="{{ route('products.show' , ['product' => $product->id , '-', $product->slug ] ) }}" class="btn-custom btn-sm secondary"> @lang('site.view_product') </a>

                    </div>
                </div>
            </div>
        </div>
        @endforeach

    </div>



    {{ $products->links() }}

</div>


</div>

</div>
</div>
<!-- Jewelry    End -->
</div>




@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    Livewire.on('gotoTop', () => {
        window.scrollTo({
            top: 15,
            left: 15,
            behaviour: 'smooth'
        })
    });




    $(document).on('click', 'a.quick-view', function(event) {
      event.preventDefault();
      var modal = $('#quickViewModal');
      var product_id = $(this).attr('data-product_id');
      $.ajax({
        url: '{{ route('get_product_details') }}',
        type: 'GET',
        dataType: 'json',
        data: {product_id:product_id},
    })
      .done(function(data) {
        console.log(data.data.image)
        modal.find('.product-zoom-image img').attr('src' , data.data.image );
        modal.find('.shop-detail-title h3').text(data.data.name );
        modal.find('.product-price-box span.product-price ').text(data.data.price );
        modal.find('.product-descr p.mb-0 ').text(data.data.mini_description );
        modal.find('a.add_to_cart ').attr(  'data-product_id'  ,  data.data.id );    
        modal.modal('show');
    });
  });



    $(document).ready(function () {
        $('.acr-select2').select2();
        $('.acr-select2').on('change', function (e) {
            var data = $('.acr-select2').select2("val");
            @this.set('category', data);
        });

        $('.sort_by').on('change', function (e) {
            var data = $('.sort_by').select2("val");
            @this.set('sort_by', data);
        });
    });

    $(document).ready(function() {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })


        $(document).on('click', 'a.add_to_cart', function(event) {
          event.preventDefault();
          var product_id = $(this).attr('data-product_id');
      // console.log(product_id)
      $.ajax({
        url: '{{ route('cart.store') }}',
        type: 'POST',
        dataType: 'json',
        data: {product_id:product_id , _token:'{{ csrf_token() }}' , quantity:1 },
    })
      .done(function(data) {
        Toast.fire({
          icon: data.status,
          title: data.message
      });
        Livewire.emit('itemAddedToCart');
    });
      
  });
    });



</script>
@endsection



