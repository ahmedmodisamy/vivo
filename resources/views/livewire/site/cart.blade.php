<div>
    <div class="section checkout-sec cart-sec">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="checkout-table">
                        <table>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th> @lang('site.product') </th>
                                    <th> @lang('site.price') </th>
                                    <th> @lang('site.quantity') </th>
                                    <th> @lang('site.total') </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count(session('cart.items')))
                                @foreach (session('cart.items') as $item)
                                <tr>
                                    <td class="remove">
                                        <div class="product-remove">
                                            <a href="#" wire:click="$emit('deleteItem' , '{{ $item['id'] }}' )" class="close-btn close-dark remove_item">
                                                <span></span>
                                                <span></span>
                                            </a>
                                        </div>
                                    </td>
                                    <td data-title="product">
                                        <div class="product-box">
                                            <img src="{{ $item['image'] }}" alt="product">
                                            <div class="product-name">
                                                <h6>
                                                    <a href="#"> {{ $item['name'] }} </a>
                                                </h6>
                                                <p>{{ $item['quantity'] }} Pieces</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-title="Price">
                                        <strong> {{ $item['price'] }} @lang('site.da') </strong>
                                    </td>
                                    <td data-title="Quantity">
                                        <div class="qty-box d-flex">
                                            <span  wire:click="$emit('dencreasItem' , '{{ $item['id'] }}')" class="qty-subtract">
                                                <i class="fa fa-minus"></i>
                                            </span>
                                            <input type="text" name="qty" value="{{ $item['quantity'] }}">
                                            <span  wire:click="$emit('increasItem' , '{{ $item['id'] }}')" class="qty-add">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        </div>
                                    </td>
                                    <td data-title="Total">
                                        <strong>{{ ($item['price'] * $item['quantity']) }} @lang('site.da') </strong>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <p> @lang('site.your_cart_is_empty') </p>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="coupen-code-wrapper">
                        <h4> @lang('site.discount_code') </h4>
                        <div class="form-group">
                            <label> @lang('site.Enter_coupon_code') </label>
                            <input type="text" class="form-control" placeholder="@lang('site.Coupon_Code') " name="coupon" value="">
                        </div>
                        <button type="submit" class="btn-custom primary check_discount_code"> @lang('site.Apply') </button>
                    </div> --}}
                </div>
                <div class="offset-xl-6 col-xl-6">
                    <div class="cart-total checkout-table">
                        <h4> @lang('site.Proceed_to_Checkout') </h4>
                        <table>
                            <tbody>
                                <tr>
                                    <th> @lang('site.subtotal') </th> 
                                    <td> {{ $subtotal }}  @lang('site.da')</td>
                                </tr>
                                <tr>
                                    <th> @lang('site.shippingCost') </th>
                                    <td> {{ $shippingCost }}   @lang('site.da')</td>
                                </tr>

                                <tr>
                                    <th> @lang('site.discount') </th>
                                    <td> <span  class="discount" ></span>  @lang('site.da')</td>
                                </tr>

                                <tr>
                                    <th>@lang('site.total')</th>
                                    <td>
                                    <strong> {{ $total }} @lang('site.da') </strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('checkout.show') }}" class="btn-custom primary btn-block"> @lang('site.Proceed_to_Checkout') </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(function() {

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Livewire.on('productDeleted', postId => {
            Toast.fire({
                icon: 'success',
                title: '@lang('site.product_deleted_from_cart')'
            })
        });



        $('button.check_discount_code').on('click', function(event) {
            event.preventDefault();
            var coupon = $('input[name="coupon"]').val();
            $.ajax({
                url: '{{ route('coupons.validate') }}',
                type: 'GET',
                dataType: 'json',
                data: {coupon:coupon},
            })
            .done(function(data) {
                console.log(data);
            });
            

        });


        




    });
</script>

@endsection
