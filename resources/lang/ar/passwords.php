<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'تم اعاده تعيين كلمة المرور بنجاح!',
    'sent' => 'تم ارسال بريد الكتورن لتعين كلمة المرور!',
    'throttled' => 'من فضلك قم بالانتظار قللا قبل المحاوله مره اخرى.',
    'token' => 'هذا التوكن غير صحيح.',
    'user' => " لا يمكننا ايجاد مستخدم خاص بهذا البريد الاكتورنى.",

];
