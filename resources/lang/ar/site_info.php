<?php

return array (
  'address_ar' => 'العنوان بالعربيه',
  'address_en' => 'العنوان بالانجليزيه',
  'editing_success' => 'تم التعديل بنجاح',
  'email' => 'البريد الاكترونى',
  'facebook' => 'حساب فيس بوك',
  'instagram' => 'حساب انستجرام',
  'phone' => 'رقم التليفون',
  'site_info' => 'معلومات الموقع',
  'twitter' => 'حساب تويتر',
  'working_hourse' => 'سعات العمل',
  'location_on_map' => 'العنون على الخريطه',
);
