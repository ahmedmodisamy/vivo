<?php

return array (
  'activaion' => 'التفعيل',
  'active' => 'مفعل',
  'add_new_brand' => 'إضافه براند جديد',
  'added_by' => 'اضيف بواسطه',
  'adding_error' => 'خطا عند الاضافه',
  'adding_success' => 'تم اضافه البراند بنجاح',
  'back' => 'رجوع',
  'brands' => 'البراندات',
  'cancel' => 'الغاء',
  'confirmation_delete_message' => 'ه انت متاكد من رغبتك فى حذف البراند ؟',
  'created_at' => 'تاريخ الاضافه',
  'curent_brand_image' => 'صوره البراند الحاليه',
  'deleting_success' => 'تم الغاء البرناد بنجاح',
  'edit' => 'تعديل',
  'edit_brand_details' => 'تعدل بيانات البراند',
  'editing_error' => 'خطا اثناء التعدل حاول مره اخرى',
  'editing_success' => 'تم التعديل بنجاح',
  'image' => 'الصوره',
  'name_ar' => 'الاسم بالعربيه',
  'name_en' => 'الاسم بالانجليزيه',
  'rows' => 'عدد الصفوف',
  'search' => 'البحث',
  'show_all_brands' => 'عرض كافه البرندات',
  'show_brand_details' => 'عرض بيانات البراند',
  'status' => 'الحاله',
  'yes' => 'نعم',
);
