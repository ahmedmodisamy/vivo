<?php

return array (
  'activaion' => 'التفعصيل',
  'active' => 'مفعل',
  'add_new_country' => 'إضافه بلد جديده',
  'adding_error' => 'خطا عند الاضافه حاول مره اخرى',
  'adding_success' => 'تم الاضافه بنجاح',
  'back' => 'رجوع',
  'confirmation_delete_message' => 'هل انت متاكد فى رغبتك من حذف العنصر ؟',
  'countries' => 'الدول',
  'country' => 'الدوله',
  'deleting_success' => 'تم الحذف بنجاح',
  'editing_error' => 'خطا عند التعديل حاول مره اخرى',
  'editing_success' => 'تم التعديل بنجاح',
  'name_ar' => 'اسم الدوله بالعربيه',
  'name_en' => 'اسم ادوله بالانجلزيه',
  'rows' => 'عدد الصفوف',
  'search' => 'البحث',
  'show_all_countries' => 'عرض كافه البلاد',
  'yes' => 'نعم',
);
