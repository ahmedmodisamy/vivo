<?php

return array (
  'activaion' => 'التفعيل',
  'active' => 'فعال',
  'add_new_coupon' => 'اضافه كود خصم جديد',
  'added_by' => 'اضيف بواسطه',
  'adding_error' => 'خطا عند الاضافه حاول مره اخرى',
  'adding_success' => 'تم اضافه كود الخصم بنجاح',
  'allowed_used_times' => 'عدد مرات المسموح باستخدامه',
  'allowed_used_times_per_user' => 'عدد مرات الاستخدام للمستخدم الواحد',
  'back' => 'رجوع',
  'categories' => 'التصنيفات',
  'code' => 'كود الخصم',
  'confirmation_delete_message' => 'هل انت متاكد فى رغبتك فى حذف العنصر؟',
  'coupons' => 'كوبونات الخصم',
  'created_at' => 'تاريخ الاضافه',
  'deleting_success' => 'تم حذف الكود بنجاح',
  'discount' => 'قيمه الخصم',
  'discount_type' => 'نوع الخصم',
  'edit_coupon_details' => 'تعديل كوبون الخصم',
  'editing_error' => 'خطا عند التعديل حاول مره اخرى',
  'editing_success' => 'تم التعديل بنجاح',
  'end_date' => 'تاريخ الانتهاء',
  'end_time' => 'وقت الانتهاء',
  'fixed_price' => 'مبلغ ثابت',
  'max_discount_amount' => 'القيمه القصوى للخصم',
  'percentage' => 'النسبه',
  'rows' => 'عدد صفوف العرض',
  'search' => 'البحث',
  'show_all_coupons' => 'عرض كافه كوبونات الخصم',
  'show_coupon_details' => 'عرض تفاصيل كوبون الخصم',
  'start_date' => 'تاريخ البدايه',
  'start_time' => 'وقت الانتهاء',
  'status' => 'الحاله',
  'yes' => 'نعم',
);
