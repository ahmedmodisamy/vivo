<?php

return array (
  'countries' => 'countries',
  'add_new_country' => 'Add new country',
  'country' => 'country',
  'show_all_countries' => 'show all countries',
  'name_ar' => 'country arabic name',
  'name_en' => 'country english name',
  'activaion' => 'activation',
  'active' => 'active',
  'rows' => 'Rows',
  'search' => 'Search',
  'deleting_success' => 'country deleted successfully',
  'confirmation_delete_message' => 'are you sure you want to delete ?',
  'yes' => 'yes',
  'back' => 'back',
  'adding_error' => 'adding error , try again',
  'adding_success' => 'adding success',
  'editing_error' => 'editing error , try again',
  'editing_success' => 'country edited sucessfully',
);
