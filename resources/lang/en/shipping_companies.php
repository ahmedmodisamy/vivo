<?php

return array (
  'shipping_companies' => 'Shipping Companies',
  'add_new_shipping_company' => 'Add new Shipping company',
  'show_all_shipping_companies' => 'Show All companies',
  'company_name' => 'Company Name',
  'governorate' => 'governorate',
  'price' => 'Price',
  'rows' => 'Rows',
  'search' => 'Search',
  'deleting_success' => 'you can\'t restore it again',
  'confirmation_delete_message' => 'Are you sure you wanan to delete this company ?',
  'yes' => 'Yes',
  'back' => 'back',
  'adding_error' => 'Adding error , try again',
  'adding_success' => 'company added successfully',
  'show_shipping_company_details' => 'show shipping company details',
  'created_at' => 'Created at',
  'status' => 'Status',
  'added_by' => 'Added by',
  'governorates' => 'Governorates',
  'main' => 'Main company',
  'not main' => 'Not Main',
);
