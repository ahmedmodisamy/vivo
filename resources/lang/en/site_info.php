<?php

return array (
  'site_info' => 'Site settings',
  'facebook' => 'facebook',
  'twitter' => 'Twiiter',
  'instagram' => 'instagram',
  'phone' => 'phone',
  'email' => 'email',
  'working_hourse' => 'Working Hours',
  'address_ar' => 'Address in arabic',
  'address_en' => 'Address in english',
  'editing_success' => 'updated successfully',
  'location_on_map' => 'location on map',
);
