<?php

return array (
  'brands' => 'Brands',
  'show_all_brands' => 'Show All brands',
  'name_en' => 'English name',
  'name_ar' => 'Arabic name',
  'image' => 'Image',
  'activaion' => 'Activation',
  'active' => 'Active',
  'show_brand_details' => 'show brand details',
  'created_at' => 'created at',
  'status' => 'Status',
  'added_by' => 'Added by',
  'edit_brand_details' => 'Edit brand details',
  'curent_brand_image' => 'Current brand image',
  'cancel' => 'Cancel',
  'edit' => 'Edit',
  'rows' => 'rows',
  'search' => 'Search',
  'deleting_success' => 'Brand deleted successfully',
  'confirmation_delete_message' => 'Are you sure you want to delete this brand ?',
  'yes' => 'Yes',
  'back' => 'Back',
  'adding_error' => 'adding error , try again',
  'adding_success' => 'brand added successfully',
  'editing_error' => 'Editing  Error , try again',
  'editing_success' => 'brand updated successfully',
  'add_new_brand' => 'Add new brand',
);
