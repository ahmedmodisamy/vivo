<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Coupon extends Model
{
    use HasFactory;

    protected $dates = [
        'start_date' ,
        'end_date' , 
    ];



    public function add($data)
    {
        $this->code = $data['code'];
        $this->discount_type = $data['discount_type'];
        $this->discount = $data['discount'];
        $this->allowed_used_times_per_user = $data['allowed_used_times_per_user'];
        $this->allowed_used_times = $data['allowed_used_times'];
        $this->max_discount_amount = $data['max_discount_amount'];
        $this->start_date = $data['start_date'];
        $this->end_date = $data['end_date'];
        $this->start_time = $data['start_time'];
        $this->end_time = $data['end_time'];
        $this->active = isset($data['active']) ? 1 : 0;
        $this->admin_id = Auth::guard('admin')->id();
        $this->categories = isset($data['category_id']) ? json_encode($data['category_id']) : null;
        return $this->save();
    }

    public function edit($data)
    {
        $this->code = $data['code'];
        $this->discount_type = $data['discount_type'];
        $this->discount = $data['discount'];
        $this->allowed_used_times_per_user = $data['allowed_used_times_per_user'];
        $this->allowed_used_times = $data['allowed_used_times'];
        $this->max_discount_amount = $data['max_discount_amount'];
        $this->start_date = $data['start_date'];
        $this->end_date = $data['end_date'];
        $this->start_time = $data['start_time'];
        $this->end_time = $data['end_time'];
        $this->active = isset($data['active']) ? 1 : 0;
        $this->categories = isset($data['category_id']) ? json_encode($data['category_id']) : null;
        return $this->save();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
