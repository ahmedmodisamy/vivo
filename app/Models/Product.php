<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Product extends Model
{
    
    use HasFactory , HasTranslations;
    public $translatable = ['name' , 'description' , 'mini_description'];

    public function add($data)
    {
        $this->admin_id = Auth::guard('admin')->id();
        $this->category_id = $data['category_id'];
        $this->stock = $data['stock'];
        $this->price = $data['price'];
        $this->price_after_sale = $data['price_after_sale'];
        $this->slug = $data['slug'];
        $this->returnable = $data['returnable'];
        $this->active = isset($data['active']) ? 1 : 0;
        $this->featured = isset($data['featured']) ? 1 : 0;


        foreach ($data['name'] as $key => $value) {
            $this->setTranslation('name' , $key , $value );
        }

        foreach ($data['description'] as $key => $value) {
            $this->setTranslation('description' , $key , $value );
        }

        foreach ($data['mini_description'] as $key => $value) {
            $this->setTranslation('mini_description' , $key , $value );
        }

        return $this->save();
    }

    public function edit($data)
    {
        $this->brand_id = $data['brand_id'];
        $this->category_id = $data['category_id'];
        $this->stock = $data['stock'];
        $this->price = $data['price'];
        $this->price_after_sale = $data['price_after_sale'];
        $this->slug = $data['slug'];
        $this->returnable = $data['returnable'];
        $this->active = isset($data['active']) ? 1 : 0;
        $this->featured = isset($data['featured']) ? 1 : 0;

        foreach ($data['name'] as $key => $value) {
            $this->setTranslation('name' , $key , $value );
        }

        foreach ($data['description'] as $key => $value) {
            $this->setTranslation('description' , $key , $value );
        }

        foreach ($data['mini_description'] as $key => $value) {
            $this->setTranslation('mini_description' , $key , $value );
        }

        return $this->save();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }



    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }


    public function hasSale()
    {
        if($this->price_after_sale != null)
            return true;
        return false;

    }



    public function price()
    {
        if($this->hasSale())
            return $this->price_after_sale;

        return $this->price;
    }


    public function oldPrice()
    {
        return $this->price;
    }


    public function rate()
    {
        return ($this->rate * 2 *10);
    }

}
