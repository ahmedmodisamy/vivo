<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Category extends Model
{
    use HasFactory , HasTranslations;

    public $translatable = ['name'];



    public function add($data)
    {
        $this->setTranslation('name' , 'ar' , $data['name']['ar']);
        $this->setTranslation('name' , 'en' , $data['name']['en']);
        $this->category_id = isset($data['category_id']) ? $data['category_id'] : null;
        $this->admin_id = Auth::guard('admin')->id();
        $this->order = $data['order'];
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }

    public function edit($data)
    {
        $this->setTranslation('name' , 'ar' , $data['name']['ar']);
        $this->setTranslation('name' , 'en' , $data['name']['en']);
        $this->category_id = isset($data['category_id']) ? $data['category_id'] : null;
        $this->order = $data['order'];
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }


    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function categories()
    {
        return $this->hasManMan(Category::class);
    }


    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
