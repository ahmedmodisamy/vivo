<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Country extends Model
{
    use HasFactory , HasTranslations;
    public $translatable = ['name'];

    public function add($data)
    {
        $this->setTranslation('name' , 'ar' , $data['name']['ar'] );
        $this->setTranslation('name' , 'en' , $data['name']['en'] );
        $this->admin_id = Auth::guard('admin')->id();
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }

    public function edit($data)
    {
        $this->setTranslation('name' , 'ar' , $data['name']['ar'] );
        $this->setTranslation('name' , 'en' , $data['name']['en'] );
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


}
