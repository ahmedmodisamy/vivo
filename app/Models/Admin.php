<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Auth;
use Hash;
class Admin extends Authenticatable
{
   use HasApiTokens, HasFactory, Notifiable;

   /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function add($data)
    {
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->password =Hash::make( $data['password']);
        $this->admin_id = Auth::guard('admin')->id();
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }


    public function edit($data)
    {
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }


    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
    
}
