<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Brand extends Model
{
    use HasFactory , HasTranslations;
    public $translatable = ['name'];



    public function add($data)
    {
        $this->admin_id = Auth::guard('admin')->id();
        $this->active = isset($data['active']) ? 1 : 0;
        foreach ($data['name'] as $key => $value) {
            $this->setTranslation('name' , $key , $value );
        }

        return $this->save();
    }

    public function edit($data)
    {

        $this->active = isset($data['active']) ? 1 : 0;
        foreach ($data['name'] as $key => $value) {
            $this->setTranslation('name' , $key , $value );
        }

        return $this->save();
    }


    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

}
