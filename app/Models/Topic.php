<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Topic extends Model
{
    use HasFactory;



    public function add($data)
    {
        $this->admin_id = Auth::guard('admin')->id();
        $this->title = $data['title'];
        $this->slug = $data['slug'];
        $this->content = $data['content'];
        $this->active = isset($data['active']) ? 1 : 0;
        $this->commentable = isset($data['commentable']) ? 1 : 0;
        return $this->Save();
    }

    public function edit($data)
    {
        $this->title = $data['title'];
        $this->content = $data['content'];
        $this->slug = $data['slug'];
        $this->active = isset($data['active']) ? 1 : 0;
        $this->commentable = isset($data['commentable']) ? 1 : 0;
        return $this->Save();
    }


    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


}
