<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Slide extends Model
{
    use HasFactory , HasTranslations;

    public $translatable = ['title' , 'subtitle'];

    public function add($data , $image)
    {
        $this->image = $image;
        $this->setTranslation('title' , 'ar' , $data['title']['ar'] );
        $this->setTranslation('subtitle' , 'en' , $data['subtitle']['en'] );
        $this->setTranslation('subtitle' , 'ar' , $data['subtitle']['ar'] );
        $this->setTranslation('title' , 'en' , $data['title']['en'] );
        $this->link = $data['link'];
        $this->active = isset($data['active']) ? 1 : 0 ;
        $this->admin_id = Auth::guard('admin')->id();
        return $this->save();
    }


    public function edit($data)
    {
        $this->setTranslation('title' , 'ar' , $data['title']['ar'] );
        $this->setTranslation('subtitle' , 'en' , $data['subtitle']['en'] );
        $this->setTranslation('subtitle' , 'ar' , $data['subtitle']['ar'] );
        $this->setTranslation('title' , 'en' , $data['title']['en'] );
        $this->link = $data['link'];
        $this->active = isset($data['active']) ? 1 : 0 ;
        return $this->save();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

}
