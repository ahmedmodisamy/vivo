<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class SiteInfo extends Model
{
    use HasFactory , HasTranslations;



    public $translatable = ['address' , 'working_hourse'];
    public function edit($data)
    {
        $this->facebook = $data['facebook'];
        $this->twitter = $data['twitter'];
        $this->instagram = $data['instagram'];
        $this->phone = $data['phone'];
        $this->email = $data['email'];
        $this->setTranslation( 'address' , 'en' ,$data['address']['en'] );
        $this->setTranslation( 'address' , 'ar' ,$data['address']['ar'] );

        $this->working_hourse = $data['working_hourse'];
        $this->lat = $data['latitude'];
        $this->lng = $data['longitude'];

        return $this->save();
    }
}
