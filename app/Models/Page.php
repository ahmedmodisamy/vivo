<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Page extends Model
{
    use HasFactory , HasTranslations;
     public $translatable = ['title' , 'content'];


    public function add($data)
    {
        $this->setTranslation('title'  , 'ar'   , $data['title']['ar'] );
        $this->setTranslation('title'  , 'en'   , $data['title']['en'] );
        $this->setTranslation('content'  , 'ar'   , $data['content']['ar'] );
        $this->setTranslation('content'  , 'en'   , $data['content']['en'] );

        $this->admin_id = Auth::guard('admin')->id();
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }

    public function edit($data)
    {
        $this->setTranslation('title'  , 'ar'   , $data['title']['ar'] );
        $this->setTranslation('title'  , 'en'   , $data['title']['en'] );
        $this->setTranslation('content'  , 'ar'   , $data['content']['ar'] );
        $this->setTranslation('content'  , 'en'   , $data['content']['en'] );
        $this->active = isset($data['active']) ? 1 : 0;
        return $this->save();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

}
