<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Auth;
class ShippingCompany extends Model
{
    use HasFactory;


    public function add($data)
    {
        $this->admin_id = Auth::guard('admin')->id();
        $this->name = $data['name'];
        $this->save();
        $prices = [];
        for ($i = 0; $i <count($data['governorate']) ; $i++) {
                $prices[] = new ShippingCompanyPrice([
                    'shipping_company_id' => $this->id , 
                    'price' => $data['price'][$data['governorate'][$i]] , 
                    'governorate_id' => $data['governorate'][$i] , 
                ]);
        }
        $this->prices()->saveMany($prices);
        return true;
    }


    public function edit($data)
    {
        $this->name = $data['name'];
        $this->save();

        $prices = [];
        $prices = [];
        for ($i = 0; $i <count($data['governorate']) ; $i++) {
                $prices[] = new ShippingCompanyPrice([
                    'shipping_company_id' => $this->id , 
                    'price' => $data['price'][$data['governorate'][$i]] , 
                    'governorate_id' => $data['governorate'][$i] , 
                ]);
        }
        $this->prices()->saveMany($prices);
        return true;
    }




    public function prices()
    {
        return $this->hasMany(ShippingCompanyPrice::class , 'shipping_company_id');
    }


    public function governorates()
    {
        return $this->hasMany(ShippingCompanyPrice::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
