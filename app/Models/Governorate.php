<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Auth;
class Governorate extends Model
{
    use HasFactory , HasTranslations;

    public $translatable = ['name'];




    public function add($data)
    {
        $this->setTranslation('name' , 'ar' , $data['name']['ar'] );
        $this->setTranslation('name' , 'en' , $data['name']['en'] );
        $this->active = isset($data['active']) ? 1 : 0;
        $this->admin_id = Auth::guard('admin')->id();
        $this->country_id = $data['country_id'];
        return $this->save();
    }

    public function edit($data)
    {
        $this->setTranslation('name' , 'ar' , $data['name']['ar'] );
        $this->setTranslation('name' , 'en' , $data['name']['en'] );
        $this->active = isset($data['active']) ? 1 : 0;
        $this->country_id = $data['country_id'];
        return $this->save();
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
