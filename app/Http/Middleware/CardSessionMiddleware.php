<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CardSessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->session()->has('cart')) {
            $cart = [
                'items' => [] , 
                'subtotal' => 0 , 
            ];
            $request->session()->put('cart' , $cart);
        }


        return $next($request);
    }
}
