<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Country;
class CountrySessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->session()->has('country_id')) {
            $country = Country::first();
            $request->session()->put('country_id' , $country->id );
            $request->session()->put('country_name' , $country->name );
            $request->session()->put('country_image' , $country->image );
        }
        return $next($request);
    }
}
