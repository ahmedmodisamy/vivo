<?php

namespace App\Http\Resources\Site;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class ProuctResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id , 
            'name' => $this->name , 
            'image' => Storage::url('products/'.$this->image) , 
            'price' => $this->price() .' '. trans('site.da') , 
            'mini_description' => $this->mini_description
        ];
    }
}
