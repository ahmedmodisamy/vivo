<?php

namespace App\Http\Livewire\Dashboard\Coupons;

use Livewire\Component;
use App\Models\Coupon;
use Livewire\WithPagination;
class ListAllCoupons extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $coupon = Coupon::find($id);
        $this->resetPage();

        if($coupon) {
            $coupon->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $coupons = Coupon::query()->with('admin');

        if($this->search != '')
            $coupons = $coupons->where('code', 'like' , '%'.$this->search.'%');


        $coupons = $coupons->latest()->paginate($this->rows);
        return view('livewire.dashboard.coupons.list-all-coupons' , compact('coupons'));
    }
}
