<?php

namespace App\Http\Livewire\Dashboard\Pages;

use Livewire\Component;
use App\Models\Page;

use Livewire\WithPagination;
class ListAllPages extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $page = Page::find($id);
        $this->resetPage();

        if($page) {
            $page->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $pages = Page::query()->with('admin');

        if($this->search != '')
            $pages = $pages->where('title->ar', 'like' , '%'.$this->search.'%')->orWhere('title->en', 'like' , '%'.$this->search.'%');


        $pages = $pages->latest()->paginate($this->rows);
        return view('livewire.dashboard.pages.list-all-pages' , compact('pages'));
    }
}
