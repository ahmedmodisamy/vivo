<?php

namespace App\Http\Livewire\Dashboard\Admins;

use Livewire\Component;
use App\Models\Admin;
use Storage;
use Livewire\WithPagination;
class ListAllAdmins extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $admin = Admin::find($id);
        $this->resetPage();

        if($admin) {
            Storage::delete('admins/'.$admin->image);
            $admin->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $admins = Admin::query()->with('admin');

        if($this->search != '')
            $admins = $admins->where('username',  'like' , '%'.$this->search.'%')->orWhere('email',  'like' , '%'.$this->search.'%');


        $admins = $admins->latest()->paginate($this->rows);
        return view('livewire.dashboard.admins.list-all-admins' , compact('admins'));
    }
}
