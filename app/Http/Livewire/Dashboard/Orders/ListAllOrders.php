<?php

namespace App\Http\Livewire\Dashboard\Orders;

use Livewire\Component;
use App\Models\Order;

use Livewire\WithPagination;
class ListAllOrders extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $order = Order::find($id);
        $this->resetPage();

        if($order) {
            $order->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $orders = Order::query()->with(['user' , 'governorate']);

        if($this->search != '')
            $orders = $orders->where('code', 'like' , '%'.$this->search.'%')->orWhere('mobile', 'like' , '%'.$this->search.'%');


        $orders = $orders->latest()->paginate($this->rows);
        return view('livewire.dashboard.orders.list-all-orders' , compact('orders'));
    }
}
