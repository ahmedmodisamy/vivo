<?php

namespace App\Http\Livewire\Dashboard\Categories;

use Livewire\Component;
use App\Models\Category;
use Storage;
use Livewire\WithPagination;
class ListAllCategories extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $category = Category::find($id);
        $this->resetPage();

        if($category) {
            Storage::delete(['categories/'.$category->image ,  'categories/'.$category->cover  , 'categories/'.$category->icon ]);
            $category->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $categories = Category::query()->with(['admin' , 'category']);

        if($this->search != '')
            $categories = $categories->where('name->ar',  'like' , '%'.$this->search.'%')->orWhere('name->en',  'like' , '%'.$this->search.'%');


        $categories = $categories->latest()->paginate($this->rows);
        return view('livewire.dashboard.categories.list-all-categories' , compact('categories'));
    }
}
