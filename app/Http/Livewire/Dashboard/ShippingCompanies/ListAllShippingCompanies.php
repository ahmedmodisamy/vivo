<?php

namespace App\Http\Livewire\Dashboard\ShippingCompanies;

use Livewire\Component;
use App\Models\ShippingCompany;
use Livewire\WithPagination;
class ListAllShippingCompanies extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $company = ShippingCompany::find($id);
        $this->resetPage();

        if($company) {
            $company->prices()->delete();
            $company->delete();

        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $shipping_companies = ShippingCompany::query()->with('admin');

        if($this->search != '')
            $shipping_companies = $shipping_companies->where('name', 'like' , '%'.$this->search.'%');


        $shipping_companies = $shipping_companies->latest()->paginate($this->rows);
        return view('livewire.dashboard.shipping-companies.list-all-shipping-companies' , compact('shipping_companies'));
    }
}
