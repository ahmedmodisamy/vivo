<?php

namespace App\Http\Livewire\Dashboard\Countries;

use Livewire\Component;
use App\Models\Country;
use Livewire\WithPagination;
class ListAllCountries extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $country = Country::find($id);
        $this->resetPage();

        if($country) {
            $country->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $countries = Country::query()->with('admin');

        if($this->search != '')
            $countries = $countries->where('name',  'like' , '%'.$this->search.'%');


        $countries = $countries->latest()->paginate($this->rows);
        return view('livewire.dashboard.countries.list-all-countries' , compact('countries'));
    }
}
