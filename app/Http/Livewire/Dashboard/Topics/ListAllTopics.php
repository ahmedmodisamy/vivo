<?php

namespace App\Http\Livewire\Dashboard\Topics;

use Livewire\Component;
use App\Models\Topic;
use Storage;
use Livewire\WithPagination;
class ListAllTopics extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $topic = Topic::find($id);
        $this->resetPage();

        if($topic) {
            Storage::delete('topics/'.$topic->image);
            $topic->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $topics = Topic::query()->with('admin');

        if($this->search != '')
            $topics = $topics->where('name', 'like' , '%'.$this->search.'%');


        $topics = $topics->latest()->paginate($this->rows);
        return view('livewire.dashboard.topics.list-all-topics' , compact('topics'));
    }
}
