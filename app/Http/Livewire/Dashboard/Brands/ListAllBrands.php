<?php

namespace App\Http\Livewire\Dashboard\Brands;

use Livewire\Component;
use App\Models\Brand;
use Storage;
use Livewire\WithPagination;
class ListAllBrands extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $brand = Brand::find($id);
        $this->resetPage();

        if($brand) {
            Storage::delete('brands/'.$brand->image);
            $brand->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $brands = Brand::query()->with('admin');

        if($this->search != '')
            $brands = $brands->where('name', 'like' , '%'.$this->search.'%');


        $brands = $brands->latest()->paginate($this->rows);
        return view('livewire.dashboard.brands.list-all-brands' , compact('brands'));
    }
}
