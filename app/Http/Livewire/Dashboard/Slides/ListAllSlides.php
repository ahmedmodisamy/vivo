<?php

namespace App\Http\Livewire\Dashboard\Slides;

use Livewire\Component;
use App\Models\Slide;
use Storage;
use Livewire\WithPagination;
class ListAllSlides extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $slide = Slide::find($id);
        $this->resetPage();

        if($slide) {
            Storage::delete('slides/'.$slide->image);
            $slide->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $slides = Slide::query()->with('admin');

        if($this->search != '')
            $slides = $slides->where('title_en',  'like' , '%'.$this->search.'%')->orWhere('title_ar', 'like' , '%'.$this->search.'%')->orWhere('subtitle_ar', 'like' , '%'.$this->search.'%')->orWhere('subtitle_en', 'like' , '%'.$this->search.'%');


        $slides = $slides->latest()->paginate($this->rows);
        return view('livewire.dashboard.slides.list-all-slides' , compact('slides'));
    }
}
