<?php

namespace App\Http\Livewire\Dashboard\Governorates;

use Livewire\Component;
use App\Models\Governorate;
use Livewire\WithPagination;
class ListAllGovernorates extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $governorate = Governorate::find($id);
        $this->resetPage();

        if($governorate) {
            $governorate->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $governorates = Governorate::query()->with(['admin' , 'country']);

        if($this->search != '')
            $governorates = $governorates->where('name', 'like' , '%'.$this->search.'%');


        $governorates = $governorates->latest()->paginate($this->rows);
        return view('livewire.dashboard.governorates.list-all-governorates' , compact('governorates'));
    }
}
