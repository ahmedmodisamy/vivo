<?php

namespace App\Http\Livewire\Dashboard\Products;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use Storage;
use Livewire\WithPagination;
class ListAllProducts extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $rows = 10;
    public $category= 'all';
    public $active= 'all';

    public function updatedRows()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    public function updatedCategory()
    {
        $this->resetPage();
    }

    protected $listeners = ['deleteItem'];

    public function deleteItem($id)
    {
        $product = Product::find($id);
        $this->resetPage();

        if($product) {
            // Storage::delete('brands/'.$brand->image);
            $product->delete();
        }
        $this->emit('itemDeleted');
    }


    public function render()
    {
        $categories = Category::all();
        $products = Product::query()->with('admin');

        if($this->search != '')
            $products = $products->where('name', 'like' , '%'.$this->search.'%')->orWhere('mini_description', 'like' , '%'.$this->search.'%')->orWhere('description', 'like' , '%'.$this->search.'%');

        if($this->category != 'all')
            $products = $products->where('category_id', '=' , $this->category );
        if($this->active != 'all')
            $products = $products->where('active', '=' , $this->active );

        $products = $products->latest()->paginate($this->rows);
        return view('livewire.dashboard.products.list-all-products' , compact('products' , 'categories'));
    }
}
