<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use Livewire\WithPagination;
class Shop extends Component
{
    use WithPagination;
    public $category = 'all';
    public $sort_by = null;
    public $search = '';
    protected $queryString = ['category' , 'sort_by' , 'search' ];
    protected $paginationTheme = 'bootstrap';


    public function updatedCategory()
    {
        $this->resetPage();
    }


    public function updatedSortBy()
    {
        $this->resetPage();
    }


    public function gotoPage($page)
    {
        $this->setPage($page);
        $this->emit('gotoTop');
    }

    public function nextPage()
    {
        $this->setPage($this->page + 1);
        $this->emit('gotoTop');
    }

    public function previousPage()
    {
        $this->setPage(max($this->page - 1, 1));
        $this->emit('gotoTop');
    }


    public function render()
    {
        




        $products = Product::query();

        if($this->sort_by != null) {
            switch ($this->sort_by) {
                case 'sales':
                    $products->orderBy('sales' , 'DESC');
                break;
                case 'views':
                    $products->orderBy('views' , 'DESC');
                break;
                case 'new_arrivals':
                    $products->latest();
                break;
                case 'priceLowToHigh':
                    $products->orderBy('price' , 'ASC');
                break;
                case 'priceHighToLow':
                    $products->orderBy('price' , 'DESC');
                break;
                default:
                break;
            }
        }

        if($this->category != 'all')
            $products = $products->where('category_id' , $this->category);

        if($this->search != '')
            $products = $products->where('name' , 'like', '%'.$this->search.'%')->orWhere('mini_description' , 'like', '%'.$this->search.'%');



        $products = $products->paginate(12);

        $categories = Category::where('active' ,1)->get();

        return view('livewire.site.shop' , compact('products' , 'categories') );
    }
}
