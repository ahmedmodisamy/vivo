<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;

class Cart extends Component
{

    public $shippingCost = 0;
    public $subtotal = 0;
    public $total = 0;

    protected $listeners = ['deleteItem' , 'increasItem' , 'dencreasItem'];


    public function calculateSubtotal()
    {
        $items = request()->session()->get('cart.items');
        $subtotal = 0;
        foreach ($items as $key => $item) {
            $subtotal += ($items[$key]['quantity'] * $items[$key]['price'] );
        }
        $this->subtotal = $subtotal;
        $this->total = $this->subtotal + $this->shippingCost;
    }

    public function deleteItem($product_id)
    {
        $items = request()->session()->get('cart.items');
        foreach ($items as $key => $item) {
            if($item['id'] == $product_id ) {
                unset($items[$key]);
            }
        }
        request()->session()->put('cart.items' , $items);
        $this->emit('productDeleted');
        $this->render();
        $this->calculateSubtotal();

    }


    public function increasItem($product_id)
    {
        $items = request()->session()->get('cart.items');
        foreach ($items as $key => $item) {
            if($item['id'] == $product_id ) {
                $items[$key]['quantity'] = $items[$key]['quantity'] + 1;
                break;
            }
        }
        request()->session()->put('cart.items' , $items);
        $this->render();
        $this->calculateSubtotal();

    }

    public function dencreasItem($product_id)
    {
        $items = request()->session()->get('cart.items');
        foreach ($items as $key => $item) {
            if($item['id'] == $product_id ) {
                $items[$key]['quantity'] = $items[$key]['quantity'] - 1;
                if ($items[$key]['quantity'] == 0) {
                    $this->emit( 'deleteItem' ,  $product_id);
                }
                break;
            }
        }
        request()->session()->put('cart.items' , $items);
        $this->render();
        $this->calculateSubtotal();

    }



    public function render()
    {
        $items = request()->session()->get('cart.items');
        $this->calculateSubtotal();
        return view('livewire.site.cart' , compact('items'));
    }
}
