<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;

class MiniCart extends Component
{

    protected $listeners = ['itemAddedToCart'];


    public function itemAddedToCart()
    {
        $this->render();
    }

    



    public function render()
    {
        return view('livewire.site.mini-cart');
    }
}
