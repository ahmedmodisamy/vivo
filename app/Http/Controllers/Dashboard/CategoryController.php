<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Storage;
use App\Http\Requests\Dashboard\Categories\StoreCategoryRequest;
use App\Http\Requests\Dashboard\Categories\UpdateCategoryRequest;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('category_id' , '=' , null )->get();
        return view('dashboard.categories.create' , compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $category = new Category;
        if(!$category->add($request->all()))
            return back()->with('error' , trans('categories.adding_error') );

        if($request->hasFile('category_image')) {
            $category_image = $request->file('category_image')->store('categories');
            $category->image = basename($category_image);
            $category->save();
        }
        if($request->hasFile('cover')) {
            $cover = $request->file('cover')->store('categories');
            $category->cover = basename($cover);
            $category->save();
        }

        if($request->hasFile('icon')) {
            $icon = $request->file('icon')->store('categories');
            $category->icon = basename($icon);
            $category->save();
        }

        return redirect(route('dashboard.categories.index'))->with('success' , trans('categories.adding_success') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $category->load(['category'  , 'admin' ]);
        return view('dashboard.categories.show' , compact('category') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::where('category_id' , '=' , null )->get();
        return view('dashboard.categories.edit' , compact('categories' , 'category') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request,Category $category)
    {
         if(!$category->edit($request->all()))
            return back()->with('error' , trans('categories.editing_error') );

        if($request->hasFile('category_image')) {
            Storage::delete('categories/'.$category->image);
            $category_image = $request->file('category_image')->store('categories');
            $category->image = basename($category_image);
            $category->save();
        }
        if($request->hasFile('cover')) {
            Storage::delete('categories/'.$category->cover);
            $cover = $request->file('cover')->store('categories');
            $category->cover = basename($cover);
            $category->save();
        }

        if($request->hasFile('icon')) {
            Storage::delete('categories/'.$category->icon);
            $icon = $request->file('icon')->store('categories');
            $category->icon = basename($icon);
            $category->save();
        }

        return redirect(route('dashboard.categories.index'))->with('success' , trans('categories.editing_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
