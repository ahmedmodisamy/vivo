<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Http\Requests\Dashboard\Profile\UpdateProfileReuqest;
use App\Http\Requests\Dashboard\Profile\UpdatePasswordReuqest;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $admin = Auth::guard('admin')->user();
        return view('dashboard.profile' , compact('admin'));
    }

  


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_profile(UpdateProfileReuqest $request )
    {
        $admin = Auth::guard('admin')->user();
        $admin->username = $request->username;
        $admin->email = $request->email;

        if($request->hasFile('image')) {
            $image = $request->file('image')->store('admins');
            $admin->image = basename($image);
        }

        $admin->save();
        return back()->with('success' , trans('dashboard.profile_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view('dashboard.password');
    }


    public function update_password(UpdatePasswordReuqest $request)
    {
        if(!Hash::check($request->current_password, Auth::guard('admin')->user()->password ))
            return back()->with('error' , trans('dashboard.current_password_is_wrong'));

        $admin = Auth::guard('admin')->user();#
        $admin->password = Hash::make($request->password);
        $admin->save();
        return back()->with('success' , trans('dashboard.password_updated'));
        
    }
}
