<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Governorate;
use App\Http\Requests\Dashboard\Governorates\StoreGovernorateRequest;
use App\Http\Requests\Dashboard\Governorates\UpdateGovernorateRequest;
class GovernorateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.governorates.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::where('active' , 1)->get();
        return view('dashboard.governorates.create' , compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGovernorateRequest $request)
    {
        $governorate = new Governorate;
        if(!$governorate->add($request->all()))
            return back()->with('error' , trans('governorates.adding_error'));

        return redirect(route('dashboard.governorates.index'))->with('success' , trans('governorates.adding_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Governorate $governorate)
    {
        $governorate->load(['country' , 'admin']);
        return view('dashboard.governorates.show' , compact('governorate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Governorate $governorate)
    {
        $countries = Country::where('active' , 1)->get();
        return view('dashboard.governorates.edit' , compact('governorate' , 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGovernorateRequest $request, Governorate $governorate)
    {
         if(!$governorate->edit($request->all()))
            return back()->with('error' , trans('governorates.editing_error'));

        return redirect(route('dashboard.governorates.index'))->with('success' , trans('governorates.editing_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
