<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\Countries\StoreCountryRequest;
use App\Http\Requests\Dashboard\Countries\UpdateCountryRequest;
use App\Models\Country;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.countries.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCountryRequest $request)
    {
        $country = new Country;
        if(!$country->add($request->all()))
            return back()->with('error' , trans('countries.adding_error') );

        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('countries');
            $country->image = basename($image);
            $country->save();
        }

        return redirect(route('dashboard.countries.index'))->with('success' , trans('countries.adding_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        return view('dashboard.countries.edit' , compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCountryRequest $request,Country $country)
    {
        if(!$country->edit($request->all()))
            return back()->with('error' , trans('countries.editing_error') );

        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('countries');
            $country->image = basename($image);
            $country->save();
        }
        

        return redirect(route('dashboard.countries.index'))->with('success' , trans('countries.editing_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
