<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Governorate;
use App\Models\ShippingCompany;
use App\Http\Requests\Dashboard\ShippingCompanies\StoreShippingCompanyRequest;
use App\Http\Requests\Dashboard\ShippingCompanies\UpdateShippingCompanyRequest;
class ShippingCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.shipping_companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $governorates = Governorate::all();
        return view('dashboard.shipping_companies.create' , compact('governorates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShippingCompanyRequest $request)
    {

        $shipping_company = new ShippingCompany;
        if(!$shipping_company->add($request->all()))
            return back()->with('error' , trans('shipping_companies.adding_error'));


        return redirect(route('dashboard.shipping_companies.index'))->with('success' , trans('shipping_companies.adding_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShippingCompany $shipping_company)
    {
        $shipping_company->load(['admin' , 'prices' , 'prices.governorate']);
        return view('dashboard.shipping_companies.show' , compact('shipping_company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ShippingCompany $shipping_company)
    {
        $governorates = Governorate::all();
        $shipping_company_governorats = $shipping_company->prices()->pluck('governorate_id')->toArray(); 
        return view('dashboard.shipping_companies.edit' , compact( 'shipping_company' ,  'governorates' , 'shipping_company_governorats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShippingCompanyRequest $request, ShippingCompany $shipping_company)
    {
        $shipping_company->prices()->delete();
        if(!$shipping_company->edit($request->all()))
            return back()->with('error' , trans('shipping_companies.adding_error'));


        return redirect(route('dashboard.shipping_companies.index'))->with('success' , trans('shipping_companies.adding_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
