<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\Slides\StoreSlideRequest;
use App\Http\Requests\Dashboard\Slides\UpdateSlideRequest;
use App\Models\Slide;
use Storage;
class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.slides.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSlideRequest $request)
    {
        $slide = new Slide;

        $image = $request->file('image')->store('slides');
        $image = basename($image);
        if(!$slide->add($request->all() , $image))
            return back()->with('error' , trans('slides.adding_error'));

        return redirect(route('dashboard.slides.index'))->with('success' , trans('slides.adding_success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        return view('dashboard.slides.edit' , compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSlideRequest $request, Slide $slide)
    {

        if(!$slide->edit($request->all()))
            return back()->with('error' , trans('slides.editing_error'));

        if($request->hasFile('image')) {

            Storage::delete('slides/'.$slide->image);
            $image = $request->file('image')->store('slides');
            $slide->image = basename($image);
            $slide->save();
        }

        return redirect(route('dashboard.slides.index'))->with('success' , trans('slides.editing_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
