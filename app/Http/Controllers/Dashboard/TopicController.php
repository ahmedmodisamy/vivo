<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboar\Topics\StoreTopicRequest;
use App\Http\Requests\Dashboar\Topics\UpdateTopicRequest;
use App\Models\Topic;
use Storage;
class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.topics.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTopicRequest $request)
    {
        $topic = new Topic;
        if(!$topic->add($request->all()))
            return back()->with('topics' , trans('topics.adding_error'));

        if($request->hasFile('image')) {
            $image = $request->file('image')->store('topics');
            $topic->image = basename($image);
            $topic->save();
        }

        return redirect(route('dashboard.topics.index'))->with('success' , trans('topics.adding_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        $topic->load('admin');
        return view('dashboard.topics.show' , compact('topic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        return view('dashboard.topics.edit' , compact('topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopicRequest $request, Topic $topic)
    {
         if(!$topic->edit($request->all()))
            return back()->with('topics' , trans('topics.editing_error'));

        if($request->hasFile('image')) {
            Storage::delete('topics/'.$topic->image);
            $image = $request->file('image')->store('topics');
            $topic->image = basename($image);
            $topic->save();
        }

        return back()->with('success' , trans('topics.editing_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
