<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use Storage;
use App\Http\Requests\Dashboard\Brands\StoreBrandRequest;
use App\Http\Requests\Dashboard\Brands\UpdateBrandRequest;
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.brands.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBrandRequest $request)
    {
        $brand = new Brand;
        if(!$brand->add($request->all()))
            return back()->with('error' , trans('brands.adding_error'));

        if($request->hasFile('image')) {
            $image = $request->file('image')->store('brands');
            $brand->image = basename($image);
            $brand->save();
        }

        return redirect(route('dashboard.brands.index'))->with('success' , trans('brands.adding_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('dashboard.brands.show' , compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('dashboard.brands.edit' , compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        

        if(!$brand->edit($request->all()))
            return back()->with('error' , trans('brands.editing_error'));

        if($request->hasFile('image')) {
            Storage::delete('brands/'.$brand->image);
            $image = $request->file('image')->store('brands');
            $brand->image = basename($image);
            $brand->save();
        }

        return redirect(route('dashboard.brands.index'))->with('success' , trans('brands.editing_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
