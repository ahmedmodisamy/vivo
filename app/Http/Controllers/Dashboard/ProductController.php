<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;

use App\Http\Requests\Dashboard\Products\StoreProductRequest;
use App\Http\Requests\Dashboard\Products\UpdateProductRequest;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::where('active' , 1)->get();
        $categories = Category::where('active' , 1)->get();
        return view('dashboard.products.create' , compact('categories' , 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = new Product;
        if(!$product->add($request->all()))
            return back()->with('error' , trans('products.adding_error'));

        if($request->hasFile('image')) {
            $image = $request->file('image')->store('products');
            $product->image = basename($image);
            $product->save();
        }


        if($request->hasFile('images')) {
            $product_images = [];
            for ($i = 0; $i <count($request->file('images')) ; $i++) {
                $image = $request->file('images.'.$i)->store('products');
                $product_images[] = new ProductImage([
                    'product_id' => $product->id , 
                    'image' => basename($image) , 
                ]);
            }

            $product->images()->saveMany($product_images);
        }



        return redirect(route('dashboard.products.index'))->with('success' , trans('products.adding_success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $brands = Brand::where('active' , 1)->get();
        $categories = Category::where('active' , 1)->get();
        return view('dashboard.products.edit' , compact('product' , 'categories' , 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        if(!$product->edit($request->all()))
            return back()->with('error' , trans('products.editing_error'));

        if($request->hasFile('image')) {
            Storage::delete('products/'.$product->image);
            $image = $request->file('image')->store('products');
            $product->image = basename($image);
            $product->save();
        }


        if($request->hasFile('images')) {
            $product_images = [];
            for ($i = 0; $i <count($request->file('images')) ; $i++) {
                $image = $request->file('images.'.$i)->store('products');
                $product_images[] = new ProductImage([
                    'product_id' => $product->id , 
                    'image' => basename($image) , 
                ]);
            }

            $product->images()->saveMany($product_images);
        }

        return back()->with('success' , trans('products.editing_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
