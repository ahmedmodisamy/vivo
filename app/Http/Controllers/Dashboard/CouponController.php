<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Dashboard\Coupons\StoreCouponRequest;
use App\Http\Requests\Dashboard\Coupons\UpdateCouponRequest;
use App\Models\Coupon;
use App\Models\Category;
class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.coupons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('active' , 1)->get();
        return view('dashboard.coupons.create' , compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCouponRequest $request)
    {
        $coupon = new Coupon;
        if(!$coupon->add($request->all()))
            return back()->with('error' , trans('coupons.adding_error'));

        return redirect(route('dashboard.coupons.index'))->with('success' , trans('coupons.adding_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        return view('dashboard.coupons.show' , compact('coupon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {   
        $categories = Category::where('active' , 1)->get();
        return view('dashboard.coupons.edit' , compact('categories' , 'coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCouponRequest $request, Coupon $coupon)
    {
        if(!$coupon->edit($request->all()))
            return back()->with('error' , trans('coupons.editing_error'));

        return redirect(route('dashboard.coupons.index'))->with('success' , trans('coupons.editing_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
