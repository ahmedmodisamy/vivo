<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Site\SendMessageRequest;
use App\Models\Message;
use App\Models\Admin;
use App\Notifications\NewMessageFromContactUsNotification;
class ContactUsController extends Controller
{
    

    public function form()
    {
        return view('site.contact');
    }



    public function send(SendMessageRequest $request)
    {
        $message = new Message;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->phone = $request->phone;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->save();
        $admin = Admin::first();
        $admin->notify( new NewMessageFromContactUsNotification($message));


        return redirect()->back()->with('success' , trans('site.message_sent'));

    }
}
