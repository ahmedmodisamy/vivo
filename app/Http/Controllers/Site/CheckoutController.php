<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Governorate;
use App\Models\Coupon;
use Carbon\Carbon;
use App\Http\Requests\Site\StoreOrderRequest;
class CheckoutController extends Controller
{

    public function index()
    {
        return view('site.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {
        // dd($request->session()->get('cart.subtotal'));
        $order = new Order;
        $order->code = '#OR'.time().mt_rand(1  , 90);
        $order->first_name = $request->first_name;
        $order->last_name = $request->last_name;
        $order->email = $request->email;
        $order->mobile = $request->phone;
        $order->city = $request->city;
        $order->address = $request->address;
        $order->country_id = $request->country_id;
        $order->governorate_id = $request->governorate_id;
        $order->comment = $request->comment;
        $order->payment_method = $request->payment_method;
        $order->total = session('cart.subtotal');
        $order->subtotal = session('cart.subtotal');
        $order->save();

        $items = [];

        foreach ($request->session()->get('cart.items') as $item) {
            $items[] = new OrderItem([
                'quantity' => $item['quantity'] , 
                'price' => $item['price'] , 
                'product_id' => $item['id'] , 
                'order_id' => $order->id , 
            ]);
        }
        $order->items()->saveMany($items);
        $items = [];
        $request->session()->put('cart.items' , $items );
        $request->session()->put('cart.subtotal' , 0 );
        return redirect(route('checkout.index'))->with('success' , trans('site.your_order_is_success') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $countries = Country::where('active' , 1)->get();
        $governorates = Governorate::where('active' , 1)->get();
        return view('site.checkout' , compact('governorates' , 'countries'));
    }



    public function validateDiscountCoupon(Request $request)
    {
        // dd(Carbon::today()->toDateString());
        $today = Carbon::today()->toDateString();
        $code = $request->coupon;
        $coupon = Coupon::where('code' , $code)->whereDate('start_date', '<=', $today )->whereDate('end_date', '>=', $today)->first();
        
        if(!$coupon)
            return response()->json( ['status' => 'error' , 'message' => 'coupon not valid' ]  , 200);

        
    }

   
}
