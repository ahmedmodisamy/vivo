<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use Auth;
use Exception;
use App\Models\User;
class GoogleAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleGoogleCallback()
    {

        $user = Socialite::driver('google')->user();

        $finduser = User::where('google_id', $user->id)->first();
        if($finduser){
            Auth::login($finduser);
            return redirect('/');
        }else{
            $newUser = new User;
            $newUser->first_name = $user->user['given_name'];
            $newUser->last_name = $user->user['family_name'];
            $newUser->google_id = $user->id;
            $newUser->email = $user->email;
            $newUser->save();
            Auth::login($newUser);
            return redirect('/');
        }
    }



}
