<?php

namespace App\Http\Controllers\Site\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use Auth;
use App\Http\Requests\Site\User\UpdateAccountRequest;
class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $countries = Country::where('active' , 1)->get();
        return view('site.user.account' , compact('user' , 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAccountRequest $request)
    {
        $user = Auth::user();
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->country_id = $request->country_id;
        $user->save();
        return back()->with('success' , trans('site.account_details_editing_success'));
    }


    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
