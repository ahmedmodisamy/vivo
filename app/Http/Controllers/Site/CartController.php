<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use LaravelLocalization;
use Storage;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->filled('product_id'))
            return response()->json( ['status' => 'error'  , 'message' => trans('site.cart_adding_error') ] , 200 );     

        $product = Product::find($request->product_id);
        if(!$product)
            return response()->json( ['status' => 'error'  , 'message' => trans('site.cart_item_not_found') ] , 200 );

        $cart = $request->session()->get('cart');
        $lang = LaravelLocalization::getCurrentLocale();

        if (array_key_exists('items',$cart)) {
            $items = $request->session()->get('cart.items');

            // dd($items);
            $products = [];
            $found = false;
            foreach ($items as $key => $item) {
                if($item['id'] == $request->product_id) {
                    $found = true;
                    $items[$key]['quantity'] = ($items[$key]['quantity'] + $request->quantity);
                    break;
                }
                else {
                    $found = false;
                    continue;
                }
            }

            if($found == false) {
                $item = [
                    'id' => $product->id , 
                    'name' => $product->getTranslation('name' , $lang) ,
                    'image' => Storage::url('products/'.$product->image) , 
                    'price' => $product->price() , 
                    'quantity' => 1 ,
                ];
                array_push($items,$item);
            }           
            $request->session()->put('cart.items' , $items);
            $subtotal = $request->session()->get('cart.subtotal');

            $items_subtotal = $subtotal + ($request->quantity * $product->price());
            $request->session()->put('cart.subtotal' , $items_subtotal );


            return response()->json( ['status' => 'success'  , 'message' => trans('site.item_added_to_cart') ] , 200 );
            
        } else {
            $items = [];
            $item = [
                'id' => $product->id , 
                'name' => $product->getTranslation('name' , $lang) ,
                'image' => Storage::url('products/'.$product->image) , 
                'price' => $product->price() , 
                'quantity' => $request->quantity ,
            ];
            array_push($items,$item);
            $request->session()->put('cart.items' , $items);
            $subtotal = ($request->quantity * $product->price());
            $request->session()->put('cart.subtotal' , $subtotal );
            return response()->json( ['status' => 'success'  , 'message' => trans('site.item_added_to_cart') ] , 200 );
            
        }

        

        ItemAddeToCartEvent::dispatch();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
