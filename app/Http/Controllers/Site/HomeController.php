<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slide;
use App\Models\Product;
use App\Models\Page;
use App\Http\Resources\Site\ProuctResource;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::latest()->where('active' , 1)->get();
        $new_products = Product::where('active' , 1)->latest()->limit(10)->get();
        $best_selling = Product::orderBy('sales' , 'DESC')->limit(10)->get();
        $featured_products = Product::where('featured' , 1)->limit(10)->get();
        return view('site.index' , compact('slides' , 'new_products' , 'best_selling' , 'featured_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function product(Product $product)
    {
        $product->load(['images' , 'reviews' , 'reviews.user']);
        $related_products = Product::where('category_id' , $product->category_id)->inRandomOrder()->limit(4)->get();
        return view('site.product' , compact('product' , 'related_products') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function shop()
    {
        return view('site.shop');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function page(Page $page)
    {
        return view('site.page' , compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_product_details(Request $request)
    {
        $product = Product::find($request->product_id);
        return response()->json( [

            'data' => new  ProuctResource($product) , 
        ] , 200);

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
