<?php

namespace App\Http\Requests\Site\User;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::id();
        return [
            'first_name' => 'required' , 
            'last_name' => 'required' , 
            'email' => 'required|unique:users,email,'.$id , 
            'phone' => 'required|unique:users,phone,'.$id , 
            'country_id' => 'required' , 
        ];
    }
}
