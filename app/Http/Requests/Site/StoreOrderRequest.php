<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'nullable' , 
            'last_name' => 'nullable' , 
            'email' => 'nullable|email' , 
            'phone' => 'required' ,
            'country_id' => 'nullable' , 
            'governorate_id' => 'required_without:city' ,
            'address' => 'nullable' , 
            'city' => 'required_without:governorate_id' , 
            'payment_method' => 'required' , 
            'comment' => 'nullable' , 

        ];
    }
}
