<?php

namespace App\Http\Requests\Dashboard\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Request;
class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(4);
        return [
            'name.ar'  => 'required|unique:categories,name,'.$id , 
            'name.en'  => 'required|unique:categories,name,'.$id , 
            'category_id' => 'nullable' ,
            'active' => 'nullable' ,  
            'order' => 'nullable' ,  
            'cover' => 'nullable|image' , 
            'category_image' => 'nullable|image' , 
            'icon' => 'nullable|image' , 
        ];
    }
}
