<?php

namespace App\Http\Requests\Dashboard\Countries;

use Illuminate\Foundation\Http\FormRequest;
use Request;
class UpdateCountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(4);
        return [
            'name.en' => 'required|unique:countries,name,'.$id , 
            'name.ar' => 'required|unique:countries,name,'.$id , 
            'active' => 'nullable', 
            'image' => 'nullable|image' , 
        ];
    }
}
