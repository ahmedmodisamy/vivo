<?php

namespace App\Http\Requests\Dashboard\Governorates;

use Illuminate\Foundation\Http\FormRequest;
use Request;
class UpdateGovernorateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(4);
        return [
            'name.ar' => 'required|unique:governorates,name,'.$id , 
            'name.en' => 'required|unique:governorates,name,'.$id , 
            'country_id' => 'required' , 
            'active' => 'nullable' , 
        ];
    }
}
