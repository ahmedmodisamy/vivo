<?php

namespace App\Http\Requests\Dashboard\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class UpdateProfileReuqest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::guard('admin')->id();
        return [
            'email' => 'required|email|unique:admins,email,'.$id , 
            'username' => 'required|unique:admins,username,'.$id , 
            'image' => 'nullable|image' , 

        ];
    }
}
