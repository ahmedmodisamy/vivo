<?php

namespace App\Http\Requests\Dashboard\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'nullable' , 
            'brand_id' => 'nullable' ,
            'name.ar' => 'required' ,  
            'name.en' => 'nullable' ,  
            'mini_description.ar' => 'required' ,  
            'mini_description.en' => 'nullable' , 
            'description.ar' => 'required' ,  
            'description.en' => 'nullable' , 
            'price' => 'required' , 
            'image' => 'nullable|image' , 
            'stock' => 'required' , 
            'slug' => 'required' , 
            'returnable' => 'required' , 
            'active' => 'nullable' , 
            'images' => 'nullable' , 
            'images.*' => 'image' , 
            'price_after_sale' => 'nullable' , 
        ];
    }
}
