<?php

namespace App\Http\Requests\Dashboard\Products;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'nullable' , 
            'name.ar' => 'required' ,  
            'name.en' => 'required' ,  
            'mini_description.ar' => 'nullable' ,  
            'mini_description.en' => 'nullable' , 
            'description.ar' => 'nullable' ,  
            'description.en' => 'nullable' , 
            'price' => 'required' , 
            'image' => 'required|image' , 
            'stock' => 'required' , 
            'slug' => 'nullable' , 
            'returnable' => 'required' , 
            'active' => 'nullable' , 
            'images' => 'nullable' , 
            'images.*' => 'image' , 
            'featured' => 'nullable' , 
            'price_after_sale' => 'nullable' , 
        ];
    }
}
