<?php

namespace App\Http\Requests\Dashboard\Coupons;

use Illuminate\Foundation\Http\FormRequest;
use Request;
class UpdateCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(4);
        return [
            'code' => 'required|unique:coupons,code,'.$id , 
            'discount_type' => 'required' , 
            'discount' => 'required' , 
            'allowed_used_times_per_user' => 'nullable' , 
            'allowed_used_times' => 'nullable' , 
            'max_discount_amount' => 'nullable' , 
            'start_date' => 'required' , 
            'end_date' => 'required' , 
            'start_time' => 'required' , 
            'end_time' => 'required' , 
            'active' => 'nullable' , 
            'category_id' => 'nullable', 
        ];
    }
}
