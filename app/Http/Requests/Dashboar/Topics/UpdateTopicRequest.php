<?php

namespace App\Http\Requests\Dashboar\Topics;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'nullable|image' , 
            'title' => 'required' , 
            'slug' => 'required' , 
            'content' => 'required' , 
            'active' => 'nullable' , 
            'commentable' => 'nullable' , 
        ];
    }
}
