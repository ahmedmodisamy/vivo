<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Governorate;
use App\Models\Category;
use App\Models\Country;
use App\Models\SiteInfo;
use App\Models\Page;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['governorates'] = Governorate::where('active' , 1)->get();
        $data['countries'] = Country::where('active' , 1)->get();
        $data['categories'] = Category::where('active' , 1)->get();
        $data['settings'] = SiteInfo::first();
        $data['pages'] = Page::where('active' , 1)->get();


        view()->share('data' , $data);
    }
}
