<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('admin_id');
            $table->tinyInteger('active')->default(1);
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->integer('discount');
            $table->tinyInteger('discount_type')->comment('1 means percentage 2 means fixed amount');
            $table->integer('allowed_used_times')->nullable();
            $table->integer('used_count')->default(0);
            $table->integer('allowed_used_times_per_user')->nullable();
            $table->integer('max_discount_amount')->nullable();
            $table->json('categories')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
