<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('user_id')->nullable();
            $table->double('subtotal');
            $table->double('total');
            $table->double('shipping_cost')->default(0);
            $table->double('shipping_company_id')->nullable();
            $table->integer('coupon_id')->nullable();
            $table->integer('governorate_id')->nullable();
            $table->string('address')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('country_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
